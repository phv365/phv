﻿Public Class clsCoord
    Private mintX As Integer
    Private mintY As Integer

    Public Property X() As Integer
        Get
            Return mintX
        End Get
        Set(ByVal value As Integer)
            mintX = value
        End Set
    End Property

    Public Property Y() As Integer
        Get
            Return mintY
        End Get
        Set(ByVal value As Integer)
            mintY = value
        End Set
    End Property


    Public Sub New(ByVal intX As Integer, ByVal intY As Integer)
        mintX = intX
        mintY = intY
    End Sub
End Class
