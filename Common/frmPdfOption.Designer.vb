<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPdfOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPdfOption))
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.grbFrame = New System.Windows.Forms.GroupBox
        Me.txtCreateMember = New System.Windows.Forms.TextBox
        Me.chkCreateMember = New System.Windows.Forms.CheckBox
        Me.txtRootInfo = New System.Windows.Forms.TextBox
        Me.txtFamilyAnniInfo = New System.Windows.Forms.TextBox
        Me.txtFamilyInfo = New System.Windows.Forms.TextBox
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.chkRootInfo = New System.Windows.Forms.CheckBox
        Me.chkFamilyAnniInfo = New System.Windows.Forms.CheckBox
        Me.chkFamilyInfo = New System.Windows.Forms.CheckBox
        Me.chkDate = New System.Windows.Forms.CheckBox
        Me.chkBorder = New System.Windows.Forms.CheckBox
        Me.grbFrame.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = Global.phv.My.Resources.Resources.back_32
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(314, 161)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 40)
        Me.btnCancel.TabIndex = 70
        Me.btnCancel.Text = "       Thoát"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Image = Global.phv.My.Resources.Resources.pdficon
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.Location = New System.Drawing.Point(174, 161)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(93, 40)
        Me.btnOK.TabIndex = 60
        Me.btnOK.Text = "     Tạo file"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'grbFrame
        '
        Me.grbFrame.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grbFrame.Controls.Add(Me.chkBorder)
        Me.grbFrame.Controls.Add(Me.txtCreateMember)
        Me.grbFrame.Controls.Add(Me.chkCreateMember)
        Me.grbFrame.Controls.Add(Me.txtRootInfo)
        Me.grbFrame.Controls.Add(Me.txtFamilyAnniInfo)
        Me.grbFrame.Controls.Add(Me.txtFamilyInfo)
        Me.grbFrame.Controls.Add(Me.dtpDate)
        Me.grbFrame.Controls.Add(Me.chkRootInfo)
        Me.grbFrame.Controls.Add(Me.chkFamilyAnniInfo)
        Me.grbFrame.Controls.Add(Me.chkFamilyInfo)
        Me.grbFrame.Controls.Add(Me.chkDate)
        Me.grbFrame.Location = New System.Drawing.Point(12, 12)
        Me.grbFrame.Name = "grbFrame"
        Me.grbFrame.Size = New System.Drawing.Size(523, 139)
        Me.grbFrame.TabIndex = 0
        Me.grbFrame.TabStop = False
        Me.grbFrame.Text = "Chọn các thông tin muốn hiển thị"
        '
        'txtCreateMember
        '
        Me.txtCreateMember.Location = New System.Drawing.Point(313, 26)
        Me.txtCreateMember.Name = "txtCreateMember"
        Me.txtCreateMember.Size = New System.Drawing.Size(204, 20)
        Me.txtCreateMember.TabIndex = 80
        '
        'chkCreateMember
        '
        Me.chkCreateMember.AutoSize = True
        Me.chkCreateMember.Location = New System.Drawing.Point(236, 26)
        Me.chkCreateMember.Name = "chkCreateMember"
        Me.chkCreateMember.Size = New System.Drawing.Size(71, 17)
        Me.chkCreateMember.TabIndex = 79
        Me.chkCreateMember.Text = "Người lập"
        Me.chkCreateMember.UseVisualStyleBackColor = True
        '
        'txtRootInfo
        '
        Me.txtRootInfo.Location = New System.Drawing.Point(125, 105)
        Me.txtRootInfo.Name = "txtRootInfo"
        Me.txtRootInfo.Size = New System.Drawing.Size(281, 20)
        Me.txtRootInfo.TabIndex = 78
        '
        'txtFamilyAnniInfo
        '
        Me.txtFamilyAnniInfo.Location = New System.Drawing.Point(125, 79)
        Me.txtFamilyAnniInfo.Name = "txtFamilyAnniInfo"
        Me.txtFamilyAnniInfo.Size = New System.Drawing.Size(392, 20)
        Me.txtFamilyAnniInfo.TabIndex = 77
        '
        'txtFamilyInfo
        '
        Me.txtFamilyInfo.Location = New System.Drawing.Point(125, 53)
        Me.txtFamilyInfo.Name = "txtFamilyInfo"
        Me.txtFamilyInfo.Size = New System.Drawing.Size(392, 20)
        Me.txtFamilyInfo.TabIndex = 76
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "dd/MM/yyyy"
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(125, 26)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(94, 20)
        Me.dtpDate.TabIndex = 75
        '
        'chkRootInfo
        '
        Me.chkRootInfo.AutoSize = True
        Me.chkRootInfo.Location = New System.Drawing.Point(18, 105)
        Me.chkRootInfo.Name = "chkRootInfo"
        Me.chkRootInfo.Size = New System.Drawing.Size(101, 17)
        Me.chkRootInfo.TabIndex = 74
        Me.chkRootInfo.Text = "Thành viên gốc"
        Me.chkRootInfo.UseVisualStyleBackColor = True
        '
        'chkFamilyAnniInfo
        '
        Me.chkFamilyAnniInfo.AutoSize = True
        Me.chkFamilyAnniInfo.Location = New System.Drawing.Point(18, 79)
        Me.chkFamilyAnniInfo.Name = "chkFamilyAnniInfo"
        Me.chkFamilyAnniInfo.Size = New System.Drawing.Size(73, 17)
        Me.chkFamilyAnniInfo.TabIndex = 73
        Me.chkFamilyAnniInfo.Text = "Quê quán"
        Me.chkFamilyAnniInfo.UseVisualStyleBackColor = True
        '
        'chkFamilyInfo
        '
        Me.chkFamilyInfo.AutoSize = True
        Me.chkFamilyInfo.Location = New System.Drawing.Point(18, 53)
        Me.chkFamilyInfo.Name = "chkFamilyInfo"
        Me.chkFamilyInfo.Size = New System.Drawing.Size(87, 17)
        Me.chkFamilyInfo.TabIndex = 72
        Me.chkFamilyInfo.Text = "Tên dòng họ"
        Me.chkFamilyInfo.UseVisualStyleBackColor = True
        '
        'chkDate
        '
        Me.chkDate.AutoSize = True
        Me.chkDate.Location = New System.Drawing.Point(18, 26)
        Me.chkDate.Name = "chkDate"
        Me.chkDate.Size = New System.Drawing.Size(69, 17)
        Me.chkDate.TabIndex = 71
        Me.chkDate.Text = "Ngày tạo"
        Me.chkDate.UseVisualStyleBackColor = True
        '
        'chkBorder
        '
        Me.chkBorder.AutoSize = True
        Me.chkBorder.Location = New System.Drawing.Point(421, 108)
        Me.chkBorder.Name = "chkBorder"
        Me.chkBorder.Size = New System.Drawing.Size(96, 17)
        Me.chkBorder.TabIndex = 75
        Me.chkBorder.Text = "Hiện khung vẽ"
        Me.chkBorder.UseVisualStyleBackColor = True
        '
        'frmPdfOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(547, 209)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.grbFrame)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPdfOption"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tùy chọn tạo file PDF"
        Me.grbFrame.ResumeLayout(False)
        Me.grbFrame.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents grbFrame As System.Windows.Forms.GroupBox
    Friend WithEvents chkDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkRootInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkFamilyAnniInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkFamilyInfo As System.Windows.Forms.CheckBox
    Friend WithEvents txtRootInfo As System.Windows.Forms.TextBox
    Friend WithEvents txtFamilyAnniInfo As System.Windows.Forms.TextBox
    Friend WithEvents txtFamilyInfo As System.Windows.Forms.TextBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCreateMember As System.Windows.Forms.TextBox
    Friend WithEvents chkCreateMember As System.Windows.Forms.CheckBox
    Friend WithEvents chkBorder As System.Windows.Forms.CheckBox
End Class
