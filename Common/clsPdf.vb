'   ******************************************************************
'      TITLE      : Pdf FUNCTIONS
'　　　FUNCTION   :
'      MEMO       : 
'      CREATE     : 2011/12/12　AKB
'      UPDATE     : 
'           2011 PHV Software
'   ******************************************************************
Option Explicit On
Option Strict Off

Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf
Imports PdfSharp.Pdf.IO
Imports PdfSharp.Pdf.Advanced
Imports System.Runtime.InteropServices
Imports System.Drawing.Imaging

Public Class clsPdf

    Private mobjDocument As PdfDocument = New PdfDocument
    Private mobjPage As PdfPage
    Private mobjGfx As XGraphics
    Private mobjPen As XPen

    Private mobjCardLeft As Object                                          'temporary card
    Private mobjCardRight As Object                                         'temporary card
    Private mintStartY As Integer = 100
    Private mintStartX As Integer = 30
    Private mintA4 As Integer = 2500
    Private mintA4Width As Integer = 846
    Private mintA4Height As Integer = 600

    Private mintFontZoom As Integer = 1
    Private mintTreeWidth As Integer = 1
    Private Const mcstrFontName As String = "Arial"

    Private mintMargin As Integer = 30
    Private mintStringLineSpace As Integer = 5
    Private mintNomalFontSize As Integer = 16
    Private mintTitleFontSize As Integer = 30
    Private mfontOptions As XPdfFontOptions = New XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always)

    Private mobjForm As PdfSharp.Forms.PagePreview

    Private mblnIsSmallCard As Boolean                                      'draw small card
    Private mintMEM_CARD_SPACE_LEFT As Integer                              'margin left
    Private mintMEM_CARD_SPACE_DOWN As Integer                              'margin bottom
    Private mintMEM_CARD_W As Integer                                       'card width
    Private mintMEM_CARD_H As Integer                                       'card height

    Private mstrFamilyInfo As String
    Private mstrFamilyAnniInfo As String
    Private mstrRootInfo As String
    Private mstrCreateDate As String
    Private mstrCreateMember As String
    Private mblnBorder As Boolean

    Private mintImageMode As Long
    Private mdblCARD_W As Double
    Private mdblCARD_H As Double
    Private mdblPIC_W As Double
    Private mdblPIC_H As Double
    Private mCardBg As XImage

    Public Function fncSetImageMode(ByVal intImageMode As Integer) As Boolean
        Try
            If intImageMode = 1 Then

                mdblCARD_W = clsDefine.CARD_MIDD_W
                mdblCARD_H = clsDefine.CARD_MIDD_H
                mdblPIC_W = clsDefine.PIC_MIDD_W
                mdblPIC_H = clsDefine.PIC_MIDD_H

            End If

        Catch ex As Exception

        End Try

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Sub New(Optional ByVal dblWidth As Double = 0, Optional ByVal dblHeight As Double = 0, Optional ByVal intTreeWidth As Integer = 0)

        mintTreeWidth = intTreeWidth
        xInit(dblWidth, dblHeight)

    End Sub

    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Sub New(ByVal blnAddPage As Boolean)
        Try

        Catch ex As Exception

        End Try

    End Sub

    '   ****************************************************************** 
    '      FUNCTION   : CreatDate
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property CreateDate() As String
        Get
            Return mstrCreateDate
        End Get

        Set(ByVal strValue As String)
            mstrCreateDate = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : MaxHeight Property, max height of panel
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property FamilyInfo() As String
        Get
            Return mstrFamilyInfo
        End Get

        Set(ByVal strValue As String)
            mstrFamilyInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : MaxHeight Property, max height of panel
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property FamilyAnniInfo() As String
        Get
            Return mstrFamilyAnniInfo
        End Get

        Set(ByVal strValue As String)
            mstrFamilyAnniInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : MaxHeight Property, max height of panel
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property RootInfo() As String
        Get
            Return mstrRootInfo
        End Get

        Set(ByVal strValue As String)
            mstrRootInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : CreatDate
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property CreateMember() As String
        Get
            Return mstrCreateMember
        End Get

        Set(ByVal strValue As String)
            mstrCreateMember = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : ShowBorder
    '      MEMO       :  
    '      CREATE     : 2013/01/30  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property ShowBorder() As Boolean
        Get
            Return mblnBorder
        End Get

        Set(ByVal blnValue As Boolean)
            mblnBorder = blnValue
        End Set

    End Property

    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Sub New(ByVal objFrom As PdfSharp.Forms.PagePreview, _
            Optional ByVal dblWidth As Double = 0, _
            Optional ByVal dblHeight As Double = 0, _
            Optional ByVal intTreeWidth As Integer = 0)

        mobjForm = mobjForm
        'Dim objRender As PdfSharp.Forms.PagePreview.RenderEvent = New PdfSharp.Forms.PagePreview.RenderEvent(AddressOf Render)
        'mobjForm.SetRenderEvent(objRender)
        mintTreeWidth = intTreeWidth
        xInit(dblWidth, dblHeight)

    End Sub

    Private Sub xInit(Optional ByVal dblWidth As Double = 0, Optional ByVal dblHeight As Double = 0)
        mobjDocument.Info.Title = "Phan mem gia pha"
        mobjDocument.Info.Creator = "AKBSoftware(akb.Com.vn)"

        mobjPage = mobjDocument.AddPage
        mobjPen = New XPen(XColor.FromArgb(0, 0, 0))

        If dblWidth > mintA4Width Then

            mobjPage.Width = dblWidth

        Else

            mobjPage.Width = mintA4Width

        End If

        mintStartX = CInt((mobjPage.Width.Point - dblWidth) / 2)

        If dblHeight > mintA4Height Then

            mobjPage.Height = dblHeight '+ 130

        Else

            mobjPage.Height = mintA4Height '+ 40

        End If

        If CInt(mobjPage.Width.Point / mintA4) > 0 Then

            mintFontZoom = CInt(mobjPage.Width.Point / mintA4)

        End If

        If mintFontZoom > 2 Then mintFontZoom = 2

        mintMargin = mintFontZoom * mintMargin
        mintTitleFontSize = mintFontZoom * mintTitleFontSize
        mintStringLineSpace = mintStringLineSpace * mintFontZoom

        mobjPage.Height = CInt(mobjPage.Height) + 3 * mintMargin
        mobjPage.Width = CInt(mobjPage.Width) + 3 * mintMargin
        mintStartX = mintMargin
        mintStartY = CInt(1.5 * mintMargin)
        mobjGfx = XGraphics.FromPdfPage(mobjPage)

    End Sub

    Public Sub fncAddpage(Optional ByVal dblWidth As Double = 0, Optional ByVal dblHeight As Double = 0)
        'mobjDocument.Info.Title = "Phan mem gia pha"
        'mobjDocument.Info.Creator = "AKBSoftware(akb.Com.vn)"
        mintMargin = 30
        mintStringLineSpace = 5
        mintTitleFontSize = 30
        mintNomalFontSize = 16
        mobjPage = mobjDocument.AddPage
        mobjPen = New XPen(XColor.FromArgb(0, 0, 0))
        mintTreeWidth = dblWidth
        If dblWidth > mintA4Width Then

            mobjPage.Width = dblWidth

        Else

            mobjPage.Width = mintA4Width

        End If

        mintStartX = CInt((mobjPage.Width.Point - dblWidth) / 2)

        If dblHeight > mintA4Height Then

            mobjPage.Height = dblHeight '+ 130

        Else

            mobjPage.Height = mintA4Height '+ 40

        End If

        If CInt(mobjPage.Width.Point / mintA4) > 0 Then

            mintFontZoom = CInt(mobjPage.Width.Point / mintA4)

        End If

        If mintFontZoom > 2 Then mintFontZoom = 2

        mintMargin = mintFontZoom * mintMargin
        mintTitleFontSize = mintFontZoom * mintTitleFontSize
        mintStringLineSpace = mintStringLineSpace * mintFontZoom

        mobjPage.Height = CInt(mobjPage.Height) + 3 * mintMargin
        mobjPage.Width = CInt(mobjPage.Width) + 3 * mintMargin
        mintStartX = mintMargin
        mintStartY = CInt(1.5 * mintMargin)
        mobjGfx = XGraphics.FromPdfPage(mobjPage)

    End Sub


    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Sub Save(ByVal strFile As String, Optional ByVal blnShow As Boolean = True)
        Try

            mobjDocument.Close()

            mobjDocument.Save(strFile)


            If blnShow = True Then

                Try
                    Process.Start(strFile)
                Catch ex As Exception
                    fncMessageWarning("Bạn cần cài đặt chường trình đọc file Pdf để mở file này.")
                End Try

            End If

            mobjPage = Nothing
            mobjDocument = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    '   ******************************************************************
    '　　　FUNCTION   : fncOpen, open pdf file
    '      PARAMS     : strFile String, file path
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Public Function fncOpen(ByVal strFile As String) As Boolean

        fncOpen = False

        Try
            Process.Start(strFile)

            mobjPen = Nothing
            mobjGfx = Nothing
            mobjPage = Nothing
            mobjDocument = Nothing

            Return True

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xAddInfo() As Boolean

        Try

            Dim intFontZoom As Integer = 1
            Dim intExtent As Integer = mintMargin

            Dim xfontTitle As XFont = New XFont(mcstrFontName, mintTitleFontSize, XFontStyle.Regular, mfontOptions)
            Dim xfontNomal As XFont = New XFont(mcstrFontName, mintNomalFontSize, XFontStyle.Regular, mfontOptions)
            Dim intHeight As Integer
            Dim strMax As String = ""

            'Reduce fontsize if String too long
            If strMax.Length < mstrFamilyAnniInfo.Length Then strMax = mstrFamilyAnniInfo
            If strMax.Length < mstrFamilyInfo.Length Then strMax = mstrFamilyInfo
            If strMax.Length < mstrRootInfo.Length Then strMax = mstrRootInfo

            Dim xsizeString As XSize = mobjGfx.MeasureString(strMax, xfontTitle)
            Do Until xsizeString.Width < mobjPage.Width.Point - 3 * mintMargin
                xfontTitle = New XFont(mcstrFontName, xfontTitle.Size - 1, XFontStyle.Regular, mfontOptions)
                xsizeString = mobjGfx.MeasureString(strMax, xfontTitle)
            Loop

            If mstrCreateMember <> "" Then mstrCreateDate = mstrCreateDate & "                      NGƯỜI LẬP: " & mstrCreateMember.ToUpper()

            'Calculate  the position of Text
            Dim strPosDate As XPoint = xGetPos(mstrCreateDate, xfontTitle, mintMargin, intHeight)
            Dim strPos1 As XPoint = xGetPos(mstrFamilyInfo, xfontTitle, strPosDate.Y, intHeight)
            Dim strPos2 As XPoint = xGetPos(mstrFamilyAnniInfo, xfontTitle, strPos1.Y + mintStringLineSpace, intHeight)
            Dim strPos3 As XPoint = xGetPos(mstrRootInfo, xfontTitle, strPos2.Y + mintStringLineSpace, intHeight)

            mintStartY = strPos3.Y + mintStringLineSpace + 2 * intHeight
            mobjPage.Height = CInt(mobjPage.Height) + mintStartY - mintMargin

            mintStartX = (mobjPage.Width.Point - mintTreeWidth) / 2

            'mobjGfx = XGraphics.FromPdfPage(mobjPage)
            mobjGfx.DrawString(mstrCreateDate, xfontNomal, XBrushes.Black, New XPoint(CInt(mintMargin + mintStringLineSpace), strPosDate.Y))
            mobjGfx.DrawString(mstrFamilyInfo, xfontTitle, XBrushes.Black, strPos1)
            mobjGfx.DrawString(mstrFamilyAnniInfo, xfontTitle, XBrushes.Black, strPos2)
            mobjGfx.DrawString(mstrRootInfo, xfontTitle, XBrushes.Black, strPos3)

            If ShowBorder Then

                mobjGfx.DrawRectangle(mobjPen, mintMargin, mintMargin, mobjPage.Width.Point - 2 * mintMargin, mobjPage.Height.Point - 2 * mintMargin)

            End If

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    '   ******************************************************************
    '　　　FUNCTION   : Save pdf file
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xAddMemInfo() As Boolean

        Try

            Try

                Dim intFontZoom As Integer = 1
                Dim intExtent As Integer = mintMargin

                Dim xfontTitle As XFont = New XFont(mcstrFontName, mintTitleFontSize, XFontStyle.Regular, mfontOptions)
                Dim xfontNomal As XFont = New XFont(mcstrFontName, mintNomalFontSize, XFontStyle.Regular, mfontOptions)
                Dim intHeight As Integer = 0
                Dim strMax As String = ""
                Dim strCreate As String = ""

                'Reduce fontsize if String too long
                If strMax.Length < mstrFamilyAnniInfo.Length Then strMax = mstrFamilyAnniInfo
                If strMax.Length < mstrFamilyInfo.Length Then strMax = mstrFamilyInfo
                If strMax.Length < mstrRootInfo.Length Then strMax = mstrRootInfo

                Dim xsizeString As XSize = mobjGfx.MeasureString(strMax, xfontTitle)
                Do Until xsizeString.Width < mobjPage.Width.Point - 3 * mintMargin
                    xfontTitle = New XFont(mcstrFontName, xfontTitle.Size - 1, XFontStyle.Regular, mfontOptions)
                    xsizeString = mobjGfx.MeasureString(strMax, xfontTitle)
                Loop

                If mstrCreateMember <> "" Then strCreate = mstrCreateDate & "                      NGƯỜI LẬP: " & mstrCreateMember.ToUpper()

                'Calculate  the position of Text
                Dim strPosDate As XPoint = xGetPos(mstrCreateDate, xfontTitle, mintMargin, intHeight)
                Dim strPos1 As XPoint = xGetPos(mstrFamilyInfo, xfontTitle, strPosDate.Y, intHeight)
                Dim strPos2 As XPoint = xGetPos(mstrFamilyAnniInfo, xfontTitle, strPos1.Y + mintStringLineSpace, intHeight)
                Dim strPos3 As XPoint = xGetPos(mstrRootInfo, xfontTitle, strPos2.Y + mintStringLineSpace, intHeight)

                mintStartY = strPos3.Y + mintStringLineSpace + 2 * intHeight
                mobjPage.Height = CInt(mobjPage.Height) + mintStartY - mintMargin

                mintStartX = (mobjPage.Width.Point - mintTreeWidth) / 2

                'mobjGfx = XGraphics.FromPdfPage(mobjPage)
                mobjGfx.DrawString(strCreate, xfontNomal, XBrushes.Black, New XPoint(CInt(mintMargin + mintStringLineSpace), strPosDate.Y))
                mobjGfx.DrawString(mstrFamilyInfo, xfontTitle, XBrushes.Black, strPos1)
                mobjGfx.DrawString(mstrFamilyAnniInfo, xfontTitle, XBrushes.Black, strPos2)
                mobjGfx.DrawString(mstrRootInfo, xfontTitle, XBrushes.Black, strPos3)

                If ShowBorder Then
                    mobjGfx.DrawRectangle(mobjPen, mintMargin, mintMargin, mobjPage.Width.Point - 2 * mintMargin, mobjPage.Height.Point - 2 * mintMargin)
                End If

            Catch ex As Exception
                Throw ex
            End Try

        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Private Function xGetPos(ByVal strInfo As String, _
                             ByRef charFont As XFont, _
                             ByVal intStartY As Integer, _
                             ByRef intHeight As Integer) As XPoint

        If strInfo = "" Then strInfo = "A"
        Dim xsizeString As XSize = mobjGfx.MeasureString(strInfo, charFont)
        intHeight = xsizeString.Height
        Dim xpPos As XPoint = New XPoint((CInt(mobjPage.Width) - CInt(xsizeString.Width)) / 2, intStartY + xsizeString.Height + mintStringLineSpace)

        Return xpPos

    End Function


    '   ******************************************************************
    '　　　FUNCTION   : fncExportTree, export F-tree to excel
    '      VALUE      : boolean, true - success, false - failure
    '      PARAMS1    : tblDrawControl      Hashtable
    '      PARAMS2    : tblNotDrawControl   Hashtable
    '      MEMO       : 
    '      CREATE     : 2011/12/13　AKB 
    '      UPDATE     : 
    '   ******************************************************************
    Public Function fncExportTree(ByVal objImage() As XImage, _
                                  ByVal objCard() As usrMemCardBase, _
                                  ByVal lstNormalLine As List(Of usrLine), _
                                  ByVal lstSpecialLine As List(Of usrLine), _
                                  Optional ByVal blnShowInfo As Boolean = True) As Boolean

        fncExportTree = False
        Dim intLineWeight As Integer
        Dim intArgbColor As Integer
        intLineWeight = My.Settings.intLineWeight
        intArgbColor = My.Settings.LineColor.ToArgb
        Try

            Dim intId As Integer
            If blnShowInfo Then
                xAddInfo()
            Else
                xAddMemInfo()

            End If


            'xDrawBackground(strRootInfo)

            'draw connector first so that it will be set to back
            'If Not xDrawConnector(mobjDraw.DrawingCard, mobjDraw.NotDrawingCard) Then Return
            'If Not xDrawConnector(lstNormalLine, lstSpecialLine) Then Return False
            fncDrawPdfConnector(mobjGfx, lstNormalLine, New XPen(XColor.FromArgb(intArgbColor), intLineWeight), mintStartX, mintStartY)
            fncDrawPdfConnector(mobjGfx, lstSpecialLine, New XPen(XColor.FromArgb(255, 0, 0), intLineWeight + 1), mintStartX, mintStartY)

            If My.Settings.intCardStyle = clsEnum.emCardStyle.CARD1 Then

                mCardBg = fncMakeImage(My.Settings.strCard1Bg)

                For intId = 0 To objCard.Length - 1
                    'draw card to pdf file
                    fncDrawCard(objCard(intId), objImage(intId))

                Next intId

            Else

                For intId = 0 To objCard.Length - 1
                    objImage(intId).Interpolate = False
                    mobjGfx.DrawImage(objImage(intId), PdfMetric(objCard(intId).CardCoor.X + mintStartX), PdfMetric(objCard(intId).CardCoor.Y + mintStartY))
                Next

            End If


            Return True

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : fncDrawCard
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function fncDrawCard(ByVal objCard As usrMemCardBase, ByVal objImage As XImage) As Boolean

        Try
            Const intFontSize As Double = 8.25
            Const intMerginTop As Double = 15
            Dim fontOptions As XPdfFontOptions = New XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always)

            Dim objCard1 As usrMemberCard1
            Dim dblStartX As Double
            Dim dblStartY As Double
            Dim dblStartTextY As Integer

            Dim strAvarta As String = ""
            Dim strBirthDate As String = ""
            Dim strDDate As String = ""
            Dim strName As String = ""
            Dim strAlias As String = ""
            Dim intAddH As Integer = 0
            Dim intImgW As Integer = 0
            Dim intImgH As Integer = 0

            objCard1 = CType(objCard, usrMemberCard1)

            Dim xfontTitle As XFont = New XFont(clsDefine.gcstrFontName, fncGetZoomValue(intFontSize), XFontStyle.Bold, fontOptions)
            Dim xfontNomal As XFont = New XFont(clsDefine.gcstrFontName, fncGetZoomValue(intFontSize), XFontStyle.Regular, fontOptions)


            dblStartX = objCard1.CardCoor.X + mintStartX
            dblStartY = objCard1.CardCoor.Y + mintStartY



            If My.Settings.intCardSize = CInt(clsEnum.emCardSize.LARGE) Then

                If My.Settings.strCard1Bg <> "" Then

                    If System.IO.File.Exists(My.Settings.strCard1Bg) Then

                        fncDrawCardAvatar(mobjGfx, fncPdfMetric(dblStartX), fncPdfMetric(dblStartY), fncPdfMetric(objCard1.Width), fncPdfMetric(objCard1.Height), mCardBg)

                    End If

                Else

                    mobjGfx.DrawRectangle(mobjPen, fncPdfMetric(CInt(dblStartX)), fncPdfMetric(CInt(dblStartY)), fncPdfMetric(objCard1.Width), fncPdfMetric(objCard1.Height))

                End If

                strAvarta = GetMemberImagePath(objCard1)

                If objCard1.CardImageLocation <> "" Then
                    intImgW = CInt(fncGetZoomValue(objCard1.CardImage.Width))
                    intImgH = CInt(fncGetZoomValue(objCard1.CardImage.Height))
                Else
                    intImgW = CInt(fncGetZoomValue(clsDefine.THUMBNAIL_W))
                    intImgH = CInt(fncGetZoomValue(clsDefine.THUMBNAIL_H))
                End If

                'draw image
                If strAvarta <> "" Then

                    fncDrawCardAvatar(mobjGfx, fncPdfMetric(CInt(dblStartX + (objCard1.Width - intImgW) / 2)), fncPdfMetric(CInt(dblStartY + fncGetZoomValue(intMerginTop))), fncPdfMetric(intImgW), fncPdfMetric(intImgH), objImage)

                End If

            Else

                mobjGfx.DrawRectangle(mobjPen, fncPdfMetric(CInt(dblStartX)), fncPdfMetric(CInt(dblStartY)), fncPdfMetric(objCard1.Width), fncPdfMetric(objCard1.Height))

            End If


            strBirthDate = objCard1.CardBirth

            strDDate = objCard1.CardDeath

            Dim arrName As String() = objCard1.CardName.Split(CChar(vbCrLf))

            If arrName.Length > 0 Then
                strName = arrName(0).Trim(CChar(vbCrLf))
            End If

            If arrName.Length > 1 Then
                strAlias = arrName(1).Trim(CChar(vbCrLf))
                strAlias = strAlias.Trim(CChar(vbCr))
                strAlias = strAlias.Trim(CChar(vbLf))
            End If

            dblStartTextY = CInt(dblStartY + intImgH + fncGetZoomValue(6))

            'draw name
            functionWriteText(strName, CInt(dblStartX), dblStartTextY, objCard1.Width, xfontTitle, intAddH)

            dblStartTextY = dblStartTextY + intAddH
            'draw alias
            functionWriteText(strAlias, CInt(dblStartX), dblStartTextY, objCard1.Width, xfontTitle, intAddH)

            dblStartTextY = dblStartTextY + intAddH

            'draw birthDay
            functionWriteText(strBirthDate, CInt(dblStartX), dblStartTextY, objCard1.Width, xfontNomal, intAddH)

            dblStartTextY = dblStartTextY + intAddH

            'draw DeathDay
            functionWriteText(strDDate, CInt(dblStartX), dblStartTextY, objCard1.Width, xfontNomal, intAddH)

            dblStartTextY = dblStartTextY + intAddH

        Catch ex As Exception

        End Try

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : functionWriteText
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function functionWriteText(ByVal strText As String, _
                                       ByVal intStartX As Integer, _
                                       ByVal intStartY As Integer, _
                                       ByVal intWidth As Integer, _
                                       ByVal objfontTitle As XFont, _
                                       ByRef intHeight As Integer) As Boolean
        Try

            Dim objPos As XPoint
            Dim objTempFont As XFont
            objTempFont = objfontTitle
            objPos = xGetPos(mobjGfx, fncPdfMetric(intStartX), fncPdfMetric(intStartY), fncPdfMetric(intWidth), strText, objTempFont, intHeight)

            mobjGfx.DrawString(strText, objTempFont, XBrushes.Black, objPos)

        Catch ex As Exception

        End Try
    End Function

    '   ******************************************************************
    '　　　FUNCTION   : xGetPos
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xGetPos(ByVal objGfx As XGraphics, _
                             ByVal intStartX As Integer, _
                             ByVal intStartY As Integer, _
                             ByVal intWidth As Integer, _
                             ByVal strInfo As String, _
                             ByRef charFont As XFont, _
                             ByRef intHeight As Integer) As XPoint

        Const cintAddHeight As Integer = 5
        If strInfo = "" Then strInfo = "A"


        Dim xsizeString As XSize = objGfx.MeasureString(strInfo, charFont)


        Do Until xsizeString.Width < intWidth - fncGetZoomValue(9)
            charFont = New XFont(charFont.Name, charFont.Size - 1, charFont.Style, charFont.PdfOptions)
            xsizeString = objGfx.MeasureString(strInfo, charFont)
        Loop

        intHeight = CInt(xsizeString.Height + cintAddHeight * My.Settings.dblCard1Multiple)
        Dim xpPos As XPoint = New XPoint(intStartX + (CInt(intWidth) - CInt(xsizeString.Width)) / 2, intStartY + intHeight)

        Return xpPos

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : fncDrawCardAvatar
    '      MEMO       : 
    '      CREATE     : 2012/01/07  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Public Function fncDrawCardAvatar(ByVal objGfx As XGraphics, _
                                      ByVal dblStartX As Double, ByVal dblStartY As Double, _
                                      ByVal dblPic_W As Double, ByVal dblPic_H As Double, _
                                      ByVal imgAvarta As XImage) As Boolean

        Try


            Dim dblImgWidth As Double
            Dim dblImgHeight As Double
            If Not IsNothing(imgAvarta) Then

                dblImgWidth = dblPic_W
                dblImgHeight = dblPic_H
                'If objImg.Width * dblPic_H > objImg.Height * dblPic_W Then
                '    dblImgWidth = dblPic_W
                '    dblImgHeight = objImg.Height * dblPic_W / objImg.Width

                'Else
                '    dblImgWidth = objImg.Width * dblPic_H / objImg.Height
                '    dblImgHeight = dblPic_H
                'End If


                objGfx.DrawImage(imgAvarta, dblStartX, dblStartY, dblImgWidth, dblImgHeight)


            End If


        Catch ex As Exception

        End Try

    End Function

    Private Function PdfMetric(ByVal intValue As Integer) As Integer

        Dim intDPI As Integer = CInt(intValue * 0.75)
        Return intDPI

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : xDrawConnector, draw lines
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : lstNormalLine   List
    '      PARAMS     : lstSpecialLine  List
    '      MEMO       : 
    '      CREATE     : 2011/09/14  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xDrawConnector(ByVal lstNormalLine As List(Of usrLine), ByVal lstSpecialLine As List(Of usrLine)) As Boolean

        xDrawConnector = False

        Try
            'Dim penBlack As XPen
            'Dim penRed As XPen
            'Dim ptStart As Point
            'Dim ptEnd As Point

            'penBlack = New XPen(XColor.FromArgb(0, 0, 0), 2)
            'penRed = New XPen(XColor.FromArgb(255, 0, 0), 3)

            'fncDrawPdfConnector(mobjGfx, lstNormalLine, New XPen(XColor.FromArgb(0, 0, 0), 2), mintStartX, mintStartY)
            'fncDrawPdfConnector(mobjGfx, lstSpecialLine, New XPen(XColor.FromArgb(255, 0, 0), 3), mintStartX, mintStartY)

            'draw normal line
            'For i As Integer = 0 To lstNormalLine.Count - 1

            '    ptStart = lstNormalLine(i).Location
            '    ptStart.X += mintStartX
            '    ptStart.Y += mintStartY

            '    ptEnd = ptStart

            '    If lstNormalLine(i).LineDirection = clsEnum.emLineDirection.HORIZONTAL Then
            '        ptEnd.X += lstNormalLine(i).Width
            '    Else
            '        ptEnd.Y += lstNormalLine(i).Height
            '    End If

            '    mobjGfx.DrawLine(penBlack, PdfMetric(ptStart.X), PdfMetric(ptStart.Y), PdfMetric(ptEnd.X), PdfMetric(ptEnd.Y))

            'Next

            ''draw special line
            'For i As Integer = 0 To lstSpecialLine.Count - 1

            '    ptStart = lstSpecialLine(i).Location
            '    ptStart.X += mintStartX
            '    ptStart.Y += mintStartY

            '    ptEnd = ptStart

            '    If lstSpecialLine(i).LineDirection = clsEnum.emLineDirection.HORIZONTAL Then
            '        ptEnd.X += lstSpecialLine(i).Width
            '    Else
            '        ptEnd.Y += lstSpecialLine(i).Height
            '    End If

            '    mobjGfx.DrawLine(penRed, ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y)

            'Next

            Return True

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Sub fncExportToImage()
        Dim imageCount As Integer

        'Iterate pages
        For Each page As PdfPage In mobjDocument.Pages

            ' Get resources dictionary
            Dim resources As PdfDictionary = page.Elements.GetDictionary("/Resources")

            If Not resources Is Nothing Then
                ' Get external objects dictionary
                Dim xObjects As PdfDictionary = resources.Elements.GetDictionary("/XObject")
                If Not xObjects Is Nothing Then
                    Dim items As ICollection(Of PdfItem) = xObjects.Elements.Values

                    'Iterate references to external objects
                    For Each item As PdfItem In items

                        Dim reference As PdfReference = CType(item, PdfReference)
                        If (Not reference Is Nothing) Then
                            Dim xObject As PdfDictionary = CType(reference.Value, PdfDictionary)

                            ' Is external object an image?
                            If Not xObject Is Nothing And xObject.Elements.GetString("/Subtype") = "/Image" Then
                                xExportImage(xObject, imageCount)
                            End If
                        End If
                    Next
                End If
            End If
        Next
    End Sub

    Private Sub xExportImage(ByVal image As PdfDictionary, ByRef count As Integer)
        Dim filter As String = image.Elements.GetName("/Filter")
        Select Case filter
            Case "/DCTDecode"
                xExportJpegImage(image, count)
                Exit Select

            Case "/FlateDecode"
                xExportAsPngImage(image, count)
                Exit Select
        End Select
    End Sub

    Private Sub xExportJpegImage(ByVal image As PdfDictionary, ByRef count As Integer)

        'Fortunately JPEG has native support in PDF and exporting an image is just writing the stream to a file.
        Dim stream As Byte() = image.Stream.Value

        Dim fs As System.IO.FileStream = New System.IO.FileStream("C:\" & String.Format("Image{0}.jpeg", count + 1), System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim bw As System.IO.BinaryWriter = New System.IO.BinaryWriter(fs)
        bw.Write(stream)
        bw.Close()

    End Sub


    Private Sub xExportAsPngImage(ByVal image As PdfDictionary, ByRef count As Integer)
        'Dim width As Integer = image.Elements.GetInteger(PdfImage.Keys.Width)
        'Dim height As Integer = image.Elements.GetInteger(PdfImage.Keys.Height)
        'Dim bitsPerComponent As Integer = image.Elements.GetInteger(PdfImage.Keys.BitsPerComponent)

        'Dim flate As PdfSharp.Pdf.Filters.FlateDecode = New PdfSharp.Pdf.Filters.FlateDecode()
        'Dim decodedBytes As Byte() = flate.Decode()

        'Dim pixelFormat As System.Drawing.Imaging.PixelFormat

        'Select Case bitsPerComponent
        '    Case 1
        '        pixelFormat = pixelFormat.Format1bppIndexed
        '        Exit Select
        '    Case 8
        '        pixelFormat = pixelFormat.Format8bppIndexed
        '        Exit Select
        '    Case 24
        '        pixelFormat = pixelFormat.Format24bppRgb
        '        Exit Select
        '    Case Else
        '        Throw New Exception("Unknown pixel format " + bitsPerComponent)
        'End Select


        'Dim bmp As Bitmap = New Bitmap(width, height, pixelFormat)
        'Dim bmpData = bmp.LockBits(New Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, pixelFormat)

        'Dim length As Integer = CInt(Math.Ceiling(width * bitsPerComponent / 8.0))
        'For i As Integer = 0 To height - 1
        '    Dim offset As Integer = i * length
        '    Dim scanOffset As Integer = i * bmpData.Stride
        '    Marshal.Copy(decodedBytes, offset, New IntPtr(bmpData.Scan0.ToInt32() + scanOffset), length)
        'Next

        'bmp.UnlockBits(bmpData);
        'using (FileStream fs = new FileStream(@"C:\Export\PdfSharp\" + String.Format("Image{0}.png", count), FileMode.Create, FileAccess.Write))
        '{
        '    bmp.Save(fs, System.Drawing.Imaging.ImageFormat.Png);
        '}

    End Sub




    ' TODO: You can put the code here that converts vom PDF internal image format to a Windows bitmap
    ' and use GDI+ to save it in PNG format.
    ' It is the work of a day or two for the most important formats. Take a look at the file
    ' PdfSharp.Pdf.Advanced/PdfImage.cs to see how we create the PDF image formats.
    ' We don't need that feature at the moment and therefore will not implement it.
    ' If you write the code for exporting images I would be pleased to publish it in a future release
    ' of PDFsharp.






End Class
