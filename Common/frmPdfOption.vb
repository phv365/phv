'   ****************************************************************** 
'      TITLE      : DRAW F-TREE OPTION
'　　　FUNCTION   :  
'      MEMO       :  
'      CREATE     : 2012/02/17　PHV 
'      UPDATE     :  
' 
'           2012 PHV Software 
'   ******************************************************************
Option Explicit On
Option Strict Off

''' <summary>
''' Option class
''' </summary>
''' <remarks></remarks>
''' <Create>2012/02/17  PHV</Create>
Public Class frmPdfOption

    Private Const mcstrClsName As String = "frmPdfOption"                  'class name
    Private mstrFamilyInfo As String
    Private mstrFamilyAnniInfo As String
    Private mstrRootInfo As String
    Private mstrCreateDate As String
    Private mfrmPrintPreview As PrintPreview
    Private mstrCreateMember As String
    Private mblnBorder As Boolean

    Private mblnChanged As Boolean = False
    Private mblnExportToPdf As Boolean = False

    ''' <summary>
    ''' The change is made
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Changed() As Boolean
        Get
            Return mblnChanged
        End Get
    End Property

    '   ******************************************************************
    '　　　FUNCTION   : fncShowForm, show this form
    '      VALUE      : boolean, true - success, false - failure
    '      PARAMS     : blnIsRollBack   Boolean, flag rollback
    '      MEMO       : 
    '      CREATE     : 2011/07/27  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Public Function fncShowForm(ByVal strRootInfo As String, ByVal frmPrintPreview As PrintPreview) As Boolean

        fncShowForm = False

        Try

            Dim strFName As String = "" 'Family Name,
            Dim strFAnni As String = "" 'Information of Root Member
            Dim strFHome As String = "" 'Information about 
            Dim strFAnniInfo As String = ""
            mblnExportToPdf = True
            mfrmPrintPreview = frmPrintPreview

            mstrFamilyInfo = ""
            mstrFamilyAnniInfo = ""
            mstrRootInfo = ""
            mstrCreateDate = ""

            'get family infor
            basCommon.fncGetFamilyInfo(strFName, strFAnni, strFHome)

            If Not basCommon.fncIsBlank(strFHome) Then mstrFamilyAnniInfo = String.Format("NGUYÊN QUÁN: {0}  ", strFHome.ToUpper())
            If Not basCommon.fncIsBlank(strFAnni) Then mstrFamilyAnniInfo &= String.Format("   NGÀY GIỖ TỔ: {0}", strFAnni.ToUpper())
            mstrFamilyInfo = String.Format("GIA PHẢ DÒNG HỌ: {0}", strFName.ToUpper())

            If strRootInfo <> "" Then
                mstrRootInfo = "GIA ĐÌNH " & strRootInfo.ToUpper
            End If

            mstrCreateDate = "Ngày tạo: " + dtpDate.Value.ToString("dd/MM/yyyy")

            If mstrFamilyAnniInfo <> "" Then
                txtFamilyAnniInfo.Text = mstrFamilyAnniInfo
            End If
            mstrCreateMember = My.Settings.strCreateMember

            txtRootInfo.Text = mstrRootInfo
            txtFamilyInfo.Text = mstrFamilyInfo
            txtCreateMember.Text = mstrCreateMember

            chkDate.Checked = True
            chkFamilyAnniInfo.Checked = True
            chkRootInfo.Checked = False
            chkFamilyInfo.Checked = True
            chkCreateMember.Checked = True
            chkBorder.Checked = False

            Me.ShowDialog()
            Return True

        Catch ex As Exception

            basCommon.fncSaveErr(mcstrClsName, "fncShowForm", ex)

        End Try

    End Function

    '   ******************************************************************
    '　　　FUNCTION   : fncShowForm, show this form
    '      VALUE      : boolean, true - success, false - failure
    '      PARAMS     : blnIsRollBack   Boolean, flag rollback
    '      MEMO       : 
    '      CREATE     : 2011/07/27  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Public Function fncShowFormGetOption(ByVal strRootInfo As String) As Boolean

        fncShowFormGetOption = False

        Try

            Dim strFName As String = "" 'Family Name,
            Dim strFAnni As String = "" 'Information of Root Member
            Dim strFHome As String = "" 'Information about 
            Dim strFAnniInfo As String = ""
            mblnExportToPdf = False
            mstrFamilyInfo = ""
            mstrFamilyAnniInfo = ""
            mstrRootInfo = ""
            mstrCreateDate = ""

            'get family infor
            basCommon.fncGetFamilyInfo(strFName, strFAnni, strFHome)

            If Not basCommon.fncIsBlank(strFHome) Then mstrFamilyAnniInfo = String.Format("NGUYÊN QUÁN: {0}  ", strFHome.ToUpper())
            If Not basCommon.fncIsBlank(strFAnni) Then mstrFamilyAnniInfo &= String.Format("   NGÀY GIỖ TỔ: {0}", strFAnni.ToUpper())
            mstrFamilyInfo = String.Format("GIA PHẢ DÒNG HỌ: {0}", strFName.ToUpper())

            If strRootInfo <> "" Then
                mstrRootInfo = "GIA ĐÌNH " & strRootInfo.ToUpper
            End If

            mstrCreateDate = "Ngày tạo: " + dtpDate.Value.ToString("dd/MM/yyyy")

            If mstrFamilyAnniInfo <> "" Then
                txtFamilyAnniInfo.Text = mstrFamilyAnniInfo
            End If
            mstrCreateMember = My.Settings.strCreateMember
            chkRootInfo.Visible = False
            txtRootInfo.Visible = False

            txtFamilyInfo.Text = mstrFamilyInfo
            txtCreateMember.Text = mstrCreateMember

            chkDate.Checked = True
            chkFamilyAnniInfo.Checked = True
            chkRootInfo.Checked = False
            chkFamilyInfo.Checked = True
            chkCreateMember.Checked = True
            chkBorder.Checked = False

            Me.ShowDialog()
            Return True

        Catch ex As Exception

            basCommon.fncSaveErr(mcstrClsName, "fncShowForm", ex)

        End Try

    End Function

    '   ****************************************************************** 
    '      FUNCTION   : FamilyInfo
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property FamilyInfo() As String
        Get
            Return mstrFamilyInfo
        End Get

        Set(ByVal strValue As String)
            mstrFamilyInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : FamilyAnniInfo
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property FamilyAnniInfo() As String
        Get
            Return mstrFamilyAnniInfo
        End Get

        Set(ByVal strValue As String)
            mstrFamilyAnniInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : RootInfo
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property RootInfo() As String
        Get
            Return mstrRootInfo
        End Get

        Set(ByVal strValue As String)
            mstrRootInfo = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : CreatDate
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property CreateDate() As String
        Get
            Return mstrCreateDate
        End Get

        Set(ByVal strValue As String)
            mstrCreateDate = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : CreatDate
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property CreateMember() As String
        Get
            Return mstrCreateMember
        End Get

        Set(ByVal strValue As String)
            mstrCreateMember = strValue
        End Set

    End Property

    '   ****************************************************************** 
    '      FUNCTION   : CreatDate
    '      MEMO       :  
    '      CREATE     : 2012/11/22  PHV
    '      UPDATE     :  
    '   ******************************************************************
    Public Property ShowBorder() As Boolean
        Get
            Return mblnBorder
        End Get

        Set(ByVal blnValue As Boolean)
            mblnBorder = blnValue
        End Set

    End Property

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub frmPdfOption_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed


    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        mblnChanged = True
        mstrCreateDate = dtpDate.Value.ToString("dd/MM/yyyy")
        mstrFamilyAnniInfo = txtFamilyAnniInfo.Text
        mstrFamilyInfo = txtFamilyInfo.Text
        mstrRootInfo = txtRootInfo.Text
        mstrCreateMember = txtCreateMember.Text
        mblnBorder = chkBorder.Checked

        If Not chkDate.Checked Then mstrCreateDate = ""
        If Not chkFamilyAnniInfo.Checked Then mstrFamilyAnniInfo = ""
        If Not chkFamilyInfo.Checked Then mstrFamilyInfo = ""
        If Not chkRootInfo.Checked Then mstrRootInfo = ""
        If Not chkCreateMember.Checked Then mstrCreateMember = ""

        If mblnExportToPdf Then

            'Dim objPdfPrint As clsPdf = New clsPdf(mintMaxW, mintMaxH)
            Dim objPdfPrint As clsPdf = New clsPdf(mfrmPrintPreview.PdfPagePreview.PageSize.Width, _
                                                   mfrmPrintPreview.PdfPagePreview.PageSize.Height, _
                                                   fncPdfMetric(mfrmPrintPreview.mintMaxX - mfrmPrintPreview.mintMinX))

            objPdfPrint.FamilyAnniInfo = FamilyAnniInfo
            objPdfPrint.FamilyInfo = FamilyInfo
            objPdfPrint.CreateDate = CreateDate
            objPdfPrint.RootInfo = RootInfo
            objPdfPrint.CreateMember = CreateMember
            objPdfPrint.ShowBorder = ShowBorder

            My.Settings.strCreateMember = CreateMember
            My.Settings.Save()

            Me.Cursor = Cursors.WaitCursor
            'try to export F-tree to PDF
            'If objPdfPrint.fncExportTree(mtblControl, mlstNormalLine, mlstSpecialLine) Then
            'Dim stRootMember as stCardInfo = stcar
            With mfrmPrintPreview
                If objPdfPrint.fncExportTree(.mobjImage, .mobjCard, .mlstNormalLine, .mlstSpecialLine) Then
                    Dim dlgSaveFile As SaveFileDialog = New SaveFileDialog()

                    dlgSaveFile.CheckPathExists = True
                    dlgSaveFile.InitialDirectory = Application.StartupPath + "\List"
                    dlgSaveFile.Title = "Cay pha he " & Now.ToString("ddMMyyyy") & ".pdf"
                    dlgSaveFile.Filter = "Tệp tin PDF(*.pdf)|*.pdf|Tất cả các file(*.*)|*.*"

                    If dlgSaveFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        objPdfPrint.Save(dlgSaveFile.FileName)
                    End If

                End If
            End With

            Me.Cursor = Cursors.Default
        Else
            Me.Close()
        End If

        'Me.Close()
    End Sub

    Private Function xDrawCard() As Boolean

        Try

        Catch ex As Exception

        End Try

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        mblnChanged = False
        Me.Close()
    End Sub


End Class