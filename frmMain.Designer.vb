<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.tsmSystem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSysUserInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmSysDataBackup = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSysDataRestore = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmExportToPdf = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmExportToWord = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmSysQuit = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonal = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonalInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonRel = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonalFa = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonalSpouse = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonalChild = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmPersonDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmView = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFullScreen = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFamily = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFamilyInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmValue = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmFamilyNewMem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFamilyBranchMngt = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmFamilyBuild = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFamilyShowTree = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmTreeFull = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmTreeCompact = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmFamilyReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmFamilySearch = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSetting = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSettingDetail = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmEvents = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmDieDate = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmBirthday = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmWedding = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmAllEvent = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSysData = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSysNow = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmSysSetting = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmMySite = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmGuide = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmUpdate = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmActivate = New System.Windows.Forms.ToolStripMenuItem()
        Me.tlsMain = New System.Windows.Forms.ToolStrip()
        Me.tsbOpen = New System.Windows.Forms.ToolStripButton()
        Me.tsbSave = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbBack = New System.Windows.Forms.ToolStripButton()
        Me.tsbForward = New System.Windows.Forms.ToolStripButton()
        Me.tsbHome = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbFamilyBuild = New System.Windows.Forms.ToolStripButton()
        Me.tsbRoot = New System.Windows.Forms.ToolStripButton()
        Me.tsbFamilyShowTree = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.tssEvent = New System.Windows.Forms.ToolStripSplitButton()
        Me.tsmDieDate2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmBirthday2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmWedding2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmAllEvent2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsbAlbum = New System.Windows.Forms.ToolStripButton()
        Me.tsbFamilyCalendar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbSync = New System.Windows.Forms.ToolStripButton()
        Me.tsbSetting = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbTreeView1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.tsbMenuTree1Basic = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsbMenuTree1Open = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsbMenuTree1Impact = New System.Windows.Forms.ToolStripMenuItem()
        Me.tslGenerationNum = New System.Windows.Forms.ToolStripLabel()
        Me.tscboGeneration = New System.Windows.Forms.ToolStripComboBox()
        Me.tslFrameSize = New System.Windows.Forms.ToolStripLabel()
        Me.tscboFrameSize = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbExportWord = New System.Windows.Forms.ToolStripButton()
        Me.tsbPdf = New System.Windows.Forms.ToolStripButton()
        Me.tsbPrintTree = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsbSelectTree = New System.Windows.Forms.ToolStripButton()
        Me.tsbMoveTree = New System.Windows.Forms.ToolStripButton()
        Me.tsbViewTree = New System.Windows.Forms.ToolStripButton()
        Me.tsbEditTree = New System.Windows.Forms.ToolStripButton()
        Me.tsbSearch = New System.Windows.Forms.ToolStripButton()
        Me.tsbPrint = New System.Windows.Forms.ToolStripButton()
        Me.sttBarMain = New System.Windows.Forms.StatusStrip()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsslTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tooltipQickSearch = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnLastPage = New System.Windows.Forms.Button()
        Me.btnFirstPage = New System.Windows.Forms.Button()
        Me.btnNextPage = New System.Windows.Forms.Button()
        Me.btnPrePage = New System.Windows.Forms.Button()
        Me.btnQickSearch = New System.Windows.Forms.Button()
        Me.tmrMain = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.splMain = New phv.SplitContainerExt()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvMain = New System.Windows.Forms.DataGridView()
        Me.clmGender = New System.Windows.Forms.DataGridViewImageColumn()
        Me.clmID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmLevel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmBirth = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmDeathDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MEMBER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GENDER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Deceased = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cbPages = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblAnniBirth = New System.Windows.Forms.LinkLabel()
        Me.lblNextBirthDay = New System.Windows.Forms.Label()
        Me.lblAnniDecease = New System.Windows.Forms.LinkLabel()
        Me.lblResultInfo = New System.Windows.Forms.Label()
        Me.grpQuickView = New System.Windows.Forms.GroupBox()
        Me.llbSearchAdv = New System.Windows.Forms.LinkLabel()
        Me.cboBranch = New System.Windows.Forms.ComboBox()
        Me.rdFemale = New System.Windows.Forms.RadioButton()
        Me.rdMale = New System.Windows.Forms.RadioButton()
        Me.rdGenderAll = New System.Windows.Forms.RadioButton()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnFamilyCard = New System.Windows.Forms.Panel()
        Me.pnFamilyTree = New System.Windows.Forms.Panel()
        Me.mnuMain.SuspendLayout()
        Me.tlsMain.SuspendLayout()
        Me.sttBarMain.SuspendLayout()
        CType(Me.splMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splMain.Panel1.SuspendLayout()
        Me.splMain.Panel2.SuspendLayout()
        Me.splMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.grpQuickView.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSystem, Me.tsmPersonal, Me.tsmView, Me.tsmFamily, Me.tsmSetting, Me.tsmEvents, Me.tsmSysData, Me.tsmActivate, Me.tsmHelp})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Padding = New System.Windows.Forms.Padding(7, 3, 0, 3)
        Me.mnuMain.Size = New System.Drawing.Size(1196, 26)
        Me.mnuMain.TabIndex = 0
        Me.mnuMain.Text = "Menu Chính"
        '
        'tsmSystem
        '
        Me.tsmSystem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSysUserInfo, Me.ToolStripSeparator2, Me.tsmSysDataBackup, Me.tsmSysDataRestore, Me.ToolStripSeparator1, Me.tsmExportToPdf, Me.tsmExportToWord, Me.ToolStripSeparator11, Me.tsmSysQuit})
        Me.tsmSystem.Name = "tsmSystem"
        Me.tsmSystem.Size = New System.Drawing.Size(62, 20)
        Me.tsmSystem.Text = "&Dữ liệu"
        '
        'tsmSysUserInfo
        '
        Me.tsmSysUserInfo.Name = "tsmSysUserInfo"
        Me.tsmSysUserInfo.Size = New System.Drawing.Size(203, 22)
        Me.tsmSysUserInfo.Text = "Tài khoản &người dùng"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(200, 6)
        '
        'tsmSysDataBackup
        '
        Me.tsmSysDataBackup.Image = CType(resources.GetObject("tsmSysDataBackup.Image"), System.Drawing.Image)
        Me.tsmSysDataBackup.Name = "tsmSysDataBackup"
        Me.tsmSysDataBackup.Size = New System.Drawing.Size(203, 22)
        Me.tsmSysDataBackup.Text = "&Sao lưu dữ liệu"
        '
        'tsmSysDataRestore
        '
        Me.tsmSysDataRestore.Image = CType(resources.GetObject("tsmSysDataRestore.Image"), System.Drawing.Image)
        Me.tsmSysDataRestore.Name = "tsmSysDataRestore"
        Me.tsmSysDataRestore.Size = New System.Drawing.Size(203, 22)
        Me.tsmSysDataRestore.Text = "&Khôi phục dữ liệu"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(200, 6)
        '
        'tsmExportToPdf
        '
        Me.tsmExportToPdf.Image = CType(resources.GetObject("tsmExportToPdf.Image"), System.Drawing.Image)
        Me.tsmExportToPdf.Name = "tsmExportToPdf"
        Me.tsmExportToPdf.Size = New System.Drawing.Size(203, 22)
        Me.tsmExportToPdf.Text = "Xuất dữ liệu ra &Pdf"
        '
        'tsmExportToWord
        '
        Me.tsmExportToWord.Image = CType(resources.GetObject("tsmExportToWord.Image"), System.Drawing.Image)
        Me.tsmExportToWord.Name = "tsmExportToWord"
        Me.tsmExportToWord.Size = New System.Drawing.Size(203, 22)
        Me.tsmExportToWord.Text = "&Xuất dữ liệu ra Word"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(200, 6)
        '
        'tsmSysQuit
        '
        Me.tsmSysQuit.Image = CType(resources.GetObject("tsmSysQuit.Image"), System.Drawing.Image)
        Me.tsmSysQuit.Name = "tsmSysQuit"
        Me.tsmSysQuit.Size = New System.Drawing.Size(203, 22)
        Me.tsmSysQuit.Text = "&Thoát"
        '
        'tsmPersonal
        '
        Me.tsmPersonal.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmPersonalInfo, Me.tsmPersonRel, Me.tsmPersonDelete})
        Me.tsmPersonal.Name = "tsmPersonal"
        Me.tsmPersonal.Size = New System.Drawing.Size(68, 20)
        Me.tsmPersonal.Text = "Cá &nhân"
        '
        'tsmPersonalInfo
        '
        Me.tsmPersonalInfo.Name = "tsmPersonalInfo"
        Me.tsmPersonalInfo.Size = New System.Drawing.Size(179, 22)
        Me.tsmPersonalInfo.Text = "&Thông tin cá nhân"
        '
        'tsmPersonRel
        '
        Me.tsmPersonRel.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmPersonalFa, Me.tsmPersonalSpouse, Me.tsmPersonalChild})
        Me.tsmPersonRel.Name = "tsmPersonRel"
        Me.tsmPersonRel.Size = New System.Drawing.Size(179, 22)
        Me.tsmPersonRel.Text = "Quan hệ &gia đình"
        '
        'tsmPersonalFa
        '
        Me.tsmPersonalFa.Name = "tsmPersonalFa"
        Me.tsmPersonalFa.Size = New System.Drawing.Size(133, 22)
        Me.tsmPersonalFa.Text = "&Bố mẹ"
        '
        'tsmPersonalSpouse
        '
        Me.tsmPersonalSpouse.Image = CType(resources.GetObject("tsmPersonalSpouse.Image"), System.Drawing.Image)
        Me.tsmPersonalSpouse.Name = "tsmPersonalSpouse"
        Me.tsmPersonalSpouse.Size = New System.Drawing.Size(133, 22)
        Me.tsmPersonalSpouse.Text = "&Vợ chồng"
        '
        'tsmPersonalChild
        '
        Me.tsmPersonalChild.Image = CType(resources.GetObject("tsmPersonalChild.Image"), System.Drawing.Image)
        Me.tsmPersonalChild.Name = "tsmPersonalChild"
        Me.tsmPersonalChild.Size = New System.Drawing.Size(133, 22)
        Me.tsmPersonalChild.Text = "&Con"
        '
        'tsmPersonDelete
        '
        Me.tsmPersonDelete.Name = "tsmPersonDelete"
        Me.tsmPersonDelete.Size = New System.Drawing.Size(179, 22)
        Me.tsmPersonDelete.Text = "Xóa thành viên"
        '
        'tsmView
        '
        Me.tsmView.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmFullScreen})
        Me.tsmView.Name = "tsmView"
        Me.tsmView.Size = New System.Drawing.Size(64, 20)
        Me.tsmView.Text = "&Hiển thị"
        '
        'tsmFullScreen
        '
        Me.tsmFullScreen.Name = "tsmFullScreen"
        Me.tsmFullScreen.Size = New System.Drawing.Size(188, 22)
        Me.tsmFullScreen.Text = "Hiện toàn &màn hình"
        '
        'tsmFamily
        '
        Me.tsmFamily.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmFamilyInfo, Me.tsmValue, Me.ToolStripSeparator14, Me.tsmFamilyNewMem, Me.tsmFamilyBranchMngt, Me.ToolStripSeparator3, Me.tsmFamilyBuild, Me.tsmFamilyShowTree, Me.ToolStripSeparator6, Me.tsmFamilyReport, Me.tsmFamilySearch})
        Me.tsmFamily.Name = "tsmFamily"
        Me.tsmFamily.Size = New System.Drawing.Size(65, 20)
        Me.tsmFamily.Text = "&Gia phả"
        '
        'tsmFamilyInfo
        '
        Me.tsmFamilyInfo.Name = "tsmFamilyInfo"
        Me.tsmFamilyInfo.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyInfo.Text = "&Về dòng họ"
        '
        'tsmValue
        '
        Me.tsmValue.Name = "tsmValue"
        Me.tsmValue.Size = New System.Drawing.Size(207, 22)
        Me.tsmValue.Text = "Quản lý tài sản &của họ"
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(204, 6)
        '
        'tsmFamilyNewMem
        '
        Me.tsmFamilyNewMem.Name = "tsmFamilyNewMem"
        Me.tsmFamilyNewMem.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyNewMem.Text = "&Thêm thành viên"
        '
        'tsmFamilyBranchMngt
        '
        Me.tsmFamilyBranchMngt.Image = CType(resources.GetObject("tsmFamilyBranchMngt.Image"), System.Drawing.Image)
        Me.tsmFamilyBranchMngt.Name = "tsmFamilyBranchMngt"
        Me.tsmFamilyBranchMngt.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyBranchMngt.Text = "Quản lý thông tin chi"
        Me.tsmFamilyBranchMngt.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(204, 6)
        '
        'tsmFamilyBuild
        '
        Me.tsmFamilyBuild.Image = CType(resources.GetObject("tsmFamilyBuild.Image"), System.Drawing.Image)
        Me.tsmFamilyBuild.Name = "tsmFamilyBuild"
        Me.tsmFamilyBuild.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyBuild.Text = "&Xây dựng phả hệ"
        '
        'tsmFamilyShowTree
        '
        Me.tsmFamilyShowTree.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmTreeFull, Me.tsmTreeCompact})
        Me.tsmFamilyShowTree.Image = CType(resources.GetObject("tsmFamilyShowTree.Image"), System.Drawing.Image)
        Me.tsmFamilyShowTree.Name = "tsmFamilyShowTree"
        Me.tsmFamilyShowTree.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyShowTree.Text = "&Hiển thị phả hệ"
        '
        'tsmTreeFull
        '
        Me.tsmTreeFull.Name = "tsmTreeFull"
        Me.tsmTreeFull.Size = New System.Drawing.Size(164, 22)
        Me.tsmTreeFull.Text = "Vẽ cây đầy đủ"
        Me.tsmTreeFull.Visible = False
        '
        'tsmTreeCompact
        '
        Me.tsmTreeCompact.Name = "tsmTreeCompact"
        Me.tsmTreeCompact.Size = New System.Drawing.Size(164, 22)
        Me.tsmTreeCompact.Text = "Vẽ cây thu gọn"
        Me.tsmTreeCompact.Visible = False
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(204, 6)
        '
        'tsmFamilyReport
        '
        Me.tsmFamilyReport.Name = "tsmFamilyReport"
        Me.tsmFamilyReport.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilyReport.Text = "&Thống kê"
        '
        'tsmFamilySearch
        '
        Me.tsmFamilySearch.Image = CType(resources.GetObject("tsmFamilySearch.Image"), System.Drawing.Image)
        Me.tsmFamilySearch.Name = "tsmFamilySearch"
        Me.tsmFamilySearch.Size = New System.Drawing.Size(207, 22)
        Me.tsmFamilySearch.Text = "Tìm kiếm "
        '
        'tsmSetting
        '
        Me.tsmSetting.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSettingDetail})
        Me.tsmSetting.Name = "tsmSetting"
        Me.tsmSetting.Size = New System.Drawing.Size(69, 20)
        Me.tsmSetting.Text = "&Thiết lập"
        '
        'tsmSettingDetail
        '
        Me.tsmSettingDetail.Image = Global.phv.My.Resources.Resources.Toolbar_setting1
        Me.tsmSettingDetail.Name = "tsmSettingDetail"
        Me.tsmSettingDetail.Size = New System.Drawing.Size(125, 22)
        Me.tsmSettingDetail.Text = "Thiết &lập"
        '
        'tsmEvents
        '
        Me.tsmEvents.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmDieDate, Me.tsmBirthday, Me.tsmWedding, Me.tsmAllEvent})
        Me.tsmEvents.Name = "tsmEvents"
        Me.tsmEvents.Size = New System.Drawing.Size(66, 20)
        Me.tsmEvents.Text = "&Sự kiện"
        '
        'tsmDieDate
        '
        Me.tsmDieDate.Image = Global.phv.My.Resources.Resources.Toolbar_calendar
        Me.tsmDieDate.Name = "tsmDieDate"
        Me.tsmDieDate.Size = New System.Drawing.Size(163, 22)
        Me.tsmDieDate.Text = "Ngày &giỗ"
        '
        'tsmBirthday
        '
        Me.tsmBirthday.Image = CType(resources.GetObject("tsmBirthday.Image"), System.Drawing.Image)
        Me.tsmBirthday.Name = "tsmBirthday"
        Me.tsmBirthday.Size = New System.Drawing.Size(163, 22)
        Me.tsmBirthday.Text = "Ngày &sinh nhật"
        '
        'tsmWedding
        '
        Me.tsmWedding.Image = CType(resources.GetObject("tsmWedding.Image"), System.Drawing.Image)
        Me.tsmWedding.Name = "tsmWedding"
        Me.tsmWedding.Size = New System.Drawing.Size(163, 22)
        Me.tsmWedding.Text = "Ngày &cưới"
        '
        'tsmAllEvent
        '
        Me.tsmAllEvent.Name = "tsmAllEvent"
        Me.tsmAllEvent.Size = New System.Drawing.Size(163, 22)
        Me.tsmAllEvent.Text = "&Tất cả"
        '
        'tsmSysData
        '
        Me.tsmSysData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmSysNow, Me.tsmSysSetting, Me.ToolStripSeparator13, Me.tsmMySite})
        Me.tsmSysData.Name = "tsmSysData"
        Me.tsmSysData.Size = New System.Drawing.Size(112, 20)
        Me.tsmSysData.Text = "Đồng bộ dữ &liệu"
        '
        'tsmSysNow
        '
        Me.tsmSysNow.Image = CType(resources.GetObject("tsmSysNow.Image"), System.Drawing.Image)
        Me.tsmSysNow.Name = "tsmSysNow"
        Me.tsmSysNow.Size = New System.Drawing.Size(178, 22)
        Me.tsmSysNow.Text = "Đồng bộ &ngay"
        '
        'tsmSysSetting
        '
        Me.tsmSysSetting.Image = CType(resources.GetObject("tsmSysSetting.Image"), System.Drawing.Image)
        Me.tsmSysSetting.Name = "tsmSysSetting"
        Me.tsmSysSetting.Size = New System.Drawing.Size(178, 22)
        Me.tsmSysSetting.Text = "Thiết lập đồng &bộ"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(175, 6)
        '
        'tsmMySite
        '
        Me.tsmMySite.Image = CType(resources.GetObject("tsmMySite.Image"), System.Drawing.Image)
        Me.tsmMySite.Name = "tsmMySite"
        Me.tsmMySite.Size = New System.Drawing.Size(178, 22)
        Me.tsmMySite.Text = "Trang &web của tôi"
        '
        'tsmHelp
        '
        Me.tsmHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmGuide, Me.tsmUpdate, Me.tsmHelpAbout})
        Me.tsmHelp.Name = "tsmHelp"
        Me.tsmHelp.Size = New System.Drawing.Size(68, 20)
        Me.tsmHelp.Text = "&Trợ giúp"
        '
        'tsmGuide
        '
        Me.tsmGuide.Image = Global.phv.My.Resources.Resources.Help
        Me.tsmGuide.Name = "tsmGuide"
        Me.tsmGuide.Size = New System.Drawing.Size(239, 22)
        Me.tsmGuide.Text = "&Hướng dẫn dùng phần mềm"
        '
        'tsmUpdate
        '
        Me.tsmUpdate.Image = Global.phv.My.Resources.Resources.update_icon
        Me.tsmUpdate.Name = "tsmUpdate"
        Me.tsmUpdate.Size = New System.Drawing.Size(239, 22)
        Me.tsmUpdate.Text = "&Cập nhật phần mềm mới"
        '
        'tsmHelpAbout
        '
        Me.tsmHelpAbout.Image = CType(resources.GetObject("tsmHelpAbout.Image"), System.Drawing.Image)
        Me.tsmHelpAbout.Name = "tsmHelpAbout"
        Me.tsmHelpAbout.Size = New System.Drawing.Size(239, 22)
        Me.tsmHelpAbout.Text = "&Về chương trình PHV 2.0"
        '
        'tsmActivate
        '
        Me.tsmActivate.Name = "tsmActivate"
        Me.tsmActivate.Size = New System.Drawing.Size(75, 20)
        Me.tsmActivate.Text = "Kích hoạt"
        Me.tsmActivate.Visible = False
        '
        'tlsMain
        '
        Me.tlsMain.AutoSize = False
        Me.tlsMain.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.tlsMain.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.tlsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbOpen, Me.tsbSave, Me.ToolStripSeparator4, Me.tsbBack, Me.tsbForward, Me.tsbHome, Me.ToolStripSeparator5, Me.tsbFamilyBuild, Me.tsbRoot, Me.tsbFamilyShowTree, Me.ToolStripSeparator10, Me.tssEvent, Me.tsbAlbum, Me.tsbFamilyCalendar, Me.ToolStripSeparator7, Me.tsbSync, Me.tsbSetting, Me.ToolStripSeparator8, Me.tsbTreeView1, Me.tslGenerationNum, Me.tscboGeneration, Me.tslFrameSize, Me.tscboFrameSize, Me.ToolStripSeparator9, Me.tsbExportWord, Me.tsbPdf, Me.tsbPrintTree, Me.ToolStripSeparator12, Me.tsbSelectTree, Me.tsbMoveTree})
        Me.tlsMain.Location = New System.Drawing.Point(0, 26)
        Me.tlsMain.Name = "tlsMain"
        Me.tlsMain.Size = New System.Drawing.Size(1196, 52)
        Me.tlsMain.Stretch = True
        Me.tlsMain.TabIndex = 1
        Me.tlsMain.Text = "ToolStrip1"
        '
        'tsbOpen
        '
        Me.tsbOpen.AutoSize = False
        Me.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbOpen.Image = Global.phv.My.Resources.Resources.Toolbar_Open_TT
        Me.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbOpen.Name = "tsbOpen"
        Me.tsbOpen.Size = New System.Drawing.Size(50, 50)
        Me.tsbOpen.Text = "ToolStripButton3"
        Me.tsbOpen.ToolTipText = "Mở dữ liệu dòng tộc"
        '
        'tsbSave
        '
        Me.tsbSave.AutoSize = False
        Me.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSave.Image = Global.phv.My.Resources.Resources.Toolbar_Save_TT
        Me.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSave.Name = "tsbSave"
        Me.tsbSave.Size = New System.Drawing.Size(50, 50)
        Me.tsbSave.Text = "Lưu dữ liệu"
        Me.tsbSave.ToolTipText = "Thông tin người dùng"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 52)
        '
        'tsbBack
        '
        Me.tsbBack.AutoSize = False
        Me.tsbBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbBack.Image = Global.phv.My.Resources.Resources.Toolbar_Back_TT
        Me.tsbBack.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbBack.Name = "tsbBack"
        Me.tsbBack.Size = New System.Drawing.Size(50, 50)
        Me.tsbBack.ToolTipText = "Quay lại"
        '
        'tsbForward
        '
        Me.tsbForward.AutoSize = False
        Me.tsbForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbForward.Image = Global.phv.My.Resources.Resources.Toolbar_Forward_TT
        Me.tsbForward.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbForward.Name = "tsbForward"
        Me.tsbForward.Size = New System.Drawing.Size(50, 50)
        Me.tsbForward.Text = "Chuyển tiếp"
        Me.tsbForward.ToolTipText = "Thêm thành viên"
        '
        'tsbHome
        '
        Me.tsbHome.AutoSize = False
        Me.tsbHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbHome.Image = Global.phv.My.Resources.Resources.Toolbar_Home_TT
        Me.tsbHome.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbHome.Name = "tsbHome"
        Me.tsbHome.Size = New System.Drawing.Size(50, 50)
        Me.tsbHome.Text = "Home"
        Me.tsbHome.ToolTipText = "Thêm thành viên"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 52)
        '
        'tsbFamilyBuild
        '
        Me.tsbFamilyBuild.AutoSize = False
        Me.tsbFamilyBuild.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFamilyBuild.Image = Global.phv.My.Resources.Resources.Toolbar_Site_TT
        Me.tsbFamilyBuild.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFamilyBuild.Name = "tsbFamilyBuild"
        Me.tsbFamilyBuild.Size = New System.Drawing.Size(50, 50)
        Me.tsbFamilyBuild.Text = "Xây dựng phả hệ"
        '
        'tsbRoot
        '
        Me.tsbRoot.AutoSize = False
        Me.tsbRoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbRoot.Image = Global.phv.My.Resources.Resources.Binary_tree
        Me.tsbRoot.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbRoot.Name = "tsbRoot"
        Me.tsbRoot.Size = New System.Drawing.Size(50, 50)
        Me.tsbRoot.Text = "Hiển thị toàn gia tộc"
        Me.tsbRoot.Visible = False
        '
        'tsbFamilyShowTree
        '
        Me.tsbFamilyShowTree.AutoSize = False
        Me.tsbFamilyShowTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFamilyShowTree.Image = Global.phv.My.Resources.Resources.Toolbar_Tree_TT
        Me.tsbFamilyShowTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFamilyShowTree.Name = "tsbFamilyShowTree"
        Me.tsbFamilyShowTree.Size = New System.Drawing.Size(50, 50)
        Me.tsbFamilyShowTree.Text = "Hiển thị phả hệ"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(6, 52)
        '
        'tssEvent
        '
        Me.tssEvent.AutoSize = False
        Me.tssEvent.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmDieDate2, Me.tsmBirthday2, Me.tsmWedding2, Me.tsmAllEvent2})
        Me.tssEvent.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.tssEvent.Image = CType(resources.GetObject("tssEvent.Image"), System.Drawing.Image)
        Me.tssEvent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.tssEvent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tssEvent.Name = "tssEvent"
        Me.tssEvent.Size = New System.Drawing.Size(150, 50)
        Me.tssEvent.Text = "Giỗ tết trong họ"
        Me.tssEvent.ToolTipText = "Sự kiện trong họ"
        '
        'tsmDieDate2
        '
        Me.tsmDieDate2.CheckOnClick = True
        Me.tsmDieDate2.Image = CType(resources.GetObject("tsmDieDate2.Image"), System.Drawing.Image)
        Me.tsmDieDate2.Name = "tsmDieDate2"
        Me.tsmDieDate2.Size = New System.Drawing.Size(163, 22)
        Me.tsmDieDate2.Text = "Ngày &giỗ"
        '
        'tsmBirthday2
        '
        Me.tsmBirthday2.Image = CType(resources.GetObject("tsmBirthday2.Image"), System.Drawing.Image)
        Me.tsmBirthday2.Name = "tsmBirthday2"
        Me.tsmBirthday2.Size = New System.Drawing.Size(163, 22)
        Me.tsmBirthday2.Text = "Ngày &sinh nhật"
        '
        'tsmWedding2
        '
        Me.tsmWedding2.Image = CType(resources.GetObject("tsmWedding2.Image"), System.Drawing.Image)
        Me.tsmWedding2.Name = "tsmWedding2"
        Me.tsmWedding2.Size = New System.Drawing.Size(163, 22)
        Me.tsmWedding2.Text = "Ngày &cưới"
        '
        'tsmAllEvent2
        '
        Me.tsmAllEvent2.Name = "tsmAllEvent2"
        Me.tsmAllEvent2.Size = New System.Drawing.Size(163, 22)
        Me.tsmAllEvent2.Text = "&Tất cả"
        '
        'tsbAlbum
        '
        Me.tsbAlbum.AutoSize = False
        Me.tsbAlbum.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbAlbum.Image = CType(resources.GetObject("tsbAlbum.Image"), System.Drawing.Image)
        Me.tsbAlbum.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbAlbum.Name = "tsbAlbum"
        Me.tsbAlbum.Size = New System.Drawing.Size(50, 50)
        Me.tsbAlbum.Text = "Ảnh gia đình"
        '
        'tsbFamilyCalendar
        '
        Me.tsbFamilyCalendar.AutoSize = False
        Me.tsbFamilyCalendar.Image = CType(resources.GetObject("tsbFamilyCalendar.Image"), System.Drawing.Image)
        Me.tsbFamilyCalendar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFamilyCalendar.Name = "tsbFamilyCalendar"
        Me.tsbFamilyCalendar.Size = New System.Drawing.Size(100, 50)
        Me.tsbFamilyCalendar.Text = "Ngày giỗ"
        Me.tsbFamilyCalendar.Visible = False
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 52)
        '
        'tsbSync
        '
        Me.tsbSync.AutoSize = False
        Me.tsbSync.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSync.Image = Global.phv.My.Resources.Resources.Sync
        Me.tsbSync.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSync.Name = "tsbSync"
        Me.tsbSync.Size = New System.Drawing.Size(50, 50)
        Me.tsbSync.Text = "Lịch sự kiện"
        '
        'tsbSetting
        '
        Me.tsbSetting.AutoSize = False
        Me.tsbSetting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSetting.Image = Global.phv.My.Resources.Resources.Toolbar_setting
        Me.tsbSetting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSetting.Name = "tsbSetting"
        Me.tsbSetting.Size = New System.Drawing.Size(50, 50)
        Me.tsbSetting.Text = "Tùy chọn"
        Me.tsbSetting.ToolTipText = "Tùy chọn hiển thị cây phả hệ"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 52)
        Me.ToolStripSeparator8.Visible = False
        '
        'tsbTreeView1
        '
        Me.tsbTreeView1.AutoSize = False
        Me.tsbTreeView1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbTreeView1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbMenuTree1Basic, Me.tsbMenuTree1Open, Me.tsbMenuTree1Impact})
        Me.tsbTreeView1.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.tsbTreeView1.Image = Global.phv.My.Resources.Resources.FamilyGen
        Me.tsbTreeView1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbTreeView1.Name = "tsbTreeView1"
        Me.tsbTreeView1.Size = New System.Drawing.Size(50, 50)
        Me.tsbTreeView1.Text = "Các thể hiện"
        Me.tsbTreeView1.ToolTipText = "Cách thể hiện"
        '
        'tsbMenuTree1Basic
        '
        Me.tsbMenuTree1Basic.CheckOnClick = True
        Me.tsbMenuTree1Basic.Image = Global.phv.My.Resources.Resources.Couple
        Me.tsbMenuTree1Basic.Name = "tsbMenuTree1Basic"
        Me.tsbMenuTree1Basic.Size = New System.Drawing.Size(253, 22)
        Me.tsbMenuTree1Basic.Text = "Cây cơ bản (Tiết kiệm giấy vẽ)"
        '
        'tsbMenuTree1Open
        '
        Me.tsbMenuTree1Open.Image = Global.phv.My.Resources.Resources.Couple3
        Me.tsbMenuTree1Open.Name = "tsbMenuTree1Open"
        Me.tsbMenuTree1Open.Size = New System.Drawing.Size(253, 22)
        Me.tsbMenuTree1Open.Text = "Cây mở rộng (Chậm hơn)"
        '
        'tsbMenuTree1Impact
        '
        Me.tsbMenuTree1Impact.Name = "tsbMenuTree1Impact"
        Me.tsbMenuTree1Impact.Size = New System.Drawing.Size(253, 22)
        Me.tsbMenuTree1Impact.Text = "Cây thu gọn"
        Me.tsbMenuTree1Impact.Visible = False
        '
        'tslGenerationNum
        '
        Me.tslGenerationNum.Name = "tslGenerationNum"
        Me.tslGenerationNum.Size = New System.Drawing.Size(51, 49)
        Me.tslGenerationNum.Text = "Số đời:"
        Me.tslGenerationNum.ToolTipText = "Sối đời tối đa được vẽ tính từ thành viên đang được chọn"
        '
        'tscboGeneration
        '
        Me.tscboGeneration.AutoSize = False
        Me.tscboGeneration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tscboGeneration.DropDownWidth = 75
        Me.tscboGeneration.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tscboGeneration.Name = "tscboGeneration"
        Me.tscboGeneration.Size = New System.Drawing.Size(50, 24)
        Me.tscboGeneration.ToolTipText = "Sối đời tối đa được vẽ tính từ thành viên đang được chọn"
        '
        'tslFrameSize
        '
        Me.tslFrameSize.Name = "tslFrameSize"
        Me.tslFrameSize.Size = New System.Drawing.Size(45, 49)
        Me.tslFrameSize.Text = "Khung"
        Me.tslFrameSize.Visible = False
        '
        'tscboFrameSize
        '
        Me.tscboFrameSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.tscboFrameSize.Items.AddRange(New Object() {"Nhỏ", "Trung Bình", "Lớn", "Rất lớn"})
        Me.tscboFrameSize.Name = "tscboFrameSize"
        Me.tscboFrameSize.Size = New System.Drawing.Size(80, 52)
        Me.tscboFrameSize.Visible = False
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 52)
        Me.ToolStripSeparator9.Visible = False
        '
        'tsbExportWord
        '
        Me.tsbExportWord.AutoSize = False
        Me.tsbExportWord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbExportWord.Image = Global.phv.My.Resources.Resources.Toolbar_word_icon
        Me.tsbExportWord.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbExportWord.Name = "tsbExportWord"
        Me.tsbExportWord.Size = New System.Drawing.Size(50, 50)
        Me.tsbExportWord.Text = "Xuất khẩu dữ liệu"
        '
        'tsbPdf
        '
        Me.tsbPdf.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPdf.Image = Global.phv.My.Resources.Resources.Toolbar_pdf
        Me.tsbPdf.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPdf.Name = "tsbPdf"
        Me.tsbPdf.Size = New System.Drawing.Size(36, 49)
        Me.tsbPdf.Text = "Xuất dữ liệu thành viên ra file Pdf"
        '
        'tsbPrintTree
        '
        Me.tsbPrintTree.AutoSize = False
        Me.tsbPrintTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrintTree.Image = Global.phv.My.Resources.Resources.Toolbar_Printer
        Me.tsbPrintTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrintTree.Name = "tsbPrintTree"
        Me.tsbPrintTree.Size = New System.Drawing.Size(50, 50)
        Me.tsbPrintTree.ToolTipText = "In cây phả hệ"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(6, 52)
        '
        'tsbSelectTree
        '
        Me.tsbSelectTree.AutoSize = False
        Me.tsbSelectTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSelectTree.Image = Global.phv.My.Resources.Resources.Toolbar_cursor2
        Me.tsbSelectTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSelectTree.Name = "tsbSelectTree"
        Me.tsbSelectTree.Size = New System.Drawing.Size(50, 50)
        Me.tsbSelectTree.Text = "Lựa chọn"
        '
        'tsbMoveTree
        '
        Me.tsbMoveTree.AutoSize = False
        Me.tsbMoveTree.Checked = True
        Me.tsbMoveTree.CheckState = System.Windows.Forms.CheckState.Checked
        Me.tsbMoveTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbMoveTree.Image = Global.phv.My.Resources.Resources.Toolbar_cursor1
        Me.tsbMoveTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbMoveTree.Name = "tsbMoveTree"
        Me.tsbMoveTree.Size = New System.Drawing.Size(50, 50)
        Me.tsbMoveTree.Text = "Di chuyển"
        '
        'tsbViewTree
        '
        Me.tsbViewTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbViewTree.Image = CType(resources.GetObject("tsbViewTree.Image"), System.Drawing.Image)
        Me.tsbViewTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbViewTree.Name = "tsbViewTree"
        Me.tsbViewTree.Size = New System.Drawing.Size(36, 40)
        Me.tsbViewTree.Text = "Xem cây gia phả"
        Me.tsbViewTree.ToolTipText = "Hiển thị cây gia phả"
        '
        'tsbEditTree
        '
        Me.tsbEditTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbEditTree.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.tsbEditTree.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEditTree.Name = "tsbEditTree"
        Me.tsbEditTree.Size = New System.Drawing.Size(23, 40)
        Me.tsbEditTree.Text = "ToolStripButton3"
        Me.tsbEditTree.ToolTipText = "Xây dựng cây gia phả"
        '
        'tsbSearch
        '
        Me.tsbSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSearch.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.tsbSearch.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbSearch.Name = "tsbSearch"
        Me.tsbSearch.Size = New System.Drawing.Size(36, 40)
        Me.tsbSearch.Text = "Tìm kiếm"
        Me.tsbSearch.ToolTipText = "Tìm kiếm đầy đủ"
        '
        'tsbPrint
        '
        Me.tsbPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrint.Image = CType(resources.GetObject("tsbPrint.Image"), System.Drawing.Image)
        Me.tsbPrint.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrint.Name = "tsbPrint"
        Me.tsbPrint.Size = New System.Drawing.Size(36, 40)
        Me.tsbPrint.Text = "ToolStripButton1"
        '
        'sttBarMain
        '
        Me.sttBarMain.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.sttBarMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblStatus})
        Me.sttBarMain.Location = New System.Drawing.Point(0, 712)
        Me.sttBarMain.Name = "sttBarMain"
        Me.sttBarMain.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me.sttBarMain.Size = New System.Drawing.Size(1196, 24)
        Me.sttBarMain.SizingGrip = False
        Me.sttBarMain.TabIndex = 2
        Me.sttBarMain.Text = "StatusStrip1"
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(69, 19)
        Me.lblStatus.Text = "0 người"
        '
        'tsslTime
        '
        Me.tsslTime.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.tsslTime.Name = "tsslTime"
        Me.tsslTime.Size = New System.Drawing.Size(0, 17)
        '
        'btnLastPage
        '
        Me.btnLastPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLastPage.Location = New System.Drawing.Point(244, 8)
        Me.btnLastPage.Name = "btnLastPage"
        Me.btnLastPage.Size = New System.Drawing.Size(37, 23)
        Me.btnLastPage.TabIndex = 24
        Me.btnLastPage.Text = ">>"
        Me.tooltipQickSearch.SetToolTip(Me.btnLastPage, "Trang cuối")
        Me.btnLastPage.UseVisualStyleBackColor = True
        '
        'btnFirstPage
        '
        Me.btnFirstPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFirstPage.Location = New System.Drawing.Point(106, 8)
        Me.btnFirstPage.Name = "btnFirstPage"
        Me.btnFirstPage.Size = New System.Drawing.Size(37, 23)
        Me.btnFirstPage.TabIndex = 20
        Me.btnFirstPage.Text = "<<"
        Me.tooltipQickSearch.SetToolTip(Me.btnFirstPage, "Trang đầu")
        Me.btnFirstPage.UseVisualStyleBackColor = True
        '
        'btnNextPage
        '
        Me.btnNextPage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNextPage.Location = New System.Drawing.Point(220, 8)
        Me.btnNextPage.Name = "btnNextPage"
        Me.btnNextPage.Size = New System.Drawing.Size(18, 23)
        Me.btnNextPage.TabIndex = 23
        Me.btnNextPage.Text = ">"
        Me.tooltipQickSearch.SetToolTip(Me.btnNextPage, "Trang sau")
        Me.btnNextPage.UseVisualStyleBackColor = True
        '
        'btnPrePage
        '
        Me.btnPrePage.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrePage.Location = New System.Drawing.Point(151, 8)
        Me.btnPrePage.Name = "btnPrePage"
        Me.btnPrePage.Size = New System.Drawing.Size(18, 23)
        Me.btnPrePage.TabIndex = 21
        Me.btnPrePage.Text = "<"
        Me.tooltipQickSearch.SetToolTip(Me.btnPrePage, "Trang trước")
        Me.btnPrePage.UseVisualStyleBackColor = True
        '
        'btnQickSearch
        '
        Me.btnQickSearch.BackColor = System.Drawing.Color.White
        Me.btnQickSearch.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnQickSearch.ForeColor = System.Drawing.Color.Black
        Me.btnQickSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnQickSearch.Location = New System.Drawing.Point(392, 26)
        Me.btnQickSearch.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnQickSearch.Name = "btnQickSearch"
        Me.btnQickSearch.Size = New System.Drawing.Size(83, 30)
        Me.btnQickSearch.TabIndex = 2
        Me.btnQickSearch.Text = "Tìm &kiếm"
        Me.tooltipQickSearch.SetToolTip(Me.btnQickSearch, "Tìm kiếm")
        Me.btnQickSearch.UseVisualStyleBackColor = False
        '
        'tmrMain
        '
        Me.tmrMain.Interval = 1000
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(36, 40)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(36, 40)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'splMain
        '
        Me.splMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splMain.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.splMain.Location = New System.Drawing.Point(0, 78)
        Me.splMain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.splMain.Name = "splMain"
        '
        'splMain.Panel1
        '
        Me.splMain.Panel1.AutoScroll = True
        Me.splMain.Panel1.BackColor = System.Drawing.Color.White
        Me.splMain.Panel1.Controls.Add(Me.Panel1)
        Me.splMain.Panel1.Controls.Add(Me.grpQuickView)
        Me.splMain.Panel1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.splMain.Panel1MinSize = 0
        '
        'splMain.Panel2
        '
        Me.splMain.Panel2.AutoScroll = True
        Me.splMain.Panel2.BackColor = System.Drawing.Color.White
        Me.splMain.Panel2.Controls.Add(Me.pnFamilyCard)
        Me.splMain.Panel2.Controls.Add(Me.pnFamilyTree)
        Me.splMain.Panel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.splMain.Panel2MinSize = 900
        Me.splMain.Size = New System.Drawing.Size(1196, 634)
        Me.splMain.SplitterDistance = 286
        Me.splMain.SplitterWidth = 10
        Me.splMain.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvMain)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lblAnniBirth)
        Me.Panel1.Controls.Add(Me.lblNextBirthDay)
        Me.Panel1.Controls.Add(Me.lblAnniDecease)
        Me.Panel1.Controls.Add(Me.lblResultInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 88)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(286, 546)
        Me.Panel1.TabIndex = 4
        '
        'dgvMain
        '
        Me.dgvMain.AllowDrop = True
        Me.dgvMain.AllowUserToAddRows = False
        Me.dgvMain.AllowUserToDeleteRows = False
        Me.dgvMain.AllowUserToResizeRows = False
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.dgvMain.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMain.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMain.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmGender, Me.clmID, Me.clmName, Me.clmLevel, Me.clmBirth, Me.clmDeathDate, Me.MEMBER_ID, Me.GENDER, Me.Deceased})
        Me.dgvMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMain.Location = New System.Drawing.Point(0, 0)
        Me.dgvMain.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dgvMain.MultiSelect = False
        Me.dgvMain.Name = "dgvMain"
        Me.dgvMain.ReadOnly = True
        Me.dgvMain.RowHeadersVisible = False
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.dgvMain.RowsDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvMain.RowTemplate.Height = 21
        Me.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMain.Size = New System.Drawing.Size(286, 426)
        Me.dgvMain.TabIndex = 19
        Me.dgvMain.TabStop = False
        '
        'clmGender
        '
        Me.clmGender.DataPropertyName = "GT"
        Me.clmGender.HeaderText = "GT"
        Me.clmGender.MinimumWidth = 30
        Me.clmGender.Name = "clmGender"
        Me.clmGender.ReadOnly = True
        Me.clmGender.Width = 30
        '
        'clmID
        '
        Me.clmID.DataPropertyName = "STT"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.clmID.DefaultCellStyle = DataGridViewCellStyle8
        Me.clmID.HeaderText = "STT"
        Me.clmID.MinimumWidth = 40
        Me.clmID.Name = "clmID"
        Me.clmID.ReadOnly = True
        Me.clmID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmID.Width = 40
        '
        'clmName
        '
        Me.clmName.DataPropertyName = "FULL_NAME"
        Me.clmName.HeaderText = "Họ và tên"
        Me.clmName.MinimumWidth = 175
        Me.clmName.Name = "clmName"
        Me.clmName.ReadOnly = True
        Me.clmName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmName.Width = 200
        '
        'clmLevel
        '
        Me.clmLevel.DataPropertyName = "LEVEL"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.clmLevel.DefaultCellStyle = DataGridViewCellStyle9
        Me.clmLevel.HeaderText = "Đời"
        Me.clmLevel.MinimumWidth = 45
        Me.clmLevel.Name = "clmLevel"
        Me.clmLevel.ReadOnly = True
        Me.clmLevel.Width = 45
        '
        'clmBirth
        '
        Me.clmBirth.DataPropertyName = "BDATE"
        Me.clmBirth.HeaderText = "Ngày sinh"
        Me.clmBirth.MinimumWidth = 90
        Me.clmBirth.Name = "clmBirth"
        Me.clmBirth.ReadOnly = True
        Me.clmBirth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmBirth.Width = 90
        '
        'clmDeathDate
        '
        Me.clmDeathDate.DataPropertyName = "DDATE"
        Me.clmDeathDate.HeaderText = "Ngày mất"
        Me.clmDeathDate.MinimumWidth = 90
        Me.clmDeathDate.Name = "clmDeathDate"
        Me.clmDeathDate.ReadOnly = True
        Me.clmDeathDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmDeathDate.Width = 90
        '
        'MEMBER_ID
        '
        Me.MEMBER_ID.DataPropertyName = "MEMBER_ID"
        Me.MEMBER_ID.HeaderText = "Member ID"
        Me.MEMBER_ID.Name = "MEMBER_ID"
        Me.MEMBER_ID.ReadOnly = True
        Me.MEMBER_ID.Visible = False
        '
        'GENDER
        '
        Me.GENDER.DataPropertyName = "GENDER"
        Me.GENDER.HeaderText = "Gender"
        Me.GENDER.Name = "GENDER"
        Me.GENDER.ReadOnly = True
        Me.GENDER.Visible = False
        '
        'Deceased
        '
        Me.Deceased.DataPropertyName = "DECEASED"
        Me.Deceased.HeaderText = "Deceased"
        Me.Deceased.Name = "Deceased"
        Me.Deceased.ReadOnly = True
        Me.Deceased.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cbPages)
        Me.Panel2.Controls.Add(Me.btnLastPage)
        Me.Panel2.Controls.Add(Me.btnFirstPage)
        Me.Panel2.Controls.Add(Me.btnNextPage)
        Me.Panel2.Controls.Add(Me.btnPrePage)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 426)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(286, 35)
        Me.Panel2.TabIndex = 20
        Me.Panel2.Visible = False
        '
        'cbPages
        '
        Me.cbPages.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbPages.FormattingEnabled = True
        Me.cbPages.Location = New System.Drawing.Point(174, 7)
        Me.cbPages.Name = "cbPages"
        Me.cbPages.Size = New System.Drawing.Size(40, 24)
        Me.cbPages.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label4.Location = New System.Drawing.Point(0, 461)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(286, 16)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Sinh nhật gần nhất:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label4.Visible = False
        '
        'lblAnniBirth
        '
        Me.lblAnniBirth.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblAnniBirth.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lblAnniBirth.Location = New System.Drawing.Point(0, 477)
        Me.lblAnniBirth.Name = "lblAnniBirth"
        Me.lblAnniBirth.Size = New System.Drawing.Size(286, 16)
        Me.lblAnniBirth.TabIndex = 28
        Me.lblAnniBirth.Visible = False
        '
        'lblNextBirthDay
        '
        Me.lblNextBirthDay.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblNextBirthDay.Location = New System.Drawing.Point(0, 493)
        Me.lblNextBirthDay.Name = "lblNextBirthDay"
        Me.lblNextBirthDay.Size = New System.Drawing.Size(286, 16)
        Me.lblNextBirthDay.TabIndex = 18
        Me.lblNextBirthDay.Text = "Ngày giỗ gần nhất: "
        Me.lblNextBirthDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblNextBirthDay.Visible = False
        '
        'lblAnniDecease
        '
        Me.lblAnniDecease.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblAnniDecease.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lblAnniDecease.Location = New System.Drawing.Point(0, 509)
        Me.lblAnniDecease.Name = "lblAnniDecease"
        Me.lblAnniDecease.Size = New System.Drawing.Size(286, 16)
        Me.lblAnniDecease.TabIndex = 29
        Me.lblAnniDecease.Visible = False
        '
        'lblResultInfo
        '
        Me.lblResultInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblResultInfo.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblResultInfo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblResultInfo.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblResultInfo.Location = New System.Drawing.Point(0, 525)
        Me.lblResultInfo.Name = "lblResultInfo"
        Me.lblResultInfo.Size = New System.Drawing.Size(286, 21)
        Me.lblResultInfo.TabIndex = 17
        Me.lblResultInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblResultInfo.Visible = False
        '
        'grpQuickView
        '
        Me.grpQuickView.Controls.Add(Me.llbSearchAdv)
        Me.grpQuickView.Controls.Add(Me.cboBranch)
        Me.grpQuickView.Controls.Add(Me.rdFemale)
        Me.grpQuickView.Controls.Add(Me.rdMale)
        Me.grpQuickView.Controls.Add(Me.rdGenderAll)
        Me.grpQuickView.Controls.Add(Me.btnQickSearch)
        Me.grpQuickView.Controls.Add(Me.txtName)
        Me.grpQuickView.Controls.Add(Me.Label1)
        Me.grpQuickView.Controls.Add(Me.Label2)
        Me.grpQuickView.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpQuickView.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.grpQuickView.Location = New System.Drawing.Point(0, 0)
        Me.grpQuickView.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpQuickView.Name = "grpQuickView"
        Me.grpQuickView.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.grpQuickView.Size = New System.Drawing.Size(286, 88)
        Me.grpQuickView.TabIndex = 2
        Me.grpQuickView.TabStop = False
        Me.grpQuickView.Text = "Tìm kiếm"
        '
        'llbSearchAdv
        '
        Me.llbSearchAdv.AutoSize = True
        Me.llbSearchAdv.Location = New System.Drawing.Point(233, 62)
        Me.llbSearchAdv.Name = "llbSearchAdv"
        Me.llbSearchAdv.Size = New System.Drawing.Size(86, 16)
        Me.llbSearchAdv.TabIndex = 151
        Me.llbSearchAdv.TabStop = True
        Me.llbSearchAdv.Text = "Tìm &nâng cao"
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(248, 31)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(41, 24)
        Me.cboBranch.TabIndex = 150
        Me.cboBranch.Visible = False
        '
        'rdFemale
        '
        Me.rdFemale.AutoSize = True
        Me.rdFemale.Location = New System.Drawing.Point(191, 60)
        Me.rdFemale.Name = "rdFemale"
        Me.rdFemale.Size = New System.Drawing.Size(44, 20)
        Me.rdFemale.TabIndex = 5
        Me.rdFemale.TabStop = True
        Me.rdFemale.Text = "Nữ"
        Me.rdFemale.UseVisualStyleBackColor = True
        '
        'rdMale
        '
        Me.rdMale.AutoSize = True
        Me.rdMale.Location = New System.Drawing.Point(138, 60)
        Me.rdMale.Name = "rdMale"
        Me.rdMale.Size = New System.Drawing.Size(53, 20)
        Me.rdMale.TabIndex = 4
        Me.rdMale.TabStop = True
        Me.rdMale.Text = "Nam"
        Me.rdMale.UseVisualStyleBackColor = True
        '
        'rdGenderAll
        '
        Me.rdGenderAll.AutoSize = True
        Me.rdGenderAll.Checked = True
        Me.rdGenderAll.Location = New System.Drawing.Point(79, 60)
        Me.rdGenderAll.Name = "rdGenderAll"
        Me.rdGenderAll.Size = New System.Drawing.Size(62, 20)
        Me.rdGenderAll.TabIndex = 3
        Me.rdGenderAll.TabStop = True
        Me.rdGenderAll.Text = "Tất cả"
        Me.rdGenderAll.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtName.Location = New System.Drawing.Point(79, 31)
        Me.txtName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(163, 22)
        Me.txtName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Giới tính :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Tên :"
        '
        'pnFamilyCard
        '
        Me.pnFamilyCard.AutoSize = True
        Me.pnFamilyCard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnFamilyCard.BackColor = System.Drawing.Color.White
        Me.pnFamilyCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.pnFamilyCard.Location = New System.Drawing.Point(244, 203)
        Me.pnFamilyCard.Name = "pnFamilyCard"
        Me.pnFamilyCard.Size = New System.Drawing.Size(0, 0)
        Me.pnFamilyCard.TabIndex = 4
        Me.pnFamilyCard.Visible = False
        '
        'pnFamilyTree
        '
        Me.pnFamilyTree.AutoScroll = True
        Me.pnFamilyTree.AutoSize = True
        Me.pnFamilyTree.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnFamilyTree.BackColor = System.Drawing.Color.White
        Me.pnFamilyTree.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.pnFamilyTree.Location = New System.Drawing.Point(131, 357)
        Me.pnFamilyTree.Name = "pnFamilyTree"
        Me.pnFamilyTree.Size = New System.Drawing.Size(0, 0)
        Me.pnFamilyTree.TabIndex = 4
        Me.pnFamilyTree.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1196, 736)
        Me.Controls.Add(Me.splMain)
        Me.Controls.Add(Me.tlsMain)
        Me.Controls.Add(Me.sttBarMain)
        Me.Controls.Add(Me.mnuMain)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.mnuMain
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MinimumSize = New System.Drawing.Size(1163, 764)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Phả Hệ Việt 2.0"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.tlsMain.ResumeLayout(False)
        Me.tlsMain.PerformLayout()
        Me.sttBarMain.ResumeLayout(False)
        Me.sttBarMain.PerformLayout()
        Me.splMain.Panel1.ResumeLayout(False)
        Me.splMain.Panel2.ResumeLayout(False)
        Me.splMain.Panel2.PerformLayout()
        CType(Me.splMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.grpQuickView.ResumeLayout(False)
        Me.grpQuickView.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents tlsMain As System.Windows.Forms.ToolStrip
    Friend WithEvents sttBarMain As System.Windows.Forms.StatusStrip
    Friend WithEvents tsbViewTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbEditTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbSearch As System.Windows.Forms.ToolStripButton
    Friend WithEvents tooltipQickSearch As System.Windows.Forms.ToolTip
    Friend WithEvents tsbPrint As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsslTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tmrMain As System.Windows.Forms.Timer
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmSystem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSysUserInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmSysDataBackup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSysDataRestore As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmSysQuit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamily As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonalInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonRel As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonalFa As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonalSpouse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmPersonalChild As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamilyInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamilyNewMem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmFamilyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbHome As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmSetting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSettingDetail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamilySearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbBack As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmFamilyBuild As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamilyShowTree As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbFamilyBuild As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbFamilyShowTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbPrintTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmPersonDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnFamilyTree As System.Windows.Forms.Panel
    Friend WithEvents splMain As phv.SplitContainerExt
    Friend WithEvents grpQuickView As System.Windows.Forms.GroupBox
    Friend WithEvents rdFemale As System.Windows.Forms.RadioButton
    Friend WithEvents rdMale As System.Windows.Forms.RadioButton
    Friend WithEvents rdGenderAll As System.Windows.Forms.RadioButton
    Friend WithEvents btnQickSearch As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvMain As System.Windows.Forms.DataGridView
    Friend WithEvents pnFamilyCard As System.Windows.Forms.Panel
    Friend WithEvents lblNextBirthDay As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblAnniBirth As System.Windows.Forms.LinkLabel
    Friend WithEvents tsbRoot As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmTreeFull As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmTreeCompact As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmGuide As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbSetting As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbExportWord As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmActivate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents clmGender As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents clmID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmBirth As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmDeathDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MEMBER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GENDER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Deceased As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnLastPage As System.Windows.Forms.Button
    Friend WithEvents btnNextPage As System.Windows.Forms.Button
    Friend WithEvents btnPrePage As System.Windows.Forms.Button
    Friend WithEvents btnFirstPage As System.Windows.Forms.Button
    Friend WithEvents cbPages As System.Windows.Forms.ComboBox
    Friend WithEvents tslGenerationNum As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tscboGeneration As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsbSelectTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbMoveTree As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbTreeView1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents tsbMenuTree1Basic As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbMenuTree1Open As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbMenuTree1Impact As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFamilyBranchMngt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbFamilyCalendar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tslFrameSize As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tscboFrameSize As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents tsbPdf As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblAnniDecease As System.Windows.Forms.LinkLabel
    Friend WithEvents lblResultInfo As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents tsbForward As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbSync As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmExportToWord As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmExportToPdf As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tsmUpdate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents llbSearchAdv As System.Windows.Forms.LinkLabel
    Friend WithEvents tsbAlbum As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsmSysData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSysNow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSysSetting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmMySite As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmFullScreen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmEvents As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmDieDate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmBirthday As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWedding As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAllEvent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tssEvent As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents tsmDieDate2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmBirthday2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmWedding2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmAllEvent2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmValue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
End Class
