﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActiveKey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActiveKey))
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtKey1 = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnActive = New System.Windows.Forms.Button()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtKey3 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtKey2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtKey4 = New System.Windows.Forms.TextBox()
        Me.txtKey5 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnTrial = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(118, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(271, 44)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Kích hoạt sản phẩm"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(135, 265)
        Me.txtPhone.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPhone.MaxLength = 20
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(156, 22)
        Me.txtPhone.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 268)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 16)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Điện thoại"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 236)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 16)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Ngày Sinh"
        '
        'txtKey1
        '
        Me.txtKey1.Location = New System.Drawing.Point(135, 297)
        Me.txtKey1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtKey1.MaxLength = 4
        Me.txtKey1.Name = "txtKey1"
        Me.txtKey1.Size = New System.Drawing.Size(44, 22)
        Me.txtKey1.TabIndex = 3
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(135, 201)
        Me.txtName.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(254, 22)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(45, 300)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Mã sản phẩm"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 204)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Họ Tên"
        '
        'btnActive
        '
        Me.btnActive.BackColor = System.Drawing.Color.White
        Me.btnActive.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.btnActive.Image = CType(resources.GetObject("btnActive.Image"), System.Drawing.Image)
        Me.btnActive.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnActive.Location = New System.Drawing.Point(251, 89)
        Me.btnActive.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnActive.Name = "btnActive"
        Me.btnActive.Size = New System.Drawing.Size(110, 44)
        Me.btnActive.TabIndex = 10
        Me.btnActive.Text = "Kích Hoạt"
        Me.btnActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnActive.UseVisualStyleBackColor = False
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "dd/MM/yyyy"
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(135, 233)
        Me.dtpDate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(156, 22)
        Me.dtpDate.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(180, 298)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 18)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "-"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(299, 300)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(13, 18)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "-"
        '
        'txtKey3
        '
        Me.txtKey3.Location = New System.Drawing.Point(254, 299)
        Me.txtKey3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtKey3.MaxLength = 4
        Me.txtKey3.Name = "txtKey3"
        Me.txtKey3.Size = New System.Drawing.Size(44, 22)
        Me.txtKey3.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(239, 299)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(13, 18)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "-"
        '
        'txtKey2
        '
        Me.txtKey2.Location = New System.Drawing.Point(195, 298)
        Me.txtKey2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtKey2.MaxLength = 4
        Me.txtKey2.Name = "txtKey2"
        Me.txtKey2.Size = New System.Drawing.Size(44, 22)
        Me.txtKey2.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(356, 300)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(13, 18)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "-"
        '
        'txtKey4
        '
        Me.txtKey4.Location = New System.Drawing.Point(311, 299)
        Me.txtKey4.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtKey4.MaxLength = 4
        Me.txtKey4.Name = "txtKey4"
        Me.txtKey4.Size = New System.Drawing.Size(44, 22)
        Me.txtKey4.TabIndex = 7
        '
        'txtKey5
        '
        Me.txtKey5.Location = New System.Drawing.Point(372, 300)
        Me.txtKey5.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtKey5.MaxLength = 5
        Me.txtKey5.Name = "txtKey5"
        Me.txtKey5.Size = New System.Drawing.Size(52, 22)
        Me.txtKey5.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.Label10.Location = New System.Drawing.Point(-3, 220)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(472, 60)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Phiên bản dùng thử này lưu trữ được 30 thành viên. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Kích hoạt ngay để trải ngh" & _
    "iệm đầy đủ sản phẩm." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'btnTrial
        '
        Me.btnTrial.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.btnTrial.Image = CType(resources.GetObject("btnTrial.Image"), System.Drawing.Image)
        Me.btnTrial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTrial.Location = New System.Drawing.Point(367, 89)
        Me.btnTrial.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnTrial.Name = "btnTrial"
        Me.btnTrial.Size = New System.Drawing.Size(117, 44)
        Me.btnTrial.TabIndex = 11
        Me.btnTrial.Text = "Dùng Thử"
        Me.btnTrial.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTrial.UseVisualStyleBackColor = False
        '
        'frmActiveKey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(496, 391)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtKey5)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtKey4)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtKey2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtKey3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dtpDate)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtPhone)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtKey1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnTrial)
        Me.Controls.Add(Me.btnActive)
        Me.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmActiveKey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Phả hệ việt 2.0"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKey1 As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnActive As System.Windows.Forms.Button
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKey3 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtKey2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtKey4 As System.Windows.Forms.TextBox
    Friend WithEvents txtKey5 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnTrial As System.Windows.Forms.Button
End Class
