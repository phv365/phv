﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDrawTreeManual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDrawTreeManual))
        Me.chkSelectAll = New System.Windows.Forms.CheckBox()
        Me.grpSearch = New System.Windows.Forms.GroupBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.cboBranch = New System.Windows.Forms.ComboBox()
        Me.lblBranch = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblMenberName = New System.Windows.Forms.Label()
        Me.dgvData = New System.Windows.Forms.DataGridView()
        Me.Choose = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MemID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MemberName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Generation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Branch = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GenCount = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.grpSearch.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Checked = True
        Me.chkSelectAll.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSelectAll.Location = New System.Drawing.Point(31, 131)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(96, 17)
        Me.chkSelectAll.TabIndex = 7
        Me.chkSelectAll.Text = "Bỏ chọn tất cả"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'grpSearch
        '
        Me.grpSearch.Controls.Add(Me.btnExit)
        Me.grpSearch.Controls.Add(Me.btnExport)
        Me.grpSearch.Controls.Add(Me.btnSearch)
        Me.grpSearch.Controls.Add(Me.cboBranch)
        Me.grpSearch.Controls.Add(Me.lblBranch)
        Me.grpSearch.Controls.Add(Me.txtName)
        Me.grpSearch.Controls.Add(Me.lblMenberName)
        Me.grpSearch.Location = New System.Drawing.Point(14, 21)
        Me.grpSearch.Name = "grpSearch"
        Me.grpSearch.Size = New System.Drawing.Size(540, 104)
        Me.grpSearch.TabIndex = 5
        Me.grpSearch.TabStop = False
        '
        'btnExit
        '
        Me.btnExit.Image = Global.phv.My.Resources.Resources.back_24
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExit.Location = New System.Drawing.Point(332, 54)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(79, 38)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Trở về"
        Me.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Image = Global.phv.My.Resources.Resources.exit32
        Me.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExport.Location = New System.Drawing.Point(217, 54)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(107, 38)
        Me.btnExport.TabIndex = 7
        Me.btnExport.Text = "Xuất dữ liệu"
        Me.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Image = Global.phv.My.Resources.Resources.MemberSearch24
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(129, 54)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(81, 39)
        Me.btnSearch.TabIndex = 6
        Me.btnSearch.Text = "Tìm Kiếm"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'cboBranch
        '
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(362, 12)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(155, 21)
        Me.cboBranch.TabIndex = 5
        '
        'lblBranch
        '
        Me.lblBranch.AutoSize = True
        Me.lblBranch.Location = New System.Drawing.Point(317, 20)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(25, 13)
        Me.lblBranch.TabIndex = 4
        Me.lblBranch.Text = "Chi:"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(100, 14)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(190, 20)
        Me.txtName.TabIndex = 1
        '
        'lblMenberName
        '
        Me.lblMenberName.AutoSize = True
        Me.lblMenberName.Location = New System.Drawing.Point(10, 21)
        Me.lblMenberName.Name = "lblMenberName"
        Me.lblMenberName.Size = New System.Drawing.Size(87, 13)
        Me.lblMenberName.TabIndex = 0
        Me.lblMenberName.Text = "Tên Thành Viên:"
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvData.ColumnHeadersHeight = 30
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Choose, Me.MemID, Me.MemberName, Me.Generation, Me.Branch, Me.GenCount})
        Me.dgvData.Location = New System.Drawing.Point(15, 152)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(539, 413)
        Me.dgvData.TabIndex = 6
        '
        'Choose
        '
        Me.Choose.DataPropertyName = "Choose"
        Me.Choose.HeaderText = "Chọn"
        Me.Choose.Name = "Choose"
        Me.Choose.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Choose.Width = 40
        '
        'MemID
        '
        Me.MemID.DataPropertyName = "MEMBER_ID"
        Me.MemID.HeaderText = "ID"
        Me.MemID.Name = "MemID"
        Me.MemID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MemID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MemID.Visible = False
        Me.MemID.Width = 50
        '
        'MemberName
        '
        Me.MemberName.DataPropertyName = "Name"
        Me.MemberName.HeaderText = "Tên Thành Viên"
        Me.MemberName.Name = "MemberName"
        Me.MemberName.ReadOnly = True
        Me.MemberName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MemberName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MemberName.Width = 270
        '
        'Generation
        '
        Me.Generation.DataPropertyName = "Gen"
        Me.Generation.HeaderText = "Đời"
        Me.Generation.Name = "Generation"
        Me.Generation.ReadOnly = True
        Me.Generation.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Generation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Generation.Visible = False
        Me.Generation.Width = 50
        '
        'Branch
        '
        Me.Branch.DataPropertyName = "Branch"
        Me.Branch.HeaderText = "Chi Họ"
        Me.Branch.Name = "Branch"
        Me.Branch.ReadOnly = True
        Me.Branch.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Branch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Branch.Width = 120
        '
        'GenCount
        '
        Me.GenCount.DataPropertyName = "GenCount"
        Me.GenCount.HeaderText = "Số đời vẽ"
        Me.GenCount.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"})
        Me.GenCount.Name = "GenCount"
        Me.GenCount.Width = 80
        '
        'frmDrawTreeManual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(569, 587)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.grpSearch)
        Me.Controls.Add(Me.dgvData)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmDrawTreeManual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Xuất dữ liệu thành viên"
        Me.grpSearch.ResumeLayout(False)
        Me.grpSearch.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents grpSearch As System.Windows.Forms.GroupBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents lblMenberName As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents Choose As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents MemID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MemberName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Generation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Branch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GenCount As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
