<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOption))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudGeneration = New System.Windows.Forms.NumericUpDown()
        Me.grbFrame = New System.Windows.Forms.GroupBox()
        Me.cboFrameType = New System.Windows.Forms.ComboBox()
        Me.usrMemCard2 = New phv.usrMemberCard2()
        Me.usrMemCard1 = New phv.usrMemberCard1()
        Me.rdCard2 = New System.Windows.Forms.RadioButton()
        Me.rdCard1 = New System.Windows.Forms.RadioButton()
        Me.chkShowUnknownBirthDay = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ursLine = New phv.usrLine()
        Me.lblkChooseColor = New System.Windows.Forms.LinkLabel()
        Me.lblColor = New System.Windows.Forms.Label()
        Me.cboLineWeight = New System.Windows.Forms.ComboBox()
        Me.lblLineWeight = New System.Windows.Forms.Label()
        Me.rdFrameCompact = New System.Windows.Forms.RadioButton()
        Me.rdFrameFull = New System.Windows.Forms.RadioButton()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblVerCm = New System.Windows.Forms.Label()
        Me.lblHozCm = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nudVerBuffer = New System.Windows.Forms.NumericUpDown()
        Me.nudHozBuffer = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.chkDrawnMan = New System.Windows.Forms.CheckBox()
        Me.ChkDrawnWomen = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudGeneration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grbFrame.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudVerBuffer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudHozBuffer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.nudGeneration)
        Me.GroupBox1.Location = New System.Drawing.Point(265, 42)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(258, 52)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Số thế hệ (đời) hiển thị tối đa"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 14)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Hiển thị tối đa"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(149, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 14)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Thế hệ"
        '
        'nudGeneration
        '
        Me.nudGeneration.Location = New System.Drawing.Point(95, 20)
        Me.nudGeneration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudGeneration.Name = "nudGeneration"
        Me.nudGeneration.Size = New System.Drawing.Size(38, 20)
        Me.nudGeneration.TabIndex = 30
        Me.nudGeneration.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'grbFrame
        '
        Me.grbFrame.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grbFrame.Controls.Add(Me.cboFrameType)
        Me.grbFrame.Controls.Add(Me.usrMemCard2)
        Me.grbFrame.Controls.Add(Me.usrMemCard1)
        Me.grbFrame.Controls.Add(Me.rdCard2)
        Me.grbFrame.Controls.Add(Me.rdCard1)
        Me.grbFrame.Location = New System.Drawing.Point(12, 3)
        Me.grbFrame.Name = "grbFrame"
        Me.grbFrame.Size = New System.Drawing.Size(240, 451)
        Me.grbFrame.TabIndex = 0
        Me.grbFrame.TabStop = False
        Me.grbFrame.Text = "Khung vẽ"
        '
        'cboFrameType
        '
        Me.cboFrameType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFrameType.FormattingEnabled = True
        Me.cboFrameType.Location = New System.Drawing.Point(28, 22)
        Me.cboFrameType.Name = "cboFrameType"
        Me.cboFrameType.Size = New System.Drawing.Size(189, 22)
        Me.cboFrameType.TabIndex = 11
        '
        'usrMemCard2
        '
        Me.usrMemCard2.BackColor = System.Drawing.Color.Transparent
        Me.usrMemCard2.CardID = 0
        Me.usrMemCard2.CardLevel = 0
        Me.usrMemCard2.CardMouseDown = False
        Me.usrMemCard2.CardSelected = False
        Me.usrMemCard2.CardSize = phv.clsEnum.emCardSize.LARGE
        Me.usrMemCard2.DrawLv = 0
        Me.usrMemCard2.Enabled = False
        Me.usrMemCard2.Location = New System.Drawing.Point(29, 282)
        Me.usrMemCard2.Name = "usrMemCard2"
        Me.usrMemCard2.ParentID = -1
        Me.usrMemCard2.ShowAlias = True
        Me.usrMemCard2.ShowBirth = True
        Me.usrMemCard2.ShowDecease = True
        Me.usrMemCard2.ShowImage = True
        Me.usrMemCard2.ShowRemark = True
        Me.usrMemCard2.Size = New System.Drawing.Size(188, 145)
        Me.usrMemCard2.SpouseID = -1
        Me.usrMemCard2.TabIndex = 1
        '
        'usrMemCard1
        '
        Me.usrMemCard1.AliveStatus = False
        Me.usrMemCard1.AllowDrop = True
        Me.usrMemCard1.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.usrMemCard1.BackgroundImage = CType(resources.GetObject("usrMemCard1.BackgroundImage"), System.Drawing.Image)
        Me.usrMemCard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.usrMemCard1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.usrMemCard1.CardBackground = CType(resources.GetObject("usrMemCard1.CardBackground"), System.Drawing.Image)
        Me.usrMemCard1.CardBirth = "1900" & vbCrLf & "Mất: 20/10/2012"
        'Me.usrMemCard1.CardDeath = "Mất: 20/10/2012"
        Me.usrMemCard1.CardGender = 0
        Me.usrMemCard1.CardID = 0
        Me.usrMemCard1.CardImage = Nothing
        Me.usrMemCard1.CardImageLocation = Nothing
        Me.usrMemCard1.CardMouseDown = False
        Me.usrMemCard1.CardName = "Ten thanh vien (ten thuong goi)"
        Me.usrMemCard1.CardSelected = False
        Me.usrMemCard1.CardSize = phv.clsEnum.emCardSize.LARGE
        Me.usrMemCard1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.usrMemCard1.DrawLv = 0
        Me.usrMemCard1.Enabled = False
        Me.usrMemCard1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.usrMemCard1.Location = New System.Drawing.Point(29, 57)
        Me.usrMemCard1.Name = "usrMemCard1"
        Me.usrMemCard1.ParentID = -1
        Me.usrMemCard1.Size = New System.Drawing.Size(119, 182)
        Me.usrMemCard1.SpouseID = -1
        Me.usrMemCard1.TabIndex = 0
        Me.usrMemCard1.Visible = False
        '
        'rdCard2
        '
        Me.rdCard2.AutoSize = True
        Me.rdCard2.Location = New System.Drawing.Point(9, 313)
        Me.rdCard2.Name = "rdCard2"
        Me.rdCard2.Size = New System.Drawing.Size(14, 13)
        Me.rdCard2.TabIndex = 20
        Me.rdCard2.UseVisualStyleBackColor = True
        '
        'rdCard1
        '
        Me.rdCard1.AutoSize = True
        Me.rdCard1.Checked = True
        Me.rdCard1.Location = New System.Drawing.Point(9, 25)
        Me.rdCard1.Name = "rdCard1"
        Me.rdCard1.Size = New System.Drawing.Size(14, 13)
        Me.rdCard1.TabIndex = 10
        Me.rdCard1.TabStop = True
        Me.rdCard1.UseVisualStyleBackColor = True
        '
        'chkShowUnknownBirthDay
        '
        Me.chkShowUnknownBirthDay.AutoSize = True
        Me.chkShowUnknownBirthDay.Location = New System.Drawing.Point(267, 23)
        Me.chkShowUnknownBirthDay.Name = "chkShowUnknownBirthDay"
        Me.chkShowUnknownBirthDay.Size = New System.Drawing.Size(223, 18)
        Me.chkShowUnknownBirthDay.TabIndex = 21
        Me.chkShowUnknownBirthDay.Text = "Hiện KHÔNG RÕ nếu không có ngày sinh"
        Me.chkShowUnknownBirthDay.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ursLine)
        Me.GroupBox3.Controls.Add(Me.lblkChooseColor)
        Me.GroupBox3.Controls.Add(Me.lblColor)
        Me.GroupBox3.Controls.Add(Me.cboLineWeight)
        Me.GroupBox3.Controls.Add(Me.lblLineWeight)
        Me.GroupBox3.Controls.Add(Me.rdFrameCompact)
        Me.GroupBox3.Controls.Add(Me.rdFrameFull)
        Me.GroupBox3.Location = New System.Drawing.Point(264, 96)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(259, 136)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Kích thước khung vẽ"
        '
        'ursLine
        '
        Me.ursLine.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.ursLine.BackColor = System.Drawing.Color.Blue
        Me.ursLine.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ursLine.LineColor = System.Drawing.Color.Blue
        Me.ursLine.LineDirection = phv.clsEnum.emLineDirection.HORIZONTAL
        Me.ursLine.LineLength = 150
        Me.ursLine.LineType = phv.clsEnum.emLineType.SINGLE_LINE
        Me.ursLine.LineWeight = 1
        Me.ursLine.Location = New System.Drawing.Point(45, 125)
        Me.ursLine.Name = "ursLine"
        Me.ursLine.Size = New System.Drawing.Size(150, 1)
        Me.ursLine.TabIndex = 55
        '
        'lblkChooseColor
        '
        Me.lblkChooseColor.AutoSize = True
        Me.lblkChooseColor.Location = New System.Drawing.Point(144, 96)
        Me.lblkChooseColor.Name = "lblkChooseColor"
        Me.lblkChooseColor.Size = New System.Drawing.Size(55, 14)
        Me.lblkChooseColor.TabIndex = 54
        Me.lblkChooseColor.TabStop = True
        Me.lblkChooseColor.Text = "Chọn màu"
        '
        'lblColor
        '
        Me.lblColor.AutoSize = True
        Me.lblColor.Location = New System.Drawing.Point(17, 96)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(100, 14)
        Me.lblColor.TabIndex = 53
        Me.lblColor.Text = "Màu của đường nối"
        '
        'cboLineWeight
        '
        Me.cboLineWeight.FormattingEnabled = True
        Me.cboLineWeight.Items.AddRange(New Object() {"1", "2", "3", "4", "5"})
        Me.cboLineWeight.Location = New System.Drawing.Point(144, 63)
        Me.cboLineWeight.Name = "cboLineWeight"
        Me.cboLineWeight.Size = New System.Drawing.Size(54, 22)
        Me.cboLineWeight.TabIndex = 52
        '
        'lblLineWeight
        '
        Me.lblLineWeight.AutoSize = True
        Me.lblLineWeight.Location = New System.Drawing.Point(16, 68)
        Me.lblLineWeight.Name = "lblLineWeight"
        Me.lblLineWeight.Size = New System.Drawing.Size(117, 14)
        Me.lblLineWeight.TabIndex = 51
        Me.lblLineWeight.Text = "Độ đậm của đường nối"
        '
        'rdFrameCompact
        '
        Me.rdFrameCompact.AutoSize = True
        Me.rdFrameCompact.Location = New System.Drawing.Point(18, 45)
        Me.rdFrameCompact.Name = "rdFrameCompact"
        Me.rdFrameCompact.Size = New System.Drawing.Size(195, 18)
        Me.rdFrameCompact.TabIndex = 50
        Me.rdFrameCompact.Text = "Khung thu gọn (Không hiển thị ảnh)"
        Me.rdFrameCompact.UseVisualStyleBackColor = True
        '
        'rdFrameFull
        '
        Me.rdFrameFull.AutoSize = True
        Me.rdFrameFull.Checked = True
        Me.rdFrameFull.Location = New System.Drawing.Point(18, 20)
        Me.rdFrameFull.Name = "rdFrameFull"
        Me.rdFrameFull.Size = New System.Drawing.Size(174, 18)
        Me.rdFrameFull.TabIndex = 40
        Me.rdFrameFull.TabStop = True
        Me.rdFrameFull.Text = "Khung đầy đủ (Có hiển thị ảnh)"
        Me.rdFrameFull.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Image = Global.phv.My.Resources.Resources.task_done
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.Location = New System.Drawing.Point(285, 404)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(103, 43)
        Me.btnOK.TabIndex = 60
        Me.btnOK.Text = "Hoàn thành"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Image = Global.phv.My.Resources.Resources.back_32
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(405, 404)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(103, 43)
        Me.btnCancel.TabIndex = 70
        Me.btnCancel.Text = "     Thoát"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblVerCm)
        Me.GroupBox2.Controls.Add(Me.lblHozCm)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.nudVerBuffer)
        Me.GroupBox2.Controls.Add(Me.nudHozBuffer)
        Me.GroupBox2.Location = New System.Drawing.Point(264, 234)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(259, 83)
        Me.GroupBox2.TabIndex = 71
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Khoảng cách tổi thiểu (điểm ảnh) giữa 2 khung"
        '
        'lblVerCm
        '
        Me.lblVerCm.AutoSize = True
        Me.lblVerCm.Location = New System.Drawing.Point(153, 57)
        Me.lblVerCm.Name = "lblVerCm"
        Me.lblVerCm.Size = New System.Drawing.Size(76, 14)
        Me.lblVerCm.TabIndex = 38
        Me.lblVerCm.Text = "Khoảng ?? cm"
        '
        'lblHozCm
        '
        Me.lblHozCm.AutoSize = True
        Me.lblHozCm.Location = New System.Drawing.Point(153, 25)
        Me.lblHozCm.Name = "lblHozCm"
        Me.lblHozCm.Size = New System.Drawing.Size(76, 14)
        Me.lblHozCm.TabIndex = 37
        Me.lblHozCm.Text = "Khoảng ?? cm"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 14)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Theo Chiều Dọc:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 14)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Theo Chiều Ngang:"
        '
        'nudVerBuffer
        '
        Me.nudVerBuffer.Location = New System.Drawing.Point(112, 51)
        Me.nudVerBuffer.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudVerBuffer.Name = "nudVerBuffer"
        Me.nudVerBuffer.Size = New System.Drawing.Size(38, 20)
        Me.nudVerBuffer.TabIndex = 34
        Me.nudVerBuffer.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'nudHozBuffer
        '
        Me.nudHozBuffer.Location = New System.Drawing.Point(112, 22)
        Me.nudHozBuffer.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudHozBuffer.Name = "nudHozBuffer"
        Me.nudHozBuffer.Size = New System.Drawing.Size(38, 20)
        Me.nudHozBuffer.TabIndex = 33
        Me.nudHozBuffer.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ChkDrawnWomen)
        Me.GroupBox4.Controls.Add(Me.chkDrawnMan)
        Me.GroupBox4.Location = New System.Drawing.Point(264, 321)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(259, 74)
        Me.GroupBox4.TabIndex = 72
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Lựa chọn thành viên"
        '
        'chkDrawnMan
        '
        Me.chkDrawnMan.AutoSize = True
        Me.chkDrawnMan.Location = New System.Drawing.Point(9, 20)
        Me.chkDrawnMan.Name = "chkDrawnMan"
        Me.chkDrawnMan.Size = New System.Drawing.Size(154, 18)
        Me.chkDrawnMan.TabIndex = 0
        Me.chkDrawnMan.Text = "Chỉ vẽ các thành viên Nam"
        Me.chkDrawnMan.UseVisualStyleBackColor = True
        '
        'ChkDrawnWomen
        '
        Me.ChkDrawnWomen.AutoSize = True
        Me.ChkDrawnWomen.Location = New System.Drawing.Point(9, 45)
        Me.ChkDrawnWomen.Name = "ChkDrawnWomen"
        Me.ChkDrawnWomen.Size = New System.Drawing.Size(143, 18)
        Me.ChkDrawnWomen.TabIndex = 1
        Me.ChkDrawnWomen.Text = "Không vẽ nhánh con gái"
        Me.ChkDrawnWomen.UseVisualStyleBackColor = True
        '
        'frmOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(538, 459)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.chkShowUnknownBirthDay)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.grbFrame)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOption"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tùy chọn vẽ cây"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudGeneration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grbFrame.ResumeLayout(False)
        Me.grbFrame.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudVerBuffer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudHozBuffer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nudGeneration As System.Windows.Forms.NumericUpDown
    Friend WithEvents grbFrame As System.Windows.Forms.GroupBox
    Friend WithEvents usrMemCard2 As phv.usrMemberCard2
    Friend WithEvents usrMemCard1 As phv.usrMemberCard1
    Friend WithEvents rdCard2 As System.Windows.Forms.RadioButton
    Friend WithEvents rdCard1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rdFrameCompact As System.Windows.Forms.RadioButton
    Friend WithEvents rdFrameFull As System.Windows.Forms.RadioButton
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboFrameType As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents nudVerBuffer As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudHozBuffer As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblVerCm As System.Windows.Forms.Label
    Friend WithEvents lblHozCm As System.Windows.Forms.Label
    Friend WithEvents chkShowUnknownBirthDay As System.Windows.Forms.CheckBox
    Friend WithEvents lblkChooseColor As System.Windows.Forms.LinkLabel
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents cboLineWeight As System.Windows.Forms.ComboBox
    Friend WithEvents lblLineWeight As System.Windows.Forms.Label
    Friend WithEvents ursLine As phv.usrLine
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ChkDrawnWomen As System.Windows.Forms.CheckBox
    Friend WithEvents chkDrawnMan As System.Windows.Forms.CheckBox
End Class
