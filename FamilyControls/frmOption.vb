'   ****************************************************************** 
'      TITLE      : DRAW F-TREE OPTION
'　　　FUNCTION   :  
'      MEMO       :  
'      CREATE     : 2012/02/17　PHV 
'      UPDATE     :  
' 
'           2012 PHV Software 
'   ******************************************************************
Option Explicit On
Option Strict Off

Imports System.IO

''' <summary>
''' Option class
''' </summary>
''' <remarks></remarks>
''' <Create>2012/02/17  PHV</Create>
Public Class frmOption


    Private Const mcstrClsName As String = "frmOption"                  'class name
    Private Const mcstrDefaultFrame As String = "Không sử dụng khung ảnh"
    Private Const mcstrFrameNumber As String = "Kiểu khung khung số "
    Private mobjColor As Color
    Private mblnChanged As Boolean = False


    Dim musrCard1 As usrMemberCard1

    ''' <summary>
    ''' The change is made
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Changed() As Boolean
        Get
            Return mblnChanged
        End Get
    End Property


    ''' <summary>
    ''' frmOption_Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <Create>2012/02/17  PHV</Create>
    Private Sub frmOption_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Dim usrDetail As usrMemberDetail

            'create preview card
            usrDetail = New usrMemberDetail()
            usrDetail.CardAlias = "A Tám"
            usrDetail.CardBirth = "1900"
            usrDetail.CardContainer = usrMemCard2
            usrDetail.CardDie = "5/5 ÂL"
            usrDetail.CardImage = My.Resources.user_male_add_512
            usrDetail.CardLevel = 5
            usrDetail.CardName = "NGUYỄN VĂN A"
            usrDetail.CardRemark = "{\rtf1\ansi\ansicpg932\deff0\deflang1033\deflangfe1041{\fonttbl{\f0\fswiss\fcharset0 Arial;}}{\*\generator Msftedit 5.41.15.1515;}\viewkind4\uc1\pard\f0\fs20 Tham gia c\'e1ch m\u7841?ng t\u7915? s\u7899?m, li\u7879?t s\u7929? ch\u7889?ng M\u7929?.\par""}"

            usrMemCard2.fncAddItem(usrDetail)

            'use new card, don't know why usrMemCard1 doesn't show image :(
            musrCard1 = New usrMemberCard1(True)
            musrCard1.CardImage = My.Resources.user_male_add_512
            musrCard1.CardGender = clsEnum.emGender.MALE
            musrCard1.CardName = "NGUYỄN VĂN A" & vbCrLf & "(A Tám)"
            musrCard1.Location = usrMemCard1.Location 'New Point(105, 19)
            musrCard1.Enabled = False
            grbFrame.Controls.Add(musrCard1)


            'load settings
            'set card style
            rdCard1.Checked = True
            nudHozBuffer.Enabled = True
            If My.Settings.intCardStyle = clsEnum.emCardStyle.CARD2 Then
                rdCard2.Checked = True
                nudHozBuffer.Enabled = False
            End If

            'set card size
            rdFrameFull.Checked = True
            If My.Settings.intCardSize = clsEnum.emCardSize.SMALL Then rdFrameCompact.Checked = True


            'set generation
            nudGeneration.Value = My.Settings.intGeneration

            nudHozBuffer.Value = My.Settings.intHozBuffer
            nudVerBuffer.Value = My.Settings.intVerBuffer
            chkShowUnknownBirthDay.Checked = My.Settings.blnShowUnknownBirthDay
            cboLineWeight.Text = My.Settings.intLineWeight
            mobjColor = My.Settings.LineColor
            chkDrawnMan.Checked = My.Settings.blnDrawnManOnly
            ChkDrawnWomen.Checked = My.Settings.blnNotDrawnWomenBranch
            ursLine.LineColor = mobjColor
            ursLine.LineWeight = My.Settings.intLineWeight
            xLoadFrame()

            grbFrame.Focus()
            rdCard1.Focus()


        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "frmOption_Load", ex)
        End Try

    End Sub

    ''' <summary>
    ''' btnOK_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <Create>2012/02/17  PHV</Create>
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        Try
            If Not basCommon.fncMessageConfirm("Các thuộc tính vẽ phả hệ đã thay đổi, bạn có muốn sử dụng những  thuộc tính này?") Then Exit Sub

            'set card style
            My.Settings.intCardStyle = CInt(clsEnum.emCardStyle.CARD1)
            If rdCard2.Checked Then My.Settings.intCardStyle = CInt(clsEnum.emCardStyle.CARD2)

            'set card size
            My.Settings.intCardSize = CInt(clsEnum.emCardSize.LARGE)
            If rdFrameCompact.Checked Then My.Settings.intCardSize = CInt(clsEnum.emCardSize.SMALL)

            'set generation
            My.Settings.intGeneration = nudGeneration.Value

            'save frame path
            If rdCard1.Checked Then
                Dim objItem As clsListItem = CType(cboFrameType.SelectedItem, clsListItem)
                My.Settings.strCard1Bg = CStr(objItem.Tag)
            End If

            My.Settings.intHozBuffer = nudHozBuffer.Value
            My.Settings.intVerBuffer = nudVerBuffer.Value
            My.Settings.blnShowUnknownBirthDay = chkShowUnknownBirthDay.Checked
            My.Settings.intLineWeight = fncCnvToInt(cboLineWeight.Text)
            My.Settings.LineColor = mobjColor
            My.Settings.dblCard1Multiple = 1
            My.Settings.blnDrawnManOnly = chkDrawnMan.Checked
            My.Settings.blnNotDrawnWomenBranch = ChkDrawnWomen.Checked
            My.Settings.Save()

            fncSetBufferBetween2Card()

            mblnChanged = True
            Me.Close()


        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnOK_Click", ex)
        End Try

    End Sub


    ''' <summary>
    ''' btnCancel_Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' <Create>2012/02/17  PHV</Create>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Try
            Me.Close()

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnCancel_Click", ex)
        End Try

    End Sub


    Private Sub cboFrameType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFrameType.SelectedIndexChanged
        Try
            Dim strPath As String
            Dim objItem As clsListItem

            'get file path of frame
            objItem = CType(cboFrameType.SelectedItem, clsListItem)
            strPath = CStr(objItem.Tag)

            'if path is not valid
            If strPath = "" Or Not File.Exists(strPath) Then
                'picFrame.Image = My.Resources.pic_frame
                musrCard1.CardBackground = Nothing 'My.Resources.pic_frame
                musrCard1.BorderStyle = BorderStyle.FixedSingle
            Else
                'load image
                'picFrame.Image = Image.FromFile(strPath)
                musrCard1.CardBackground = Image.FromFile(strPath)
            End If

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "cboFrameType_SelectedIndexChanged", ex)
        End Try
    End Sub


    ''' <summary>
    ''' Load frame background
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function xLoadFrame() As Boolean

        xLoadFrame = False

        Try
            Dim strFolder As String
            Dim intIndex As Integer = 1

            cboFrameType.Items.Clear()
            cboFrameType.Items.Add(New clsListItem(mcstrDefaultFrame, ""))
            cboFrameType.SelectedIndex = 0

            strFolder = My.Application.Info.DirectoryPath & basConst.gcstrDocsFolder & basConst.gcstrFrameFolder

            If Not Directory.Exists(strFolder) Then
                cboFrameType.SelectedIndex = 0
                Return True
            End If


            For Each file As FileInfo In New DirectoryInfo(strFolder).GetFiles

                If Not file.Name.ToLower().EndsWith(".png") Then Continue For

                'add file to combobox
                cboFrameType.Items.Add(New clsListItem(mcstrFrameNumber & intIndex, file.FullName))
                If file.FullName = My.Settings.strCard1Bg Then cboFrameType.SelectedIndex = intIndex
                intIndex += 1

            Next

            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xLoadFrame", ex)
        End Try

    End Function


    ''' <summary>
    ''' Check box changes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub rdCard1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdCard1.CheckedChanged
        If rdCard1.Checked Then
            cboFrameType.Enabled = True
            nudHozBuffer.Enabled = True
        Else
            cboFrameType.Enabled = False
            nudHozBuffer.Enabled = False
        End If
    End Sub

    Private Sub nudVerBuffer_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudVerBuffer.ValueChanged
        lblVerCm.Text = "Khoảng: " & Math.Round(nudVerBuffer.Value / 96 * 25.4, 2) & " mm"
    End Sub

    Private Sub nudHozBuffer_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudHozBuffer.ValueChanged
        lblHozCm.Text = "Khoảng: " & Math.Round(nudHozBuffer.Value / 96 * 25.4, 2) & " mm"
    End Sub



    Private Sub cboLineWeight_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineWeight.SelectedIndexChanged
        Try
            Dim intLineWeight As Integer
            intLineWeight = fncCnvToInt(cboLineWeight.Text)
            ursLine.LineWeight = intLineWeight

        Catch ex As Exception

        End Try
    End Sub

    Private Sub lblkChooseColor_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblkChooseColor.LinkClicked
        Try
            Dim objColor As Color
            Dim objColorDialog As New ColorDialog
            objColor = My.Settings.LineColor
            If objColorDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                objColor = objColorDialog.Color
                ursLine.LineColor = objColor
                mobjColor = objColor
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class