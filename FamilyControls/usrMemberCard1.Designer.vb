﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class usrMemberCard1
    Inherits usrMemCardBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picMember = New System.Windows.Forms.PictureBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblBirth = New System.Windows.Forms.Label()
        Me.lblGender = New System.Windows.Forms.Label()
        Me.lblDeath = New System.Windows.Forms.Label()
        CType(Me.picMember, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picMember
        '
        Me.picMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picMember.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picMember.InitialImage = Nothing
        Me.picMember.Location = New System.Drawing.Point(32, 13)
        Me.picMember.Name = "picMember"
        Me.picMember.Size = New System.Drawing.Size(80, 100)
        Me.picMember.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picMember.TabIndex = 0
        Me.picMember.TabStop = False
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.Color.Transparent
        Me.lblName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(5, 114)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(136, 28)
        Me.lblName.TabIndex = 1
        Me.lblName.Text = "Ten thanh vien" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(ten thuong goi)"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblBirth
        '
        Me.lblBirth.BackColor = System.Drawing.Color.Transparent
        Me.lblBirth.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirth.Location = New System.Drawing.Point(5, 147)
        Me.lblBirth.Name = "lblBirth"
        Me.lblBirth.Size = New System.Drawing.Size(136, 14)
        Me.lblBirth.TabIndex = 1
        Me.lblBirth.Text = "1900"
        Me.lblBirth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGender
        '
        Me.lblGender.BackColor = System.Drawing.Color.Transparent
        Me.lblGender.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(18, 182)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(110, 14)
        Me.lblGender.TabIndex = 1
        Me.lblGender.Text = "Nam"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblGender.Visible = False
        '
        'lblDeath
        '
        Me.lblDeath.BackColor = System.Drawing.Color.Transparent
        Me.lblDeath.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeath.Location = New System.Drawing.Point(5, 166)
        Me.lblDeath.Name = "lblDeath"
        Me.lblDeath.Size = New System.Drawing.Size(136, 14)
        Me.lblDeath.TabIndex = 2
        Me.lblDeath.Text = "Mất: 10/01/1900"
        Me.lblDeath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'usrMemberCard1
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.phv.My.Resources.Resources.pic_frame
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Controls.Add(Me.lblDeath)
        Me.Controls.Add(Me.lblGender)
        Me.Controls.Add(Me.lblBirth)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.picMember)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "usrMemberCard1"
        Me.Size = New System.Drawing.Size(144, 194)
        CType(Me.picMember, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picMember As System.Windows.Forms.PictureBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblBirth As System.Windows.Forms.Label
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents lblDeath As System.Windows.Forms.Label

End Class
