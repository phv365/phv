﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPersonInfo
    Inherits phv.FamilyForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPersonInfo))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtAlias = New System.Windows.Forms.TextBox()
        Me.txtPhone1 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtURL = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnSaveEdu = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbpEducation = New System.Windows.Forms.TabPage()
        Me.pnEduDetail = New System.Windows.Forms.Panel()
        Me.txtSchoolName = New System.Windows.Forms.TextBox()
        Me.lblEndEdu = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnDown2 = New System.Windows.Forms.Button()
        Me.dgvEdu = New System.Windows.Forms.DataGridView()
        Me.clmSchoolName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmTimeEdu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmRemarkEdu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmTempTimeEdu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnUp2 = New System.Windows.Forms.Button()
        Me.lblStartEdu = New System.Windows.Forms.Label()
        Me.btnEndEdu = New System.Windows.Forms.Button()
        Me.btnCreateEdu = New System.Windows.Forms.Button()
        Me.btnStartEdu = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtRemarkEdu = New System.Windows.Forms.TextBox()
        Me.btnDelEdu = New System.Windows.Forms.Button()
        Me.rdEduGeneral = New System.Windows.Forms.RadioButton()
        Me.rdEduDetail = New System.Windows.Forms.RadioButton()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.pnEduGeneral = New System.Windows.Forms.Panel()
        Me.txtEduGeneral = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtHometown = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tbpRelationship = New System.Windows.Forms.TabPage()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.dgvRel = New System.Windows.Forms.DataGridView()
        Me.clmNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmRel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmRelBirth = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmRelRemark = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbpFact = New System.Windows.Forms.TabPage()
        Me.pnFactDetail = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.lblEndFact = New System.Windows.Forms.Label()
        Me.txtFactPlace = New System.Windows.Forms.TextBox()
        Me.lblStartFact = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnDown3 = New System.Windows.Forms.Button()
        Me.btnUp3 = New System.Windows.Forms.Button()
        Me.dgvFact = New System.Windows.Forms.DataGridView()
        Me.clmFactName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFactPlace = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFactTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFactDesc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmTempTimeFact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEndFact = New System.Windows.Forms.Button()
        Me.txtFactName = New System.Windows.Forms.TextBox()
        Me.btnStartFact = New System.Windows.Forms.Button()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtFactDesc = New System.Windows.Forms.TextBox()
        Me.btnDelFact = New System.Windows.Forms.Button()
        Me.btnSaveFact = New System.Windows.Forms.Button()
        Me.btnCreateFact = New System.Windows.Forms.Button()
        Me.rdFactGeneral = New System.Windows.Forms.RadioButton()
        Me.rdFactDetail = New System.Windows.Forms.RadioButton()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.pnFactGeneral = New System.Windows.Forms.Panel()
        Me.txtFactGeneral = New System.Windows.Forms.TextBox()
        Me.tbpRemark = New System.Windows.Forms.TabPage()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.txtRemark = New System.Windows.Forms.RichTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.tbpPerson = New System.Windows.Forms.TabPage()
        Me.gbButton = New System.Windows.Forms.GroupBox()
        Me.picButton = New System.Windows.Forms.PictureBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.btnBLunarCal = New System.Windows.Forms.Button()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtBYLunar = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtBMLunar = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtBYSun = New System.Windows.Forms.TextBox()
        Me.txtBirthPlace = New System.Windows.Forms.TextBox()
        Me.txtBDLunar = New System.Windows.Forms.TextBox()
        Me.btnBSunCal = New System.Windows.Forms.Button()
        Me.lblBYLunar = New System.Windows.Forms.Label()
        Me.cbNation = New System.Windows.Forms.ComboBox()
        Me.txtBMSun = New System.Windows.Forms.TextBox()
        Me.cbReligion = New System.Windows.Forms.ComboBox()
        Me.txtBDSun = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.pnDieInfo = New System.Windows.Forms.GroupBox()
        Me.txtDYLunar = New System.Windows.Forms.TextBox()
        Me.txtDYSun = New System.Windows.Forms.TextBox()
        Me.txtDMLunar = New System.Windows.Forms.TextBox()
        Me.btnDSunCal = New System.Windows.Forms.Button()
        Me.lblDiePlace = New System.Windows.Forms.Label()
        Me.txtDDSun = New System.Windows.Forms.TextBox()
        Me.lblDieDate = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtBuryPlace = New System.Windows.Forms.TextBox()
        Me.txtDDLunar = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.btnDLunarCal = New System.Windows.Forms.Button()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.lblDYLunar = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtDMSun = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.cboBranch = New System.Windows.Forms.ComboBox()
        Me.lblDelImg = New System.Windows.Forms.Label()
        Me.txtFamilyOrder = New System.Windows.Forms.NumericUpDown()
        Me.picImage = New System.Windows.Forms.PictureBox()
        Me.chkDie = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rdMale = New System.Windows.Forms.RadioButton()
        Me.rdFemale = New System.Windows.Forms.RadioButton()
        Me.rdUnknow = New System.Windows.Forms.RadioButton()
        Me.txtMidName = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbcPersonInfo = New System.Windows.Forms.TabControl()
        Me.tbpCardID = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtRemarkContact = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtFax = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMail2 = New System.Windows.Forms.TextBox()
        Me.txtIM = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtMail1 = New System.Windows.Forms.TextBox()
        Me.txtPhone2 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbpJob = New System.Windows.Forms.TabPage()
        Me.rdCareerGeneral = New System.Windows.Forms.RadioButton()
        Me.pnCareerDetail = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lblEndCareer = New System.Windows.Forms.Label()
        Me.lblStartCareer = New System.Windows.Forms.Label()
        Me.txtPosition = New System.Windows.Forms.TextBox()
        Me.btnEndCareer = New System.Windows.Forms.Button()
        Me.txtOccupt = New System.Windows.Forms.TextBox()
        Me.btnStartCareer = New System.Windows.Forms.Button()
        Me.txtOffName = New System.Windows.Forms.TextBox()
        Me.btnDelCareer = New System.Windows.Forms.Button()
        Me.txtOffAddr = New System.Windows.Forms.TextBox()
        Me.btnCreateCareer = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnDown1 = New System.Windows.Forms.Button()
        Me.btnUp1 = New System.Windows.Forms.Button()
        Me.dgvCareer = New System.Windows.Forms.DataGridView()
        Me.clmName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmAddr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPosition = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmOccupt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmTempTimeCareer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnSaveCareer = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.rdCareerDetail = New System.Windows.Forms.RadioButton()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.pnCareerGeneral = New System.Windows.Forms.Panel()
        Me.txtCareerGeneral = New System.Windows.Forms.TextBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.ttpPersonInfo = New System.Windows.Forms.ToolTip(Me.components)
        Me.dlgOpenImage = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tbpEducation.SuspendLayout()
        Me.pnEduDetail.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvEdu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnEduGeneral.SuspendLayout()
        Me.tbpRelationship.SuspendLayout()
        CType(Me.dgvRel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpFact.SuspendLayout()
        Me.pnFactDetail.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvFact, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnFactGeneral.SuspendLayout()
        Me.tbpRemark.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.tbpPerson.SuspendLayout()
        Me.gbButton.SuspendLayout()
        CType(Me.picButton, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.pnDieInfo.SuspendLayout()
        CType(Me.txtFamilyOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbcPersonInfo.SuspendLayout()
        Me.tbpCardID.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.tbpJob.SuspendLayout()
        Me.pnCareerDetail.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvCareer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnCareerGeneral.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtAlias
        '
        Me.txtAlias.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtAlias.Location = New System.Drawing.Point(115, 131)
        Me.txtAlias.MaxLength = 200
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(129, 22)
        Me.txtAlias.TabIndex = 70
        '
        'txtPhone1
        '
        Me.txtPhone1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone1.Location = New System.Drawing.Point(141, 26)
        Me.txtPhone1.MaxLength = 50
        Me.txtPhone1.Name = "txtPhone1"
        Me.txtPhone1.Size = New System.Drawing.Size(230, 22)
        Me.txtPhone1.TabIndex = 21
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label9.Location = New System.Drawing.Point(11, 29)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 16)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Điện thoại cố định :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(19, 35)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(57, 13)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Thời gian :"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(17, 6)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(62, 13)
        Me.Label33.TabIndex = 14
        Me.Label33.Text = "Tên trường:"
        '
        'txtURL
        '
        Me.txtURL.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtURL.Location = New System.Drawing.Point(141, 150)
        Me.txtURL.MaxLength = 200
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(430, 22)
        Me.txtURL.TabIndex = 26
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(147, 51)
        Me.txtAddress.MaxLength = 200
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(430, 22)
        Me.txtAddress.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(308, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(46, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Địa chỉ :"
        '
        'btnSaveEdu
        '
        Me.btnSaveEdu.Location = New System.Drawing.Point(356, 84)
        Me.btnSaveEdu.Name = "btnSaveEdu"
        Me.btnSaveEdu.Size = New System.Drawing.Size(104, 36)
        Me.btnSaveEdu.TabIndex = 44
        Me.btnSaveEdu.Text = "&Lưu Thông tin"
        Me.btnSaveEdu.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(21, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(74, 13)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Tên cơ quan :"
        '
        'tbpEducation
        '
        Me.tbpEducation.Controls.Add(Me.pnEduDetail)
        Me.tbpEducation.Controls.Add(Me.rdEduGeneral)
        Me.tbpEducation.Controls.Add(Me.rdEduDetail)
        Me.tbpEducation.Controls.Add(Me.Label26)
        Me.tbpEducation.Controls.Add(Me.pnEduGeneral)
        Me.tbpEducation.Location = New System.Drawing.Point(4, 25)
        Me.tbpEducation.Name = "tbpEducation"
        Me.tbpEducation.Size = New System.Drawing.Size(598, 528)
        Me.tbpEducation.TabIndex = 3
        Me.tbpEducation.Text = "Giáo dục"
        Me.tbpEducation.UseVisualStyleBackColor = True
        Me.tbpEducation.Visible = False
        '
        'pnEduDetail
        '
        Me.pnEduDetail.Controls.Add(Me.Label33)
        Me.pnEduDetail.Controls.Add(Me.Label18)
        Me.pnEduDetail.Controls.Add(Me.txtSchoolName)
        Me.pnEduDetail.Controls.Add(Me.lblEndEdu)
        Me.pnEduDetail.Controls.Add(Me.GroupBox3)
        Me.pnEduDetail.Controls.Add(Me.lblStartEdu)
        Me.pnEduDetail.Controls.Add(Me.btnSaveEdu)
        Me.pnEduDetail.Controls.Add(Me.btnEndEdu)
        Me.pnEduDetail.Controls.Add(Me.btnCreateEdu)
        Me.pnEduDetail.Controls.Add(Me.btnStartEdu)
        Me.pnEduDetail.Controls.Add(Me.Label34)
        Me.pnEduDetail.Controls.Add(Me.Label29)
        Me.pnEduDetail.Controls.Add(Me.txtRemarkEdu)
        Me.pnEduDetail.Controls.Add(Me.btnDelEdu)
        Me.pnEduDetail.Location = New System.Drawing.Point(3, 34)
        Me.pnEduDetail.Name = "pnEduDetail"
        Me.pnEduDetail.Size = New System.Drawing.Size(595, 491)
        Me.pnEduDetail.TabIndex = 49
        '
        'txtSchoolName
        '
        Me.txtSchoolName.Location = New System.Drawing.Point(95, 3)
        Me.txtSchoolName.MaxLength = 50
        Me.txtSchoolName.Name = "txtSchoolName"
        Me.txtSchoolName.Size = New System.Drawing.Size(365, 20)
        Me.txtSchoolName.TabIndex = 39
        '
        'lblEndEdu
        '
        Me.lblEndEdu.AutoSize = True
        Me.lblEndEdu.Location = New System.Drawing.Point(289, 35)
        Me.lblEndEdu.Name = "lblEndEdu"
        Me.lblEndEdu.Size = New System.Drawing.Size(44, 13)
        Me.lblEndEdu.TabIndex = 47
        Me.lblEndEdu.Text = "Chưa rõ"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnDown2)
        Me.GroupBox3.Controls.Add(Me.dgvEdu)
        Me.GroupBox3.Controls.Add(Me.btnUp2)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 226)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(589, 262)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Danh sách thông tin giáo dục"
        '
        'btnDown2
        '
        Me.btnDown2.Image = CType(resources.GetObject("btnDown2.Image"), System.Drawing.Image)
        Me.btnDown2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDown2.Location = New System.Drawing.Point(556, 60)
        Me.btnDown2.Name = "btnDown2"
        Me.btnDown2.Size = New System.Drawing.Size(33, 33)
        Me.btnDown2.TabIndex = 52
        Me.btnDown2.UseVisualStyleBackColor = True
        '
        'dgvEdu
        '
        Me.dgvEdu.AllowUserToAddRows = False
        Me.dgvEdu.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvEdu.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEdu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEdu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmSchoolName, Me.clmTimeEdu, Me.clmRemarkEdu, Me.clmTempTimeEdu})
        Me.dgvEdu.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvEdu.Location = New System.Drawing.Point(3, 16)
        Me.dgvEdu.MultiSelect = False
        Me.dgvEdu.Name = "dgvEdu"
        Me.dgvEdu.ReadOnly = True
        Me.dgvEdu.RowHeadersVisible = False
        Me.dgvEdu.RowTemplate.Height = 21
        Me.dgvEdu.RowTemplate.ReadOnly = True
        Me.dgvEdu.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEdu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEdu.Size = New System.Drawing.Size(546, 243)
        Me.dgvEdu.TabIndex = 46
        '
        'clmSchoolName
        '
        Me.clmSchoolName.HeaderText = "Tên trường"
        Me.clmSchoolName.MinimumWidth = 160
        Me.clmSchoolName.Name = "clmSchoolName"
        Me.clmSchoolName.ReadOnly = True
        Me.clmSchoolName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmSchoolName.Width = 160
        '
        'clmTimeEdu
        '
        Me.clmTimeEdu.HeaderText = "Thời gian"
        Me.clmTimeEdu.MinimumWidth = 160
        Me.clmTimeEdu.Name = "clmTimeEdu"
        Me.clmTimeEdu.ReadOnly = True
        Me.clmTimeEdu.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmTimeEdu.Width = 160
        '
        'clmRemarkEdu
        '
        Me.clmRemarkEdu.HeaderText = "Mô tả"
        Me.clmRemarkEdu.MinimumWidth = 250
        Me.clmRemarkEdu.Name = "clmRemarkEdu"
        Me.clmRemarkEdu.ReadOnly = True
        Me.clmRemarkEdu.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmRemarkEdu.Width = 250
        '
        'clmTempTimeEdu
        '
        Me.clmTempTimeEdu.HeaderText = "TempTime"
        Me.clmTempTimeEdu.Name = "clmTempTimeEdu"
        Me.clmTempTimeEdu.ReadOnly = True
        Me.clmTempTimeEdu.Visible = False
        '
        'btnUp2
        '
        Me.btnUp2.Image = CType(resources.GetObject("btnUp2.Image"), System.Drawing.Image)
        Me.btnUp2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnUp2.Location = New System.Drawing.Point(555, 21)
        Me.btnUp2.Name = "btnUp2"
        Me.btnUp2.Size = New System.Drawing.Size(33, 33)
        Me.btnUp2.TabIndex = 51
        Me.btnUp2.UseVisualStyleBackColor = True
        '
        'lblStartEdu
        '
        Me.lblStartEdu.AutoSize = True
        Me.lblStartEdu.Location = New System.Drawing.Point(143, 35)
        Me.lblStartEdu.Name = "lblStartEdu"
        Me.lblStartEdu.Size = New System.Drawing.Size(44, 13)
        Me.lblStartEdu.TabIndex = 47
        Me.lblStartEdu.Text = "Chưa rõ"
        '
        'btnEndEdu
        '
        Me.btnEndEdu.Location = New System.Drawing.Point(243, 31)
        Me.btnEndEdu.Name = "btnEndEdu"
        Me.btnEndEdu.Size = New System.Drawing.Size(42, 23)
        Me.btnEndEdu.TabIndex = 41
        Me.btnEndEdu.Text = "Đến"
        Me.btnEndEdu.UseVisualStyleBackColor = True
        '
        'btnCreateEdu
        '
        Me.btnCreateEdu.Location = New System.Drawing.Point(284, 84)
        Me.btnCreateEdu.Name = "btnCreateEdu"
        Me.btnCreateEdu.Size = New System.Drawing.Size(68, 36)
        Me.btnCreateEdu.TabIndex = 43
        Me.btnCreateEdu.Text = "Tạo &Mới"
        Me.btnCreateEdu.UseVisualStyleBackColor = True
        '
        'btnStartEdu
        '
        Me.btnStartEdu.Location = New System.Drawing.Point(95, 31)
        Me.btnStartEdu.Name = "btnStartEdu"
        Me.btnStartEdu.Size = New System.Drawing.Size(42, 23)
        Me.btnStartEdu.TabIndex = 40
        Me.btnStartEdu.Text = "Từ"
        Me.btnStartEdu.UseVisualStyleBackColor = True
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(49, 157)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(37, 13)
        Me.Label34.TabIndex = 23
        Me.Label34.Text = "Mô tả:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(221, 35)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(14, 13)
        Me.Label29.TabIndex = 32
        Me.Label29.Text = "~"
        '
        'txtRemarkEdu
        '
        Me.txtRemarkEdu.Location = New System.Drawing.Point(95, 154)
        Me.txtRemarkEdu.MaxLength = 500
        Me.txtRemarkEdu.Multiline = True
        Me.txtRemarkEdu.Name = "txtRemarkEdu"
        Me.txtRemarkEdu.Size = New System.Drawing.Size(305, 66)
        Me.txtRemarkEdu.TabIndex = 42
        '
        'btnDelEdu
        '
        Me.btnDelEdu.Location = New System.Drawing.Point(466, 84)
        Me.btnDelEdu.Name = "btnDelEdu"
        Me.btnDelEdu.Size = New System.Drawing.Size(52, 36)
        Me.btnDelEdu.TabIndex = 45
        Me.btnDelEdu.Text = "&Xóa"
        Me.btnDelEdu.UseVisualStyleBackColor = True
        '
        'rdEduGeneral
        '
        Me.rdEduGeneral.AutoSize = True
        Me.rdEduGeneral.Location = New System.Drawing.Point(191, 8)
        Me.rdEduGeneral.Name = "rdEduGeneral"
        Me.rdEduGeneral.Size = New System.Drawing.Size(71, 17)
        Me.rdEduGeneral.TabIndex = 28
        Me.rdEduGeneral.Text = "Tổng hợp"
        Me.rdEduGeneral.UseVisualStyleBackColor = True
        '
        'rdEduDetail
        '
        Me.rdEduDetail.AutoSize = True
        Me.rdEduDetail.Checked = True
        Me.rdEduDetail.Location = New System.Drawing.Point(103, 8)
        Me.rdEduDetail.Name = "rdEduDetail"
        Me.rdEduDetail.Size = New System.Drawing.Size(57, 17)
        Me.rdEduDetail.TabIndex = 25
        Me.rdEduDetail.TabStop = True
        Me.rdEduDetail.Text = "Chi tiết"
        Me.rdEduDetail.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(0, 10)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(77, 13)
        Me.Label26.TabIndex = 48
        Me.Label26.Text = "Kiểu nhập liệu:"
        '
        'pnEduGeneral
        '
        Me.pnEduGeneral.Controls.Add(Me.txtEduGeneral)
        Me.pnEduGeneral.Location = New System.Drawing.Point(3, 34)
        Me.pnEduGeneral.Name = "pnEduGeneral"
        Me.pnEduGeneral.Size = New System.Drawing.Size(641, 289)
        Me.pnEduGeneral.TabIndex = 50
        Me.pnEduGeneral.Visible = False
        '
        'txtEduGeneral
        '
        Me.txtEduGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEduGeneral.Location = New System.Drawing.Point(0, 0)
        Me.txtEduGeneral.Multiline = True
        Me.txtEduGeneral.Name = "txtEduGeneral"
        Me.txtEduGeneral.Size = New System.Drawing.Size(641, 289)
        Me.txtEduGeneral.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 33)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Vị trí công tác :"
        '
        'txtHometown
        '
        Me.txtHometown.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHometown.Location = New System.Drawing.Point(147, 19)
        Me.txtHometown.MaxLength = 200
        Me.txtHometown.Name = "txtHometown"
        Me.txtHometown.Size = New System.Drawing.Size(430, 22)
        Me.txtHometown.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(45, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 16)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Nguyên quán :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(67, 153)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 16)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "Website :"
        '
        'tbpRelationship
        '
        Me.tbpRelationship.Controls.Add(Me.Label20)
        Me.tbpRelationship.Controls.Add(Me.dgvRel)
        Me.tbpRelationship.Location = New System.Drawing.Point(4, 25)
        Me.tbpRelationship.Name = "tbpRelationship"
        Me.tbpRelationship.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpRelationship.Size = New System.Drawing.Size(598, 528)
        Me.tbpRelationship.TabIndex = 4
        Me.tbpRelationship.Text = "Quan hệ gia đình"
        Me.tbpRelationship.UseVisualStyleBackColor = True
        Me.tbpRelationship.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(88, 22)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(389, 13)
        Me.Label20.TabIndex = 5
        Me.Label20.Text = "DANH SÁCH CÁC QUAN HỆ TRONG GIA ĐÌNH (BỐ, MẸ, ANH, CHỊ, EM, CON)"
        '
        'dgvRel
        '
        Me.dgvRel.AllowUserToAddRows = False
        Me.dgvRel.AllowUserToDeleteRows = False
        Me.dgvRel.AllowUserToResizeRows = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvRel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmNo, Me.clmFullName, Me.clmRel, Me.clmRelBirth, Me.clmRelRemark})
        Me.dgvRel.Location = New System.Drawing.Point(14, 57)
        Me.dgvRel.MultiSelect = False
        Me.dgvRel.Name = "dgvRel"
        Me.dgvRel.ReadOnly = True
        Me.dgvRel.RowHeadersVisible = False
        Me.dgvRel.RowTemplate.Height = 21
        Me.dgvRel.RowTemplate.ReadOnly = True
        Me.dgvRel.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRel.Size = New System.Drawing.Size(578, 275)
        Me.dgvRel.TabIndex = 1
        '
        'clmNo
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.clmNo.DefaultCellStyle = DataGridViewCellStyle3
        Me.clmNo.HeaderText = "STT"
        Me.clmNo.MinimumWidth = 50
        Me.clmNo.Name = "clmNo"
        Me.clmNo.ReadOnly = True
        Me.clmNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmNo.Width = 50
        '
        'clmFullName
        '
        Me.clmFullName.HeaderText = "Họ và tên"
        Me.clmFullName.MinimumWidth = 150
        Me.clmFullName.Name = "clmFullName"
        Me.clmFullName.ReadOnly = True
        Me.clmFullName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmFullName.Width = 150
        '
        'clmRel
        '
        Me.clmRel.HeaderText = "Quan hệ"
        Me.clmRel.MinimumWidth = 125
        Me.clmRel.Name = "clmRel"
        Me.clmRel.ReadOnly = True
        Me.clmRel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmRel.Width = 125
        '
        'clmRelBirth
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.clmRelBirth.DefaultCellStyle = DataGridViewCellStyle4
        Me.clmRelBirth.HeaderText = "Ngày sinh"
        Me.clmRelBirth.MinimumWidth = 100
        Me.clmRelBirth.Name = "clmRelBirth"
        Me.clmRelBirth.ReadOnly = True
        Me.clmRelBirth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'clmRelRemark
        '
        Me.clmRelRemark.HeaderText = "Ghi chú"
        Me.clmRelRemark.MinimumWidth = 195
        Me.clmRelRemark.Name = "clmRelRemark"
        Me.clmRelRemark.ReadOnly = True
        Me.clmRelRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmRelRemark.Width = 195
        '
        'tbpFact
        '
        Me.tbpFact.Controls.Add(Me.pnFactDetail)
        Me.tbpFact.Controls.Add(Me.rdFactGeneral)
        Me.tbpFact.Controls.Add(Me.rdFactDetail)
        Me.tbpFact.Controls.Add(Me.Label41)
        Me.tbpFact.Controls.Add(Me.pnFactGeneral)
        Me.tbpFact.Location = New System.Drawing.Point(4, 25)
        Me.tbpFact.Name = "tbpFact"
        Me.tbpFact.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpFact.Size = New System.Drawing.Size(598, 528)
        Me.tbpFact.TabIndex = 5
        Me.tbpFact.Text = "Sự kiện"
        Me.tbpFact.UseVisualStyleBackColor = True
        '
        'pnFactDetail
        '
        Me.pnFactDetail.Controls.Add(Me.Label17)
        Me.pnFactDetail.Controls.Add(Me.Label28)
        Me.pnFactDetail.Controls.Add(Me.Label32)
        Me.pnFactDetail.Controls.Add(Me.lblEndFact)
        Me.pnFactDetail.Controls.Add(Me.txtFactPlace)
        Me.pnFactDetail.Controls.Add(Me.lblStartFact)
        Me.pnFactDetail.Controls.Add(Me.GroupBox4)
        Me.pnFactDetail.Controls.Add(Me.btnEndFact)
        Me.pnFactDetail.Controls.Add(Me.txtFactName)
        Me.pnFactDetail.Controls.Add(Me.btnStartFact)
        Me.pnFactDetail.Controls.Add(Me.Label37)
        Me.pnFactDetail.Controls.Add(Me.Label36)
        Me.pnFactDetail.Controls.Add(Me.txtFactDesc)
        Me.pnFactDetail.Controls.Add(Me.btnDelFact)
        Me.pnFactDetail.Controls.Add(Me.btnSaveFact)
        Me.pnFactDetail.Controls.Add(Me.btnCreateFact)
        Me.pnFactDetail.Location = New System.Drawing.Point(3, 34)
        Me.pnFactDetail.Name = "pnFactDetail"
        Me.pnFactDetail.Size = New System.Drawing.Size(595, 488)
        Me.pnFactDetail.TabIndex = 60
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 4)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Tên sự kiện :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(18, 68)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(70, 16)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "Thời gian :"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(20, 36)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(67, 16)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Địa điểm :"
        '
        'lblEndFact
        '
        Me.lblEndFact.AutoSize = True
        Me.lblEndFact.Location = New System.Drawing.Point(287, 69)
        Me.lblEndFact.Name = "lblEndFact"
        Me.lblEndFact.Size = New System.Drawing.Size(55, 16)
        Me.lblEndFact.TabIndex = 57
        Me.lblEndFact.Text = "Chưa rõ"
        '
        'txtFactPlace
        '
        Me.txtFactPlace.Location = New System.Drawing.Point(93, 34)
        Me.txtFactPlace.MaxLength = 200
        Me.txtFactPlace.Name = "txtFactPlace"
        Me.txtFactPlace.Size = New System.Drawing.Size(496, 22)
        Me.txtFactPlace.TabIndex = 48
        '
        'lblStartFact
        '
        Me.lblStartFact.AutoSize = True
        Me.lblStartFact.Location = New System.Drawing.Point(141, 69)
        Me.lblStartFact.Name = "lblStartFact"
        Me.lblStartFact.Size = New System.Drawing.Size(55, 16)
        Me.lblStartFact.TabIndex = 58
        Me.lblStartFact.Text = "Chưa rõ"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btnDown3)
        Me.GroupBox4.Controls.Add(Me.btnUp3)
        Me.GroupBox4.Controls.Add(Me.dgvFact)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 272)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(586, 182)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Danh sách các sự kiện"
        '
        'btnDown3
        '
        Me.btnDown3.Image = CType(resources.GetObject("btnDown3.Image"), System.Drawing.Image)
        Me.btnDown3.Location = New System.Drawing.Point(596, 56)
        Me.btnDown3.Name = "btnDown3"
        Me.btnDown3.Size = New System.Drawing.Size(33, 33)
        Me.btnDown3.TabIndex = 57
        Me.btnDown3.UseVisualStyleBackColor = True
        '
        'btnUp3
        '
        Me.btnUp3.Image = CType(resources.GetObject("btnUp3.Image"), System.Drawing.Image)
        Me.btnUp3.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnUp3.Location = New System.Drawing.Point(596, 18)
        Me.btnUp3.Name = "btnUp3"
        Me.btnUp3.Size = New System.Drawing.Size(33, 33)
        Me.btnUp3.TabIndex = 56
        Me.btnUp3.UseVisualStyleBackColor = True
        '
        'dgvFact
        '
        Me.dgvFact.AllowUserToAddRows = False
        Me.dgvFact.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvFact.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvFact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFact.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmFactName, Me.clmFactPlace, Me.clmFactTime, Me.clmFactDesc, Me.clmTempTimeFact})
        Me.dgvFact.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvFact.Location = New System.Drawing.Point(3, 18)
        Me.dgvFact.MultiSelect = False
        Me.dgvFact.Name = "dgvFact"
        Me.dgvFact.ReadOnly = True
        Me.dgvFact.RowHeadersVisible = False
        Me.dgvFact.RowTemplate.Height = 21
        Me.dgvFact.RowTemplate.ReadOnly = True
        Me.dgvFact.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvFact.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFact.Size = New System.Drawing.Size(587, 161)
        Me.dgvFact.TabIndex = 55
        '
        'clmFactName
        '
        Me.clmFactName.HeaderText = "Tên sự kiện"
        Me.clmFactName.MinimumWidth = 140
        Me.clmFactName.Name = "clmFactName"
        Me.clmFactName.ReadOnly = True
        Me.clmFactName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmFactName.Width = 140
        '
        'clmFactPlace
        '
        Me.clmFactPlace.HeaderText = "Địa điểm"
        Me.clmFactPlace.MinimumWidth = 140
        Me.clmFactPlace.Name = "clmFactPlace"
        Me.clmFactPlace.ReadOnly = True
        Me.clmFactPlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmFactPlace.Width = 140
        '
        'clmFactTime
        '
        Me.clmFactTime.HeaderText = "Thời gian"
        Me.clmFactTime.MinimumWidth = 140
        Me.clmFactTime.Name = "clmFactTime"
        Me.clmFactTime.ReadOnly = True
        Me.clmFactTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmFactTime.Width = 140
        '
        'clmFactDesc
        '
        Me.clmFactDesc.HeaderText = "Mô tả"
        Me.clmFactDesc.MinimumWidth = 160
        Me.clmFactDesc.Name = "clmFactDesc"
        Me.clmFactDesc.ReadOnly = True
        Me.clmFactDesc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmFactDesc.Width = 160
        '
        'clmTempTimeFact
        '
        Me.clmTempTimeFact.HeaderText = "TempTime"
        Me.clmTempTimeFact.Name = "clmTempTimeFact"
        Me.clmTempTimeFact.ReadOnly = True
        Me.clmTempTimeFact.Visible = False
        '
        'btnEndFact
        '
        Me.btnEndFact.Location = New System.Drawing.Point(241, 65)
        Me.btnEndFact.Name = "btnEndFact"
        Me.btnEndFact.Size = New System.Drawing.Size(42, 23)
        Me.btnEndFact.TabIndex = 50
        Me.btnEndFact.Text = "Đến"
        Me.btnEndFact.UseVisualStyleBackColor = True
        '
        'txtFactName
        '
        Me.txtFactName.Location = New System.Drawing.Point(93, 4)
        Me.txtFactName.MaxLength = 200
        Me.txtFactName.Name = "txtFactName"
        Me.txtFactName.Size = New System.Drawing.Size(496, 22)
        Me.txtFactName.TabIndex = 47
        '
        'btnStartFact
        '
        Me.btnStartFact.Location = New System.Drawing.Point(93, 65)
        Me.btnStartFact.Name = "btnStartFact"
        Me.btnStartFact.Size = New System.Drawing.Size(42, 23)
        Me.btnStartFact.TabIndex = 49
        Me.btnStartFact.Text = "Từ"
        Me.btnStartFact.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(32, 151)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(53, 16)
        Me.Label37.TabIndex = 25
        Me.Label37.Text = "Mô  tả :"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(219, 68)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(16, 16)
        Me.Label36.TabIndex = 35
        Me.Label36.Text = "~"
        '
        'txtFactDesc
        '
        Me.txtFactDesc.Location = New System.Drawing.Point(91, 101)
        Me.txtFactDesc.MaxLength = 200
        Me.txtFactDesc.Multiline = True
        Me.txtFactDesc.Name = "txtFactDesc"
        Me.txtFactDesc.Size = New System.Drawing.Size(498, 110)
        Me.txtFactDesc.TabIndex = 51
        '
        'btnDelFact
        '
        Me.btnDelFact.Location = New System.Drawing.Point(297, 219)
        Me.btnDelFact.Name = "btnDelFact"
        Me.btnDelFact.Size = New System.Drawing.Size(80, 30)
        Me.btnDelFact.TabIndex = 54
        Me.btnDelFact.Text = "&Xóa"
        Me.btnDelFact.UseVisualStyleBackColor = True
        '
        'btnSaveFact
        '
        Me.btnSaveFact.Location = New System.Drawing.Point(195, 219)
        Me.btnSaveFact.Name = "btnSaveFact"
        Me.btnSaveFact.Size = New System.Drawing.Size(80, 30)
        Me.btnSaveFact.TabIndex = 53
        Me.btnSaveFact.Text = "&Lưu "
        Me.btnSaveFact.UseVisualStyleBackColor = True
        '
        'btnCreateFact
        '
        Me.btnCreateFact.Location = New System.Drawing.Point(93, 219)
        Me.btnCreateFact.Name = "btnCreateFact"
        Me.btnCreateFact.Size = New System.Drawing.Size(80, 30)
        Me.btnCreateFact.TabIndex = 52
        Me.btnCreateFact.Text = "Tạo &Mới"
        Me.btnCreateFact.UseVisualStyleBackColor = True
        '
        'rdFactGeneral
        '
        Me.rdFactGeneral.AutoSize = True
        Me.rdFactGeneral.Location = New System.Drawing.Point(221, 8)
        Me.rdFactGeneral.Name = "rdFactGeneral"
        Me.rdFactGeneral.Size = New System.Drawing.Size(81, 20)
        Me.rdFactGeneral.TabIndex = 28
        Me.rdFactGeneral.Text = "Tổng hợp"
        Me.rdFactGeneral.UseVisualStyleBackColor = True
        '
        'rdFactDetail
        '
        Me.rdFactDetail.AutoSize = True
        Me.rdFactDetail.Checked = True
        Me.rdFactDetail.Location = New System.Drawing.Point(133, 8)
        Me.rdFactDetail.Name = "rdFactDetail"
        Me.rdFactDetail.Size = New System.Drawing.Size(67, 20)
        Me.rdFactDetail.TabIndex = 25
        Me.rdFactDetail.TabStop = True
        Me.rdFactDetail.Text = "Chi tiết"
        Me.rdFactDetail.UseVisualStyleBackColor = True
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(5, 10)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(118, 16)
        Me.Label41.TabIndex = 59
        Me.Label41.Text = "Kiểu nhập dữ liệu :"
        '
        'pnFactGeneral
        '
        Me.pnFactGeneral.Controls.Add(Me.txtFactGeneral)
        Me.pnFactGeneral.Location = New System.Drawing.Point(3, 34)
        Me.pnFactGeneral.Name = "pnFactGeneral"
        Me.pnFactGeneral.Size = New System.Drawing.Size(641, 289)
        Me.pnFactGeneral.TabIndex = 61
        Me.pnFactGeneral.Visible = False
        '
        'txtFactGeneral
        '
        Me.txtFactGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFactGeneral.Location = New System.Drawing.Point(0, 0)
        Me.txtFactGeneral.Multiline = True
        Me.txtFactGeneral.Name = "txtFactGeneral"
        Me.txtFactGeneral.Size = New System.Drawing.Size(641, 289)
        Me.txtFactGeneral.TabIndex = 0
        '
        'tbpRemark
        '
        Me.tbpRemark.Controls.Add(Me.GroupBox8)
        Me.tbpRemark.Location = New System.Drawing.Point(4, 25)
        Me.tbpRemark.Name = "tbpRemark"
        Me.tbpRemark.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpRemark.Size = New System.Drawing.Size(598, 528)
        Me.tbpRemark.TabIndex = 6
        Me.tbpRemark.Text = "Ghi chú"
        Me.tbpRemark.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.txtRemark)
        Me.GroupBox8.Location = New System.Drawing.Point(9, 13)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(579, 509)
        Me.GroupBox8.TabIndex = 6
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Thông tin bổ sung"
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(6, 21)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(565, 482)
        Me.txtRemark.TabIndex = 5
        Me.txtRemark.Text = ""
        '
        'Label22
        '
        Me.Label22.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label22.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label22.Location = New System.Drawing.Point(0, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(625, 46)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "THÔNG TIN CHI TIẾT"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCancel
        '
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(485, 18)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(103, 30)
        Me.btnCancel.TabIndex = 190
        Me.btnCancel.Text = "Thoát"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'tbpPerson
        '
        Me.tbpPerson.Controls.Add(Me.gbButton)
        Me.tbpPerson.Controls.Add(Me.GroupBox5)
        Me.tbpPerson.Controls.Add(Me.pnDieInfo)
        Me.tbpPerson.Controls.Add(Me.Label42)
        Me.tbpPerson.Controls.Add(Me.cboBranch)
        Me.tbpPerson.Controls.Add(Me.lblDelImg)
        Me.tbpPerson.Controls.Add(Me.txtFamilyOrder)
        Me.tbpPerson.Controls.Add(Me.picImage)
        Me.tbpPerson.Controls.Add(Me.chkDie)
        Me.tbpPerson.Controls.Add(Me.Label1)
        Me.tbpPerson.Controls.Add(Me.txtAlias)
        Me.tbpPerson.Controls.Add(Me.rdMale)
        Me.tbpPerson.Controls.Add(Me.rdFemale)
        Me.tbpPerson.Controls.Add(Me.rdUnknow)
        Me.tbpPerson.Controls.Add(Me.txtMidName)
        Me.tbpPerson.Controls.Add(Me.txtLastName)
        Me.tbpPerson.Controls.Add(Me.Label35)
        Me.tbpPerson.Controls.Add(Me.Label2)
        Me.tbpPerson.Controls.Add(Me.txtFirstName)
        Me.tbpPerson.Controls.Add(Me.Label24)
        Me.tbpPerson.Controls.Add(Me.Label23)
        Me.tbpPerson.Controls.Add(Me.Label3)
        Me.tbpPerson.Controls.Add(Me.Label4)
        Me.tbpPerson.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.tbpPerson.Location = New System.Drawing.Point(4, 25)
        Me.tbpPerson.Name = "tbpPerson"
        Me.tbpPerson.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpPerson.Size = New System.Drawing.Size(598, 528)
        Me.tbpPerson.TabIndex = 0
        Me.tbpPerson.Text = "Thông tin cơ bản"
        Me.tbpPerson.UseVisualStyleBackColor = True
        '
        'gbButton
        '
        Me.gbButton.Controls.Add(Me.picButton)
        Me.gbButton.Location = New System.Drawing.Point(7, 379)
        Me.gbButton.Name = "gbButton"
        Me.gbButton.Size = New System.Drawing.Size(581, 143)
        Me.gbButton.TabIndex = 314
        Me.gbButton.TabStop = False
        '
        'picButton
        '
        Me.picButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picButton.Cursor = System.Windows.Forms.Cursors.No
        Me.picButton.Image = CType(resources.GetObject("picButton.Image"), System.Drawing.Image)
        Me.picButton.InitialImage = Nothing
        Me.picButton.Location = New System.Drawing.Point(4, 18)
        Me.picButton.Name = "picButton"
        Me.picButton.Size = New System.Drawing.Size(571, 119)
        Me.picButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picButton.TabIndex = 314
        Me.picButton.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label55)
        Me.GroupBox5.Controls.Add(Me.Label53)
        Me.GroupBox5.Controls.Add(Me.Label30)
        Me.GroupBox5.Controls.Add(Me.btnBLunarCal)
        Me.GroupBox5.Controls.Add(Me.Label31)
        Me.GroupBox5.Controls.Add(Me.txtBYLunar)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.txtBMLunar)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.txtBYSun)
        Me.GroupBox5.Controls.Add(Me.txtBirthPlace)
        Me.GroupBox5.Controls.Add(Me.txtBDLunar)
        Me.GroupBox5.Controls.Add(Me.btnBSunCal)
        Me.GroupBox5.Controls.Add(Me.lblBYLunar)
        Me.GroupBox5.Controls.Add(Me.cbNation)
        Me.GroupBox5.Controls.Add(Me.txtBMSun)
        Me.GroupBox5.Controls.Add(Me.cbReligion)
        Me.GroupBox5.Controls.Add(Me.txtBDSun)
        Me.GroupBox5.Controls.Add(Me.Label47)
        Me.GroupBox5.Controls.Add(Me.Label46)
        Me.GroupBox5.Controls.Add(Me.Label45)
        Me.GroupBox5.Controls.Add(Me.Label44)
        Me.GroupBox5.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 224)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(581, 149)
        Me.GroupBox5.TabIndex = 165
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Thông tin ngày sinh"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label55.Location = New System.Drawing.Point(267, 29)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(108, 16)
        Me.Label55.TabIndex = 302
        Me.Label55.Text = "Ngày/Tháng/Năm"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label53.Location = New System.Drawing.Point(51, 54)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(36, 16)
        Me.Label53.TabIndex = 154
        Me.Label53.Text = "(ÂL):"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label30.Location = New System.Drawing.Point(16, 119)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(72, 16)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Quốc tịch :"
        '
        'btnBLunarCal
        '
        Me.btnBLunarCal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnBLunarCal.Location = New System.Drawing.Point(393, 52)
        Me.btnBLunarCal.Name = "btnBLunarCal"
        Me.btnBLunarCal.Size = New System.Drawing.Size(70, 22)
        Me.btnBLunarCal.TabIndex = 163
        Me.btnBLunarCal.Text = "Chọn lịch"
        Me.btnBLunarCal.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label31.Location = New System.Drawing.Point(267, 119)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(65, 16)
        Me.Label31.TabIndex = 4
        Me.Label31.Text = "Tôn giáo :"
        '
        'txtBYLunar
        '
        Me.txtBYLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtBYLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBYLunar.Location = New System.Drawing.Point(206, 51)
        Me.txtBYLunar.MaxLength = 4
        Me.txtBYLunar.Name = "txtBYLunar"
        Me.txtBYLunar.ReadOnly = True
        Me.txtBYLunar.Size = New System.Drawing.Size(51, 22)
        Me.txtBYLunar.TabIndex = 162
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Ngày sinh :"
        '
        'txtBMLunar
        '
        Me.txtBMLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtBMLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBMLunar.Location = New System.Drawing.Point(147, 51)
        Me.txtBMLunar.MaxLength = 2
        Me.txtBMLunar.Name = "txtBMLunar"
        Me.txtBMLunar.ReadOnly = True
        Me.txtBMLunar.Size = New System.Drawing.Size(35, 22)
        Me.txtBMLunar.TabIndex = 161
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label19.Location = New System.Drawing.Point(24, 91)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 16)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Nơi sinh :"
        '
        'txtBYSun
        '
        Me.txtBYSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtBYSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBYSun.Location = New System.Drawing.Point(206, 23)
        Me.txtBYSun.MaxLength = 4
        Me.txtBYSun.Name = "txtBYSun"
        Me.txtBYSun.ReadOnly = True
        Me.txtBYSun.Size = New System.Drawing.Size(51, 22)
        Me.txtBYSun.TabIndex = 159
        '
        'txtBirthPlace
        '
        Me.txtBirthPlace.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtBirthPlace.Location = New System.Drawing.Point(89, 88)
        Me.txtBirthPlace.MaxLength = 200
        Me.txtBirthPlace.Name = "txtBirthPlace"
        Me.txtBirthPlace.Size = New System.Drawing.Size(480, 22)
        Me.txtBirthPlace.TabIndex = 110
        '
        'txtBDLunar
        '
        Me.txtBDLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtBDLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBDLunar.Location = New System.Drawing.Point(88, 51)
        Me.txtBDLunar.MaxLength = 2
        Me.txtBDLunar.Name = "txtBDLunar"
        Me.txtBDLunar.ReadOnly = True
        Me.txtBDLunar.Size = New System.Drawing.Size(35, 22)
        Me.txtBDLunar.TabIndex = 160
        '
        'btnBSunCal
        '
        Me.btnBSunCal.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.btnBSunCal.Location = New System.Drawing.Point(393, 24)
        Me.btnBSunCal.Name = "btnBSunCal"
        Me.btnBSunCal.Size = New System.Drawing.Size(70, 22)
        Me.btnBSunCal.TabIndex = 100
        Me.btnBSunCal.Text = "Chọn lịch"
        Me.btnBSunCal.UseVisualStyleBackColor = True
        '
        'lblBYLunar
        '
        Me.lblBYLunar.AutoSize = True
        Me.lblBYLunar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblBYLunar.Location = New System.Drawing.Point(267, 54)
        Me.lblBYLunar.Name = "lblBYLunar"
        Me.lblBYLunar.Size = New System.Drawing.Size(70, 16)
        Me.lblBYLunar.TabIndex = 152
        Me.lblBYLunar.Text = "Nhâm Thìn"
        '
        'cbNation
        '
        Me.cbNation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbNation.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.cbNation.FormattingEnabled = True
        Me.cbNation.Items.AddRange(New Object() {"Afghan", "Albanian", "Algerian", "American", "Andorran", "Angolan", "Antiguans", "Argentinean", "Armenian", "Australian", "Austrian", "Azerbaijani", "Bahamian", "Bahraini", "Bangladeshi", "Barbadian", "Barbudans", "Batswana", "Belarusian", "Belgian", "Belizean", "Beninese", "Bhutanese", "Bolivian", "Bosnian", "Brazilian", "British", "Bruneian", "Bulgarian", "Burkinabe", "Burmese", "Burundian", "Cambodian", "Cameroonian", "Canadian", "Cape Verdean", "Central African", "Chadian", "Chilean", "Chinese", "Colombian", "Comoran", "Congolese", "Costa Rican", "Croatian", "Cuban", "Cypriot", "Czech", "Danish", "Djibouti", "Dominican", "Dutch", "East Timorese", "Ecuadorean", "Egyptian", "Emirian", "Equatorial Guinean", "Eritrean", "Estonian", "Ethiopian", "Fijian", "Filipino", "Finnish", "French", "Gabonese", "Gambian", "Georgian", "German", "Ghanaian", "Greek", "Grenadian", "Guatemalan", "Guinea-Bissauan", "Guinean", "Guyanese", "Haitian", "Herzegovinian", "Honduran", "Hungarian", "I-Kiribati", "Icelander", "Indian", "Indonesian", "Iranian", "Iraqi", "Irish", "Israeli", "Italian", "Ivorian", "Jamaican", "Japanese", "Jordanian", "Kazakhstani", "Kenyan", "Kittian and Nevisian", "Kuwaiti", "Kyrgyz", "Laotian", "Latvian", "Lebanese", "Liberian", "Libyan", "Liechtensteiner", "Lithuanian", "Luxembourger", "Macedonian", "Malagasy", "Malawian", "Malaysian", "Maldivan", "Malian", "Maltese", "Marshallese", "Mauritanian", "Mauritian", "Mexican", "Micronesian", "Moldovan", "Monacan", "Mongolian", "Moroccan", "Mosotho", "Motswana", "Mozambican", "Namibian", "Nauruan", "Nepalese", "New Zealander", "Nicaraguan", "Nigerian", "Nigerien", "North Korean", "Northern Irish", "Norwegian", "Omani", "Pakistani", "Palauan", "Panamanian", "Papua New Guinean", "Paraguayan", "Peruvian", "Polish", "Portuguese", "Qatari", "Romanian", "Russian", "Rwandan", "Saint Lucian", "Salvadoran", "Samoan", "San Marinese", "Sao Tomean", "Saudi", "Scottish", "Senegalese", "Serbian", "Seychellois", "Sierra Leonean", "Singaporean", "Slovakian", "Slovenian", "Solomon Islander", "Somali", "South African", "South Korean", "Spanish", "Sri Lankan", "Sudanese", "Surinamer", "Swazi", "Swedish", "Swiss", "Syrian", "Taiwanese", "Tajik", "Tanzanian", "Thai", "Togolese", "Tongan", "Trinidadian or Tobagonian", "Tunisian", "Turkish", "Tuvaluan", "Ugandan", "Ukrainian", "Uruguayan", "Uzbekistani", "Venezuelan", "Vietnamese", "Welsh", "Yemenite", "Zambian", "Zimbabwean"})
        Me.cbNation.Location = New System.Drawing.Point(88, 116)
        Me.cbNation.Name = "cbNation"
        Me.cbNation.Size = New System.Drawing.Size(169, 24)
        Me.cbNation.TabIndex = 120
        '
        'txtBMSun
        '
        Me.txtBMSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtBMSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBMSun.Location = New System.Drawing.Point(147, 23)
        Me.txtBMSun.MaxLength = 2
        Me.txtBMSun.Name = "txtBMSun"
        Me.txtBMSun.ReadOnly = True
        Me.txtBMSun.Size = New System.Drawing.Size(35, 22)
        Me.txtBMSun.TabIndex = 158
        '
        'cbReligion
        '
        Me.cbReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbReligion.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.cbReligion.FormattingEnabled = True
        Me.cbReligion.Location = New System.Drawing.Point(338, 116)
        Me.cbReligion.Name = "cbReligion"
        Me.cbReligion.Size = New System.Drawing.Size(125, 24)
        Me.cbReligion.TabIndex = 130
        '
        'txtBDSun
        '
        Me.txtBDSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtBDSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtBDSun.Location = New System.Drawing.Point(88, 23)
        Me.txtBDSun.MaxLength = 2
        Me.txtBDSun.Name = "txtBDSun"
        Me.txtBDSun.ReadOnly = True
        Me.txtBDSun.Size = New System.Drawing.Size(35, 22)
        Me.txtBDSun.TabIndex = 157
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label47.Location = New System.Drawing.Point(129, 23)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(12, 16)
        Me.Label47.TabIndex = 155
        Me.Label47.Text = "/"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label46.Location = New System.Drawing.Point(188, 54)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(12, 16)
        Me.Label46.TabIndex = 153
        Me.Label46.Text = "/"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label45.Location = New System.Drawing.Point(129, 54)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(12, 16)
        Me.Label45.TabIndex = 156
        Me.Label45.Text = "/"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label44.Location = New System.Drawing.Point(188, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(12, 16)
        Me.Label44.TabIndex = 151
        Me.Label44.Text = "/"
        '
        'pnDieInfo
        '
        Me.pnDieInfo.Controls.Add(Me.txtDYLunar)
        Me.pnDieInfo.Controls.Add(Me.txtDYSun)
        Me.pnDieInfo.Controls.Add(Me.txtDMLunar)
        Me.pnDieInfo.Controls.Add(Me.btnDSunCal)
        Me.pnDieInfo.Controls.Add(Me.lblDiePlace)
        Me.pnDieInfo.Controls.Add(Me.txtDDSun)
        Me.pnDieInfo.Controls.Add(Me.lblDieDate)
        Me.pnDieInfo.Controls.Add(Me.Label48)
        Me.pnDieInfo.Controls.Add(Me.txtBuryPlace)
        Me.pnDieInfo.Controls.Add(Me.txtDDLunar)
        Me.pnDieInfo.Controls.Add(Me.Label52)
        Me.pnDieInfo.Controls.Add(Me.Label49)
        Me.pnDieInfo.Controls.Add(Me.btnDLunarCal)
        Me.pnDieInfo.Controls.Add(Me.Label54)
        Me.pnDieInfo.Controls.Add(Me.lblDYLunar)
        Me.pnDieInfo.Controls.Add(Me.Label51)
        Me.pnDieInfo.Controls.Add(Me.Label50)
        Me.pnDieInfo.Controls.Add(Me.txtDMSun)
        Me.pnDieInfo.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.pnDieInfo.Location = New System.Drawing.Point(7, 388)
        Me.pnDieInfo.Name = "pnDieInfo"
        Me.pnDieInfo.Size = New System.Drawing.Size(581, 126)
        Me.pnDieInfo.TabIndex = 164
        Me.pnDieInfo.TabStop = False
        Me.pnDieInfo.Text = "Thông tin ngày mất"
        '
        'txtDYLunar
        '
        Me.txtDYLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtDYLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDYLunar.Location = New System.Drawing.Point(205, 55)
        Me.txtDYLunar.MaxLength = 4
        Me.txtDYLunar.Name = "txtDYLunar"
        Me.txtDYLunar.Size = New System.Drawing.Size(51, 22)
        Me.txtDYLunar.TabIndex = 312
        '
        'txtDYSun
        '
        Me.txtDYSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtDYSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDYSun.Location = New System.Drawing.Point(205, 27)
        Me.txtDYSun.MaxLength = 4
        Me.txtDYSun.Name = "txtDYSun"
        Me.txtDYSun.Size = New System.Drawing.Size(51, 22)
        Me.txtDYSun.TabIndex = 309
        '
        'txtDMLunar
        '
        Me.txtDMLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtDMLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDMLunar.Location = New System.Drawing.Point(146, 55)
        Me.txtDMLunar.MaxLength = 2
        Me.txtDMLunar.Name = "txtDMLunar"
        Me.txtDMLunar.Size = New System.Drawing.Size(35, 22)
        Me.txtDMLunar.TabIndex = 311
        '
        'btnDSunCal
        '
        Me.btnDSunCal.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.btnDSunCal.Location = New System.Drawing.Point(393, 24)
        Me.btnDSunCal.Name = "btnDSunCal"
        Me.btnDSunCal.Size = New System.Drawing.Size(70, 22)
        Me.btnDSunCal.TabIndex = 100
        Me.btnDSunCal.Text = "Chọn lịch"
        Me.btnDSunCal.UseVisualStyleBackColor = True
        '
        'lblDiePlace
        '
        Me.lblDiePlace.AutoSize = True
        Me.lblDiePlace.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblDiePlace.Location = New System.Drawing.Point(0, 90)
        Me.lblDiePlace.Name = "lblDiePlace"
        Me.lblDiePlace.Size = New System.Drawing.Size(84, 16)
        Me.lblDiePlace.TabIndex = 32
        Me.lblDiePlace.Text = "Nơi an táng :"
        '
        'txtDDSun
        '
        Me.txtDDSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtDDSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDDSun.Location = New System.Drawing.Point(87, 27)
        Me.txtDDSun.MaxLength = 2
        Me.txtDDSun.Name = "txtDDSun"
        Me.txtDDSun.Size = New System.Drawing.Size(35, 22)
        Me.txtDDSun.TabIndex = 307
        '
        'lblDieDate
        '
        Me.lblDieDate.AutoSize = True
        Me.lblDieDate.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblDieDate.Location = New System.Drawing.Point(15, 30)
        Me.lblDieDate.Name = "lblDieDate"
        Me.lblDieDate.Size = New System.Drawing.Size(72, 16)
        Me.lblDieDate.TabIndex = 28
        Me.lblDieDate.Text = "Ngày mất :"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label48.Location = New System.Drawing.Point(128, 30)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(12, 16)
        Me.Label48.TabIndex = 305
        Me.Label48.Text = "/"
        '
        'txtBuryPlace
        '
        Me.txtBuryPlace.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtBuryPlace.Location = New System.Drawing.Point(88, 87)
        Me.txtBuryPlace.MaxLength = 200
        Me.txtBuryPlace.Name = "txtBuryPlace"
        Me.txtBuryPlace.Size = New System.Drawing.Size(481, 22)
        Me.txtBuryPlace.TabIndex = 170
        '
        'txtDDLunar
        '
        Me.txtDDLunar.BackColor = System.Drawing.SystemColors.Window
        Me.txtDDLunar.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDDLunar.Location = New System.Drawing.Point(87, 55)
        Me.txtDDLunar.MaxLength = 2
        Me.txtDDLunar.Name = "txtDDLunar"
        Me.txtDDLunar.Size = New System.Drawing.Size(35, 22)
        Me.txtDDLunar.TabIndex = 310
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label52.Location = New System.Drawing.Point(16, 58)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(68, 16)
        Me.Label52.TabIndex = 302
        Me.Label52.Text = "(Âm lịch) :"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label49.Location = New System.Drawing.Point(128, 58)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(12, 16)
        Me.Label49.TabIndex = 306
        Me.Label49.Text = "/"
        '
        'btnDLunarCal
        '
        Me.btnDLunarCal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.btnDLunarCal.Location = New System.Drawing.Point(393, 55)
        Me.btnDLunarCal.Name = "btnDLunarCal"
        Me.btnDLunarCal.Size = New System.Drawing.Size(70, 22)
        Me.btnDLunarCal.TabIndex = 313
        Me.btnDLunarCal.Text = "Chọn lịch"
        Me.btnDLunarCal.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label54.Location = New System.Drawing.Point(266, 30)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(108, 16)
        Me.Label54.TabIndex = 301
        Me.Label54.Text = "Ngày/Tháng/Năm"
        '
        'lblDYLunar
        '
        Me.lblDYLunar.AutoSize = True
        Me.lblDYLunar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.lblDYLunar.Location = New System.Drawing.Point(266, 58)
        Me.lblDYLunar.Name = "lblDYLunar"
        Me.lblDYLunar.Size = New System.Drawing.Size(70, 16)
        Me.lblDYLunar.TabIndex = 301
        Me.lblDYLunar.Text = "Nhâm Thìn"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label51.Location = New System.Drawing.Point(187, 58)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(12, 16)
        Me.Label51.TabIndex = 303
        Me.Label51.Text = "/"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label50.Location = New System.Drawing.Point(187, 30)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(12, 16)
        Me.Label50.TabIndex = 304
        Me.Label50.Text = "/"
        '
        'txtDMSun
        '
        Me.txtDMSun.BackColor = System.Drawing.SystemColors.Window
        Me.txtDMSun.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.txtDMSun.Location = New System.Drawing.Point(146, 27)
        Me.txtDMSun.MaxLength = 2
        Me.txtDMSun.Name = "txtDMSun"
        Me.txtDMSun.Size = New System.Drawing.Size(35, 22)
        Me.txtDMSun.TabIndex = 308
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label42.Location = New System.Drawing.Point(346, 194)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(31, 16)
        Me.Label42.TabIndex = 148
        Me.Label42.Text = "Chi:"
        Me.Label42.Visible = False
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(383, 187)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(168, 24)
        Me.cboBranch.TabIndex = 147
        Me.cboBranch.Visible = False
        '
        'lblDelImg
        '
        Me.lblDelImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDelImg.Image = Global.phv.My.Resources.Resources.CloseBoxRed24
        Me.lblDelImg.Location = New System.Drawing.Point(566, 9)
        Me.lblDelImg.Name = "lblDelImg"
        Me.lblDelImg.Size = New System.Drawing.Size(18, 18)
        Me.lblDelImg.TabIndex = 146
        Me.ttpPersonInfo.SetToolTip(Me.lblDelImg, "Xóa ảnh đại diện của thành viên")
        Me.lblDelImg.Visible = False
        '
        'txtFamilyOrder
        '
        Me.txtFamilyOrder.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtFamilyOrder.Location = New System.Drawing.Point(115, 166)
        Me.txtFamilyOrder.Name = "txtFamilyOrder"
        Me.txtFamilyOrder.Size = New System.Drawing.Size(44, 22)
        Me.txtFamilyOrder.TabIndex = 80
        Me.txtFamilyOrder.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'picImage
        '
        Me.picImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picImage.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picImage.Image = CType(resources.GetObject("picImage.Image"), System.Drawing.Image)
        Me.picImage.ImageLocation = ""
        Me.picImage.InitialImage = Nothing
        Me.picImage.Location = New System.Drawing.Point(468, 9)
        Me.picImage.Margin = New System.Windows.Forms.Padding(0)
        Me.picImage.Name = "picImage"
        Me.picImage.Size = New System.Drawing.Size(116, 144)
        Me.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picImage.TabIndex = 27
        Me.picImage.TabStop = False
        Me.ttpPersonInfo.SetToolTip(Me.picImage, "Hình đại diện")
        '
        'chkDie
        '
        Me.chkDie.AutoSize = True
        Me.chkDie.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.chkDie.Location = New System.Drawing.Point(115, 198)
        Me.chkDie.Name = "chkDie"
        Me.chkDie.Size = New System.Drawing.Size(69, 20)
        Me.chkDie.TabIndex = 140
        Me.chkDie.Text = "Đã mất"
        Me.chkDie.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label1.Location = New System.Drawing.Point(49, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Giới tính :"
        '
        'rdMale
        '
        Me.rdMale.AutoSize = True
        Me.rdMale.Checked = True
        Me.rdMale.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.rdMale.Location = New System.Drawing.Point(115, 13)
        Me.rdMale.Name = "rdMale"
        Me.rdMale.Size = New System.Drawing.Size(53, 20)
        Me.rdMale.TabIndex = 10
        Me.rdMale.TabStop = True
        Me.rdMale.Text = "Nam"
        Me.rdMale.UseVisualStyleBackColor = True
        '
        'rdFemale
        '
        Me.rdFemale.AutoSize = True
        Me.rdFemale.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.rdFemale.Location = New System.Drawing.Point(192, 13)
        Me.rdFemale.Name = "rdFemale"
        Me.rdFemale.Size = New System.Drawing.Size(44, 20)
        Me.rdFemale.TabIndex = 20
        Me.rdFemale.TabStop = True
        Me.rdFemale.Text = "Nữ"
        Me.rdFemale.UseVisualStyleBackColor = True
        '
        'rdUnknow
        '
        Me.rdUnknow.AutoSize = True
        Me.rdUnknow.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.rdUnknow.Location = New System.Drawing.Point(260, 13)
        Me.rdUnknow.Name = "rdUnknow"
        Me.rdUnknow.Size = New System.Drawing.Size(116, 20)
        Me.rdUnknow.TabIndex = 30
        Me.rdUnknow.TabStop = True
        Me.rdUnknow.Text = "Không xác định"
        Me.rdUnknow.UseVisualStyleBackColor = True
        Me.rdUnknow.Visible = False
        '
        'txtMidName
        '
        Me.txtMidName.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtMidName.Location = New System.Drawing.Point(115, 71)
        Me.txtMidName.MaxLength = 100
        Me.txtMidName.Name = "txtMidName"
        Me.txtMidName.Size = New System.Drawing.Size(129, 22)
        Me.txtMidName.TabIndex = 50
        '
        'txtLastName
        '
        Me.txtLastName.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(115, 41)
        Me.txtLastName.MaxLength = 100
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(129, 22)
        Me.txtLastName.TabIndex = 40
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label35.Location = New System.Drawing.Point(43, 75)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(66, 16)
        Me.Label35.TabIndex = 4
        Me.Label35.Text = "Tên đệm :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label2.Location = New System.Drawing.Point(77, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Họ :"
        '
        'txtFirstName
        '
        Me.txtFirstName.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.txtFirstName.Location = New System.Drawing.Point(115, 101)
        Me.txtFirstName.MaxLength = 100
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(129, 22)
        Me.txtFirstName.TabIndex = 60
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label24.Location = New System.Drawing.Point(173, 168)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(86, 16)
        Me.Label24.TabIndex = 6
        Me.Label24.Text = "trong gia đình"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label23.Location = New System.Drawing.Point(43, 168)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(63, 16)
        Me.Label23.TabIndex = 6
        Me.Label23.Text = "Con thứ :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label3.Location = New System.Drawing.Point(72, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Tên :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Tên thường gọi :"
        '
        'tbcPersonInfo
        '
        Me.tbcPersonInfo.Controls.Add(Me.tbpPerson)
        Me.tbcPersonInfo.Controls.Add(Me.tbpCardID)
        Me.tbcPersonInfo.Controls.Add(Me.tbpFact)
        Me.tbcPersonInfo.Controls.Add(Me.tbpRemark)
        Me.tbcPersonInfo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.tbcPersonInfo.Location = New System.Drawing.Point(12, 49)
        Me.tbcPersonInfo.Name = "tbcPersonInfo"
        Me.tbcPersonInfo.SelectedIndex = 0
        Me.tbcPersonInfo.Size = New System.Drawing.Size(606, 557)
        Me.tbcPersonInfo.TabIndex = 3
        '
        'tbpCardID
        '
        Me.tbpCardID.Controls.Add(Me.GroupBox7)
        Me.tbpCardID.Controls.Add(Me.GroupBox6)
        Me.tbpCardID.Controls.Add(Me.txtAddress)
        Me.tbpCardID.Controls.Add(Me.txtHometown)
        Me.tbpCardID.Controls.Add(Me.Label5)
        Me.tbpCardID.Controls.Add(Me.Label7)
        Me.tbpCardID.Location = New System.Drawing.Point(4, 25)
        Me.tbpCardID.Name = "tbpCardID"
        Me.tbpCardID.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpCardID.Size = New System.Drawing.Size(598, 528)
        Me.tbpCardID.TabIndex = 1
        Me.tbpCardID.Text = "Liên hệ"
        Me.tbpCardID.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtRemarkContact)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 317)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(585, 205)
        Me.GroupBox7.TabIndex = 41
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Ghi chú"
        '
        'txtRemarkContact
        '
        Me.txtRemarkContact.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarkContact.Location = New System.Drawing.Point(14, 21)
        Me.txtRemarkContact.MaxLength = 500
        Me.txtRemarkContact.Multiline = True
        Me.txtRemarkContact.Name = "txtRemarkContact"
        Me.txtRemarkContact.Size = New System.Drawing.Size(557, 170)
        Me.txtRemarkContact.TabIndex = 28
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtPhone1)
        Me.GroupBox6.Controls.Add(Me.txtFax)
        Me.GroupBox6.Controls.Add(Me.Label14)
        Me.GroupBox6.Controls.Add(Me.Label43)
        Me.GroupBox6.Controls.Add(Me.txtURL)
        Me.GroupBox6.Controls.Add(Me.Label9)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.txtMail2)
        Me.GroupBox6.Controls.Add(Me.txtIM)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Controls.Add(Me.Label38)
        Me.GroupBox6.Controls.Add(Me.txtMail1)
        Me.GroupBox6.Controls.Add(Me.txtPhone2)
        Me.GroupBox6.Controls.Add(Me.Label39)
        Me.GroupBox6.Location = New System.Drawing.Point(6, 96)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(585, 215)
        Me.GroupBox6.TabIndex = 40
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Thông tin liên hệ"
        '
        'txtFax
        '
        Me.txtFax.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.Location = New System.Drawing.Point(411, 26)
        Me.txtFax.MaxLength = 50
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(160, 22)
        Me.txtFax.TabIndex = 25
        Me.txtFax.Visible = False
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label43.Location = New System.Drawing.Point(368, 29)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(38, 16)
        Me.Label43.TabIndex = 39
        Me.Label43.Text = "Fax :"
        Me.Label43.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(58, 184)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(73, 16)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "Facebook :"
        '
        'txtMail2
        '
        Me.txtMail2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMail2.Location = New System.Drawing.Point(141, 119)
        Me.txtMail2.MaxLength = 100
        Me.txtMail2.Name = "txtMail2"
        Me.txtMail2.Size = New System.Drawing.Size(430, 22)
        Me.txtMail2.TabIndex = 24
        '
        'txtIM
        '
        Me.txtIM.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIM.Location = New System.Drawing.Point(141, 181)
        Me.txtIM.MaxLength = 50
        Me.txtIM.Name = "txtIM"
        Me.txtIM.Size = New System.Drawing.Size(430, 22)
        Me.txtIM.TabIndex = 27
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(75, 122)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(56, 16)
        Me.Label15.TabIndex = 35
        Me.Label15.Text = "Email 2:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        Me.Label38.Location = New System.Drawing.Point(12, 57)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(120, 16)
        Me.Label38.TabIndex = 31
        Me.Label38.Text = "Điện thoại di động :"
        '
        'txtMail1
        '
        Me.txtMail1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMail1.Location = New System.Drawing.Point(141, 88)
        Me.txtMail1.MaxLength = 100
        Me.txtMail1.Name = "txtMail1"
        Me.txtMail1.Size = New System.Drawing.Size(430, 22)
        Me.txtMail1.TabIndex = 23
        '
        'txtPhone2
        '
        Me.txtPhone2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone2.Location = New System.Drawing.Point(141, 57)
        Me.txtPhone2.MaxLength = 50
        Me.txtPhone2.Name = "txtPhone2"
        Me.txtPhone2.Size = New System.Drawing.Size(230, 22)
        Me.txtPhone2.TabIndex = 22
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(75, 91)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(56, 16)
        Me.Label39.TabIndex = 33
        Me.Label39.Text = "Email 1:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(33, 54)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(104, 16)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Đ/C thường trú :"
        '
        'tbpJob
        '
        Me.tbpJob.Controls.Add(Me.rdCareerGeneral)
        Me.tbpJob.Controls.Add(Me.pnCareerDetail)
        Me.tbpJob.Controls.Add(Me.rdCareerDetail)
        Me.tbpJob.Controls.Add(Me.Label25)
        Me.tbpJob.Controls.Add(Me.pnCareerGeneral)
        Me.tbpJob.Location = New System.Drawing.Point(4, 25)
        Me.tbpJob.Name = "tbpJob"
        Me.tbpJob.Size = New System.Drawing.Size(598, 528)
        Me.tbpJob.TabIndex = 2
        Me.tbpJob.Text = "Nghề nghiệp"
        Me.tbpJob.UseVisualStyleBackColor = True
        Me.tbpJob.Visible = False
        '
        'rdCareerGeneral
        '
        Me.rdCareerGeneral.AutoSize = True
        Me.rdCareerGeneral.Location = New System.Drawing.Point(221, 8)
        Me.rdCareerGeneral.Name = "rdCareerGeneral"
        Me.rdCareerGeneral.Size = New System.Drawing.Size(71, 17)
        Me.rdCareerGeneral.TabIndex = 28
        Me.rdCareerGeneral.Text = "Tổng hợp"
        Me.rdCareerGeneral.UseVisualStyleBackColor = True
        '
        'pnCareerDetail
        '
        Me.pnCareerDetail.Controls.Add(Me.Label13)
        Me.pnCareerDetail.Controls.Add(Me.Label10)
        Me.pnCareerDetail.Controls.Add(Me.Label21)
        Me.pnCareerDetail.Controls.Add(Me.lblEndCareer)
        Me.pnCareerDetail.Controls.Add(Me.Label12)
        Me.pnCareerDetail.Controls.Add(Me.lblStartCareer)
        Me.pnCareerDetail.Controls.Add(Me.txtPosition)
        Me.pnCareerDetail.Controls.Add(Me.btnEndCareer)
        Me.pnCareerDetail.Controls.Add(Me.txtOccupt)
        Me.pnCareerDetail.Controls.Add(Me.btnStartCareer)
        Me.pnCareerDetail.Controls.Add(Me.txtOffName)
        Me.pnCareerDetail.Controls.Add(Me.btnDelCareer)
        Me.pnCareerDetail.Controls.Add(Me.txtOffAddr)
        Me.pnCareerDetail.Controls.Add(Me.btnCreateCareer)
        Me.pnCareerDetail.Controls.Add(Me.GroupBox2)
        Me.pnCareerDetail.Controls.Add(Me.btnSaveCareer)
        Me.pnCareerDetail.Controls.Add(Me.Label8)
        Me.pnCareerDetail.Controls.Add(Me.Label27)
        Me.pnCareerDetail.Location = New System.Drawing.Point(3, 34)
        Me.pnCareerDetail.Name = "pnCareerDetail"
        Me.pnCareerDetail.Size = New System.Drawing.Size(641, 316)
        Me.pnCareerDetail.TabIndex = 41
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(276, 36)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Nghề nghiệp :"
        '
        'lblEndCareer
        '
        Me.lblEndCareer.AutoSize = True
        Me.lblEndCareer.Location = New System.Drawing.Point(198, 86)
        Me.lblEndCareer.Name = "lblEndCareer"
        Me.lblEndCareer.Size = New System.Drawing.Size(44, 13)
        Me.lblEndCareer.TabIndex = 39
        Me.lblEndCareer.Text = "Chưa rõ"
        '
        'lblStartCareer
        '
        Me.lblStartCareer.AutoSize = True
        Me.lblStartCareer.Location = New System.Drawing.Point(198, 61)
        Me.lblStartCareer.Name = "lblStartCareer"
        Me.lblStartCareer.Size = New System.Drawing.Size(44, 13)
        Me.lblStartCareer.TabIndex = 39
        Me.lblStartCareer.Text = "Chưa rõ"
        '
        'txtPosition
        '
        Me.txtPosition.Location = New System.Drawing.Point(117, 30)
        Me.txtPosition.MaxLength = 50
        Me.txtPosition.Name = "txtPosition"
        Me.txtPosition.Size = New System.Drawing.Size(141, 20)
        Me.txtPosition.TabIndex = 31
        '
        'btnEndCareer
        '
        Me.btnEndCareer.Location = New System.Drawing.Point(117, 83)
        Me.btnEndCareer.Name = "btnEndCareer"
        Me.btnEndCareer.Size = New System.Drawing.Size(75, 23)
        Me.btnEndCareer.TabIndex = 34
        Me.btnEndCareer.Text = "Lịch"
        Me.btnEndCareer.UseVisualStyleBackColor = True
        '
        'txtOccupt
        '
        Me.txtOccupt.Location = New System.Drawing.Point(370, 33)
        Me.txtOccupt.MaxLength = 50
        Me.txtOccupt.Name = "txtOccupt"
        Me.txtOccupt.Size = New System.Drawing.Size(267, 20)
        Me.txtOccupt.TabIndex = 32
        '
        'btnStartCareer
        '
        Me.btnStartCareer.Location = New System.Drawing.Point(117, 57)
        Me.btnStartCareer.Name = "btnStartCareer"
        Me.btnStartCareer.Size = New System.Drawing.Size(75, 23)
        Me.btnStartCareer.TabIndex = 33
        Me.btnStartCareer.Text = "Lịch"
        Me.btnStartCareer.UseVisualStyleBackColor = True
        '
        'txtOffName
        '
        Me.txtOffName.Location = New System.Drawing.Point(117, 2)
        Me.txtOffName.MaxLength = 50
        Me.txtOffName.Name = "txtOffName"
        Me.txtOffName.Size = New System.Drawing.Size(141, 20)
        Me.txtOffName.TabIndex = 29
        '
        'btnDelCareer
        '
        Me.btnDelCareer.Location = New System.Drawing.Point(564, 73)
        Me.btnDelCareer.Name = "btnDelCareer"
        Me.btnDelCareer.Size = New System.Drawing.Size(68, 36)
        Me.btnDelCareer.TabIndex = 37
        Me.btnDelCareer.Text = "&Xóa"
        Me.btnDelCareer.UseVisualStyleBackColor = True
        '
        'txtOffAddr
        '
        Me.txtOffAddr.Location = New System.Drawing.Point(370, 1)
        Me.txtOffAddr.MaxLength = 200
        Me.txtOffAddr.Name = "txtOffAddr"
        Me.txtOffAddr.Size = New System.Drawing.Size(267, 20)
        Me.txtOffAddr.TabIndex = 30
        '
        'btnCreateCareer
        '
        Me.btnCreateCareer.Location = New System.Drawing.Point(358, 73)
        Me.btnCreateCareer.Name = "btnCreateCareer"
        Me.btnCreateCareer.Size = New System.Drawing.Size(68, 36)
        Me.btnCreateCareer.TabIndex = 35
        Me.btnCreateCareer.Text = "Tạo &Mới"
        Me.btnCreateCareer.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnDown1)
        Me.GroupBox2.Controls.Add(Me.btnUp1)
        Me.GroupBox2.Controls.Add(Me.dgvCareer)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 115)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(630, 198)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Danh sách các công việc"
        '
        'btnDown1
        '
        Me.btnDown1.Image = CType(resources.GetObject("btnDown1.Image"), System.Drawing.Image)
        Me.btnDown1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDown1.Location = New System.Drawing.Point(590, 57)
        Me.btnDown1.Name = "btnDown1"
        Me.btnDown1.Size = New System.Drawing.Size(33, 33)
        Me.btnDown1.TabIndex = 39
        Me.btnDown1.UseVisualStyleBackColor = True
        '
        'btnUp1
        '
        Me.btnUp1.Image = CType(resources.GetObject("btnUp1.Image"), System.Drawing.Image)
        Me.btnUp1.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnUp1.Location = New System.Drawing.Point(590, 18)
        Me.btnUp1.Name = "btnUp1"
        Me.btnUp1.Size = New System.Drawing.Size(33, 33)
        Me.btnUp1.TabIndex = 39
        Me.btnUp1.UseVisualStyleBackColor = True
        '
        'dgvCareer
        '
        Me.dgvCareer.AllowUserToAddRows = False
        Me.dgvCareer.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(163, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCareer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvCareer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCareer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmName, Me.clmAddr, Me.clmPosition, Me.clmOccupt, Me.clmTime, Me.clmTempTimeCareer})
        Me.dgvCareer.Dock = System.Windows.Forms.DockStyle.Left
        Me.dgvCareer.Location = New System.Drawing.Point(3, 16)
        Me.dgvCareer.MultiSelect = False
        Me.dgvCareer.Name = "dgvCareer"
        Me.dgvCareer.ReadOnly = True
        Me.dgvCareer.RowHeadersVisible = False
        Me.dgvCareer.RowTemplate.Height = 21
        Me.dgvCareer.RowTemplate.ReadOnly = True
        Me.dgvCareer.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCareer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCareer.Size = New System.Drawing.Size(581, 179)
        Me.dgvCareer.TabIndex = 38
        '
        'clmName
        '
        Me.clmName.HeaderText = "Tên cơ quan"
        Me.clmName.MinimumWidth = 110
        Me.clmName.Name = "clmName"
        Me.clmName.ReadOnly = True
        Me.clmName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmName.Width = 110
        '
        'clmAddr
        '
        Me.clmAddr.HeaderText = "Địa chỉ"
        Me.clmAddr.MinimumWidth = 110
        Me.clmAddr.Name = "clmAddr"
        Me.clmAddr.ReadOnly = True
        Me.clmAddr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmAddr.Width = 110
        '
        'clmPosition
        '
        Me.clmPosition.HeaderText = "Vị trí"
        Me.clmPosition.MinimumWidth = 110
        Me.clmPosition.Name = "clmPosition"
        Me.clmPosition.ReadOnly = True
        Me.clmPosition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmPosition.Width = 110
        '
        'clmOccupt
        '
        Me.clmOccupt.HeaderText = "Nghề nghiệp"
        Me.clmOccupt.MinimumWidth = 110
        Me.clmOccupt.Name = "clmOccupt"
        Me.clmOccupt.ReadOnly = True
        Me.clmOccupt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmOccupt.Width = 110
        '
        'clmTime
        '
        Me.clmTime.HeaderText = "Thời gian"
        Me.clmTime.MinimumWidth = 118
        Me.clmTime.Name = "clmTime"
        Me.clmTime.ReadOnly = True
        Me.clmTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.clmTime.Width = 135
        '
        'clmTempTimeCareer
        '
        Me.clmTempTimeCareer.HeaderText = "TempTime"
        Me.clmTempTimeCareer.Name = "clmTempTimeCareer"
        Me.clmTempTimeCareer.ReadOnly = True
        Me.clmTempTimeCareer.Visible = False
        '
        'btnSaveCareer
        '
        Me.btnSaveCareer.Location = New System.Drawing.Point(442, 73)
        Me.btnSaveCareer.Name = "btnSaveCareer"
        Me.btnSaveCareer.Size = New System.Drawing.Size(106, 36)
        Me.btnSaveCareer.TabIndex = 36
        Me.btnSaveCareer.Text = "&Lưu Thông tin"
        Me.btnSaveCareer.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(21, 61)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Thời gian từ :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(71, 86)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(32, 13)
        Me.Label27.TabIndex = 24
        Me.Label27.Text = "đến :"
        '
        'rdCareerDetail
        '
        Me.rdCareerDetail.AutoSize = True
        Me.rdCareerDetail.Checked = True
        Me.rdCareerDetail.Location = New System.Drawing.Point(133, 8)
        Me.rdCareerDetail.Name = "rdCareerDetail"
        Me.rdCareerDetail.Size = New System.Drawing.Size(57, 17)
        Me.rdCareerDetail.TabIndex = 25
        Me.rdCareerDetail.TabStop = True
        Me.rdCareerDetail.Text = "Chi tiết"
        Me.rdCareerDetail.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(5, 10)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(95, 13)
        Me.Label25.TabIndex = 40
        Me.Label25.Text = "Kiểu nhập dữ liệu :"
        '
        'pnCareerGeneral
        '
        Me.pnCareerGeneral.Controls.Add(Me.txtCareerGeneral)
        Me.pnCareerGeneral.Location = New System.Drawing.Point(3, 34)
        Me.pnCareerGeneral.Name = "pnCareerGeneral"
        Me.pnCareerGeneral.Size = New System.Drawing.Size(641, 289)
        Me.pnCareerGeneral.TabIndex = 41
        Me.pnCareerGeneral.Visible = False
        '
        'txtCareerGeneral
        '
        Me.txtCareerGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCareerGeneral.Location = New System.Drawing.Point(0, 0)
        Me.txtCareerGeneral.Multiline = True
        Me.txtCareerGeneral.Name = "txtCareerGeneral"
        Me.txtCareerGeneral.Size = New System.Drawing.Size(641, 289)
        Me.txtCareerGeneral.TabIndex = 0
        '
        'btnOK
        '
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.Location = New System.Drawing.Point(363, 18)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(103, 30)
        Me.btnOK.TabIndex = 180
        Me.btnOK.Text = "Lưu"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'dlgOpenImage
        '
        Me.dlgOpenImage.CheckFileExists = False
        Me.dlgOpenImage.Filter = "JPG (*.jpg)|*.jpg|GIF (*.gif)|*gif|BMP (*.bmp)|*.bmp|PNG (*.png)|*.png|All (*.*)|" & _
    "*.*"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnCancel)
        Me.GroupBox1.Controls.Add(Me.btnOK)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 612)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 57)
        Me.GroupBox1.TabIndex = 191
        Me.GroupBox1.TabStop = False
        '
        'frmPersonInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.ClientSize = New System.Drawing.Size(625, 672)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.tbcPersonInfo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmPersonInfo"
        Me.Text = "Thêm thành viên mới"
        Me.tbpEducation.ResumeLayout(False)
        Me.tbpEducation.PerformLayout()
        Me.pnEduDetail.ResumeLayout(False)
        Me.pnEduDetail.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvEdu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnEduGeneral.ResumeLayout(False)
        Me.pnEduGeneral.PerformLayout()
        Me.tbpRelationship.ResumeLayout(False)
        Me.tbpRelationship.PerformLayout()
        CType(Me.dgvRel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpFact.ResumeLayout(False)
        Me.tbpFact.PerformLayout()
        Me.pnFactDetail.ResumeLayout(False)
        Me.pnFactDetail.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvFact, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnFactGeneral.ResumeLayout(False)
        Me.pnFactGeneral.PerformLayout()
        Me.tbpRemark.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.tbpPerson.ResumeLayout(False)
        Me.tbpPerson.PerformLayout()
        Me.gbButton.ResumeLayout(False)
        CType(Me.picButton, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.pnDieInfo.ResumeLayout(False)
        Me.pnDieInfo.PerformLayout()
        CType(Me.txtFamilyOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbcPersonInfo.ResumeLayout(False)
        Me.tbpCardID.ResumeLayout(False)
        Me.tbpCardID.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.tbpJob.ResumeLayout(False)
        Me.tbpJob.PerformLayout()
        Me.pnCareerDetail.ResumeLayout(False)
        Me.pnCareerDetail.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvCareer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnCareerGeneral.ResumeLayout(False)
        Me.pnCareerGeneral.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtAlias As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtURL As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnSaveEdu As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbpEducation As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvEdu As System.Windows.Forms.DataGridView
    Friend WithEvents txtSchoolName As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtHometown As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tbpRelationship As System.Windows.Forms.TabPage
    Friend WithEvents tbpFact As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvFact As System.Windows.Forms.DataGridView
    Friend WithEvents txtFactPlace As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tbpRemark As System.Windows.Forms.TabPage
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents tbpPerson As System.Windows.Forms.TabPage
    Friend WithEvents chkDie As System.Windows.Forms.CheckBox
    Friend WithEvents cbReligion As System.Windows.Forms.ComboBox
    Friend WithEvents cbNation As System.Windows.Forms.ComboBox
    Friend WithEvents txtBuryPlace As System.Windows.Forms.TextBox
    Friend WithEvents lblDieDate As System.Windows.Forms.Label
    Friend WithEvents lblDiePlace As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rdMale As System.Windows.Forms.RadioButton
    Friend WithEvents rdFemale As System.Windows.Forms.RadioButton
    Friend WithEvents rdUnknow As System.Windows.Forms.RadioButton
    Friend WithEvents txtBirthPlace As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMidName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbcPersonInfo As System.Windows.Forms.TabControl
    Friend WithEvents tbpCardID As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbpJob As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCareer As System.Windows.Forms.DataGridView
    Friend WithEvents txtOffAddr As System.Windows.Forms.TextBox
    Friend WithEvents txtOffName As System.Windows.Forms.TextBox
    Friend WithEvents txtPosition As System.Windows.Forms.TextBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents ttpPersonInfo As System.Windows.Forms.ToolTip
    Friend WithEvents txtIM As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnCreateEdu As System.Windows.Forms.Button
    Friend WithEvents btnDelEdu As System.Windows.Forms.Button
    Friend WithEvents txtRemarkEdu As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtFactName As System.Windows.Forms.TextBox
    Friend WithEvents btnDelFact As System.Windows.Forms.Button
    Friend WithEvents btnCreateFact As System.Windows.Forms.Button
    Friend WithEvents btnSaveFact As System.Windows.Forms.Button
    Friend WithEvents txtFactDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnDelCareer As System.Windows.Forms.Button
    Friend WithEvents btnCreateCareer As System.Windows.Forms.Button
    Friend WithEvents btnSaveCareer As System.Windows.Forms.Button
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtMail1 As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtRemarkContact As System.Windows.Forms.TextBox
    Friend WithEvents txtMail2 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents picImage As System.Windows.Forms.PictureBox
    Friend WithEvents txtFax As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents dgvRel As System.Windows.Forms.DataGridView
    Friend WithEvents dlgOpenImage As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtOccupt As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents clmNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFullName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmRel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmRelBirth As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmRelRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtFamilyOrder As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnDSunCal As System.Windows.Forms.Button
    Friend WithEvents btnBSunCal As System.Windows.Forms.Button
    Friend WithEvents lblEndCareer As System.Windows.Forms.Label
    Friend WithEvents lblStartCareer As System.Windows.Forms.Label
    Friend WithEvents btnEndCareer As System.Windows.Forms.Button
    Friend WithEvents btnStartCareer As System.Windows.Forms.Button
    Friend WithEvents lblEndEdu As System.Windows.Forms.Label
    Friend WithEvents lblStartEdu As System.Windows.Forms.Label
    Friend WithEvents btnEndEdu As System.Windows.Forms.Button
    Friend WithEvents btnStartEdu As System.Windows.Forms.Button
    Friend WithEvents lblEndFact As System.Windows.Forms.Label
    Friend WithEvents lblStartFact As System.Windows.Forms.Label
    Friend WithEvents btnEndFact As System.Windows.Forms.Button
    Friend WithEvents btnStartFact As System.Windows.Forms.Button
    Friend WithEvents txtRemark As System.Windows.Forms.RichTextBox
    Friend WithEvents rdCareerGeneral As System.Windows.Forms.RadioButton
    Friend WithEvents rdCareerDetail As System.Windows.Forms.RadioButton
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnDown1 As System.Windows.Forms.Button
    Friend WithEvents btnUp1 As System.Windows.Forms.Button
    Friend WithEvents rdEduGeneral As System.Windows.Forms.RadioButton
    Friend WithEvents rdEduDetail As System.Windows.Forms.RadioButton
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btnDown2 As System.Windows.Forms.Button
    Friend WithEvents btnUp2 As System.Windows.Forms.Button
    Friend WithEvents clmSchoolName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmTimeEdu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmRemarkEdu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmTempTimeEdu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rdFactGeneral As System.Windows.Forms.RadioButton
    Friend WithEvents rdFactDetail As System.Windows.Forms.RadioButton
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents btnDown3 As System.Windows.Forms.Button
    Friend WithEvents btnUp3 As System.Windows.Forms.Button
    Friend WithEvents clmFactName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFactPlace As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFactTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFactDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmTempTimeFact As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmAddr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPosition As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmOccupt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmTempTimeCareer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnCareerDetail As System.Windows.Forms.Panel
    Friend WithEvents pnCareerGeneral As System.Windows.Forms.Panel
    Friend WithEvents txtCareerGeneral As System.Windows.Forms.TextBox
    Friend WithEvents pnEduDetail As System.Windows.Forms.Panel
    Friend WithEvents pnEduGeneral As System.Windows.Forms.Panel
    Friend WithEvents txtEduGeneral As System.Windows.Forms.TextBox
    Friend WithEvents pnFactDetail As System.Windows.Forms.Panel
    Friend WithEvents pnFactGeneral As System.Windows.Forms.Panel
    Friend WithEvents txtFactGeneral As System.Windows.Forms.TextBox
    Friend WithEvents lblDelImg As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents btnBLunarCal As System.Windows.Forms.Button
    Friend WithEvents txtBYLunar As System.Windows.Forms.TextBox
    Friend WithEvents txtBMLunar As System.Windows.Forms.TextBox
    Friend WithEvents txtBYSun As System.Windows.Forms.TextBox
    Friend WithEvents txtBDLunar As System.Windows.Forms.TextBox
    Friend WithEvents lblBYLunar As System.Windows.Forms.Label
    Friend WithEvents txtBMSun As System.Windows.Forms.TextBox
    Friend WithEvents txtBDSun As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txtDYLunar As System.Windows.Forms.TextBox
    Friend WithEvents txtDMLunar As System.Windows.Forms.TextBox
    Friend WithEvents txtDYSun As System.Windows.Forms.TextBox
    Friend WithEvents txtDDSun As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtDDLunar As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents lblDYLunar As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtDMSun As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents btnDLunarCal As System.Windows.Forms.Button
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents pnDieInfo As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents picButton As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents gbButton As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox

End Class
