﻿'CREATE : 2013/03/20  PHV 
Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Pdf
Imports PdfSharp.Pdf.IO

Public Class frmDrawTreeManual
    Private Const mcstrClsName As String = "frmDrawTreeManual"
    Private Const mcstrNoResult As String = "Không tìm thấy kết quả."                             'no result message
    Private mstSearchInfo As clsDbAccess.stSearch                                   'Search information
    Private mstResult As stSearchResult
    Private mtblSearchData As DataTable
    Private mtblLevel As DataTable                                                  'datatable from main form

    Private mobjImage() As XImage
    Private mobjCard() As usrMemCardBase
    Public mintMaxX As Integer = -1
    Public mintMinX As Integer = Integer.MaxValue

    Private mNoAvatar_Img As XImage
    Private mNoAvatar_F_Img As XImage
    Private mUnKnowAvatar_F_Img As XImage
    Private mCardBg As XImage
    Private mblnDrawS1 As Boolean
    Private mobjLoadingThread As System.Threading.Thread
    Private mfrmWaiting As frmProgress
    Private mstrFamAniInfo As String
    Private mstrFamInfo As String
    Private mstrCreateDate As String
    Private mstrRootInfo As String
    Private mstrCreateMem As String
    Private mblnShowBorder As Boolean

    Private Structure stSearchResult

        Dim intMemID As Integer                     'mem ID
        Dim intLevel As Integer                     'level/generation
        Dim strLastName As String                   'last name
        Dim strMidName As String                    'middle name
        Dim strFirstName As String                  'first name
        Dim strAlias As String                      'alias
        Dim intGen As Integer
        Dim intBranch As Integer
    End Structure

    Private Enum enmGridCol As Integer
        emChoose = 0
        emMemID = 1
        emName = 2
        emGen = 3
        emBranch = 4
        emGenCount = 5
    End Enum

    Public Sub New(ByVal tblLevel As DataTable)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        'new lunar calendar form
        Me.mtblLevel = tblLevel

    End Sub

    Public Function ShowForm(Optional ByVal blnDrawS1 As Boolean = True) As Boolean
        Try
            fncSetComboBranch(cboBranch)
            mblnDrawS1 = blnDrawS1
            Me.ShowDialog()
        Catch ex As Exception

        End Try
    End Function


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Try
            Me.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    '   ****************************************************************** 
    '      FUNCTION   : xSearch, make a search
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ******************************************************************
    Private Function xGetSearchData() As Boolean

        xGetSearchData = False

        Try
            'reset structure
            mstSearchInfo = Nothing

            With mstSearchInfo
                'get keyword
                .strKeyword = txtName.Text.Trim()
                .intBranch = fncCnvToInt(cboBranch.SelectedValue)
                .intGender = clsEnum.emGender.MALE

            End With

            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xGetSearchData", ex)
        End Try

    End Function

    '   ****************************************************************** 
    '      FUNCTION   : xSearch, make a search
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ******************************************************************
    Private Function xSearch(ByRef tblData As DataTable) As Boolean

        xSearch = False

        Try
            'do searching
            tblData = gobjDB.fncGetQuickSearch(mstSearchInfo)

            'if there is no data
            If tblData Is Nothing Then
                If dgvData.Rows.Count > 0 Then
                    For i As Integer = dgvData.Rows.Count - 1 To 0 Step -1
                        dgvData.Rows.RemoveAt(i)
                    Next
                End If
                basCommon.fncMessageWarning(mcstrNoResult)
                Exit Function
            End If

            'remove duplicated row
            Dim strCols(tblData.Columns.Count - 1) As String

            For index As Integer = 0 To tblData.Columns.Count - 1
                strCols(index) = tblData.Columns(index).ColumnName
            Next

            tblData = tblData.DefaultView.ToTable(True, strCols)

            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnSearch_Click", ex)
        End Try

    End Function

    '   ****************************************************************** 
    '      FUNCTION   : xFillGrid, fill data to grid
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : tblData DataTable, 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV  
    '      UPDATE     :  
    '   ******************************************************************
    Private Function xFillData(ByVal tblData As DataTable) As Boolean

        xFillData = False

        Dim strContent(5) As String

        Try
            For i As Integer = 0 To tblData.Rows.Count - 1

                With mstResult

                    'get data from specific row in datatable
                    xGetRowData(tblData, i)
                    'clear array before use it
                    Array.Clear(strContent, 0, strContent.Length)
                    'row number\
                    strContent(enmGridCol.emChoose) = "True"
                    strContent(enmGridCol.emMemID) = CStr(.intMemID)
                    'name
                    strContent(enmGridCol.emName) = basCommon.fncGetFullName(.strFirstName, .strMidName, .strLastName, .strAlias)
                    strContent(enmGridCol.emGen) = CStr(mstResult.intLevel)
                    strContent(enmGridCol.emBranch) = basCommon.fncGetBranchName(mstResult.intBranch)
                    strContent(enmGridCol.emGenCount) = CStr(gcintDefGeneration)
                    mtblSearchData.Rows.Add(strContent)

                End With

            Next

            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = mtblSearchData
            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xFillData", ex)
        Finally
            Erase strContent
        End Try

    End Function


    '   ****************************************************************** 
    '      FUNCTION   : xGetRowData, get data at specific row
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : tblData DataTable, 
    '                 : intRow Integer, row to get data
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ******************************************************************
    Private Function xGetRowData(ByVal tblData As DataTable, ByVal intRow As Integer) As Boolean

        xGetRowData = False

        Try
            'get data from specific row
            With tblData.Rows(intRow)

                'member id
                Integer.TryParse(basCommon.fncCnvNullToString(.Item("MEMBER_ID")), mstResult.intMemID)
                mstResult.intLevel = xGetLevel(mstResult.intMemID)
                'full name
                mstResult.strLastName = basCommon.fncCnvNullToString(.Item("LAST_NAME"))
                mstResult.strMidName = basCommon.fncCnvNullToString(.Item("MIDDLE_NAME"))
                mstResult.strFirstName = basCommon.fncCnvNullToString(.Item("FIRST_NAME"))
                mstResult.strAlias = basCommon.fncCnvNullToString(.Item("ALIAS_NAME"))
                mstResult.intBranch = basCommon.fncCnvToInt(.Item("BRANCH_ID"))
            End With

            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xGetRowData", ex)
        End Try

    End Function

    '   ************************************************************************ 
    '      FUNCTION   : xGetLevel, get level of a member
    '      VALUE      : Integer, -1 if member is not found.
    '      PARAMS     : intMemID    Integer, member id
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Function xGetLevel(ByVal intMemID As Integer) As Integer

        xGetLevel = -1

        Try
            Dim dtRow() As DataRow

            dtRow = mtblLevel.Select(String.Format("MEMBER_ID = {0}", intMemID))

            'return -1 if member is not found
            If dtRow.Length <= 0 Then Exit Function

            'else return level of that member
            Integer.TryParse(basCommon.fncCnvNullToString(dtRow(0)("LEVEL")), xGetLevel)

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xOpenPrintPreview", ex)
        End Try

    End Function
    '   ************************************************************************ 
    '      FUNCTION   : btnSearch_Click
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim tblData As New DataTable
            mtblSearchData = New DataTable
            mtblSearchData.Columns.Add("Choose")
            mtblSearchData.Columns.Add("Member_ID")
            mtblSearchData.Columns.Add("Name")
            mtblSearchData.Columns.Add("Gen")
            mtblSearchData.Columns.Add("Branch")
            mtblSearchData.Columns.Add("GenCount")
            Me.Cursor = Cursors.WaitCursor
            'get search data
            xGetSearchData()

            'do searching
            If Not xSearch(tblData) Then Exit Sub

            'fill grid
            xFillData(tblData)
        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnSearch_Click", ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    '   ************************************************************************ 
    '      FUNCTION   : xGetDataFromGrid
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Function xGetDataFromGrid() As Dictionary(Of Integer, Integer)
        xGetDataFromGrid = Nothing
        Dim objDicreturn As Dictionary(Of Integer, Integer)
        objDicreturn = Nothing
        Try
            If dgvData.Rows.Count <= 0 Then Exit Function
            objDicreturn = New Dictionary(Of Integer, Integer)
            For i As Integer = 0 To dgvData.Rows.Count - 1
                If Not IsDBNull(dgvData.Rows(i).Cells("Choose").Value) Then
                    If CBool(dgvData.Rows(i).Cells("Choose").Value) And fncCnvToInt(dgvData.Rows(i).Cells("GenCount").Value) > 0 Then
                        objDicreturn.Add(fncCnvToInt(dgvData.Rows(i).Cells("MemId").Value), fncCnvToInt(dgvData.Rows(i).Cells("GenCount").Value))
                    End If
                End If

            Next i
            Return objDicreturn
        Catch ex As Exception

        End Try
    End Function
    '   ************************************************************************ 
    '      FUNCTION   : btnExport_Click
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim objPdfOption As frmPdfOption
            If dgvData.RowCount > 0 Then
                'Get Pdf Option
                objPdfOption = New frmPdfOption
                If Not objPdfOption.fncShowFormGetOption(mstrRootInfo) Then Exit Sub
                mstrFamAniInfo = objPdfOption.FamilyAnniInfo
                mstrFamInfo = objPdfOption.FamilyInfo
                mstrCreateDate = objPdfOption.CreateDate
                mstrRootInfo = objPdfOption.RootInfo
                mstrCreateMem = objPdfOption.CreateMember
                mblnShowBorder = objPdfOption.ShowBorder
                mfrmWaiting = New frmProgress()

                mobjLoadingThread = New System.Threading.Thread(AddressOf xDraw)
                mobjLoadingThread.Start()
                'objThread.Start()
                mfrmWaiting.ShowDialog()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub xDraw()

        Dim objDrawTree As MethodInvoker = Nothing
        Try
            'set text
            objDrawTree = New MethodInvoker(AddressOf xInvokeDraw)
            Me.Invoke(objDrawTree)
        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xShowTree", ex)
        Finally
            objDrawTree = Nothing
        End Try

    End Sub
    Private Sub xInvokeDraw()
        Try
            Dim objDicData As New Dictionary(Of Integer, Integer)
            Dim intCount As Integer = 0
            objDicData = xGetDataFromGrid()
            If Not IsNothing(objDicData) Then
                xDrawToPdf(objDicData)
            End If
        Catch ex As Exception

        End Try
    End Sub

    '   ************************************************************************ 
    '      FUNCTION   : xDrawToPdf
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Function xDrawToPdf(ByVal objDic As Dictionary(Of Integer, Integer)) As Boolean
        Try
            Dim dtUser As New DataTable
            Dim dtSpouseRel As New DataTable
            Dim dtChildRel As New DataTable
            Dim dtCardInfo As New Hashtable
            Dim objCardControl As New Hashtable
            Dim objDrawA1 As clsDrawTreeA1 = Nothing
            Dim objDrawS1 As clsDrawTreeS1 = Nothing
            Dim objPair As KeyValuePair(Of Integer, Integer)
            Dim objPdf As clsPdf
            Dim lstNomalLine As List(Of usrLine) = Nothing
            Dim lstSpecialLine As List(Of usrLine) = Nothing

            Dim intMaxH As Integer = 0
            Dim intMaxW As Integer = 0
            Dim intId As Integer = 0
            Dim strPath As String = ""
            Dim strFolder As String = ""
            Dim i As Integer
            Dim objCard As usrMemCardBase
            Dim intTmp As Integer = 0
            Dim strRootInfo As String = ""
            Dim objPdfOption As frmPdfOption = Nothing

            strFolder = My.Application.Info.DirectoryPath & basConst.gcstrTempFolder
            'create temp folder
            If Not basCommon.fncCreateFolder(strFolder, True) Then Exit Function

            mNoAvatar_Img = fncMakeImage(My.Application.Info.DirectoryPath & "\docs\no_avatar_m.jpg")
            mNoAvatar_F_Img = fncMakeImage(My.Application.Info.DirectoryPath & "\docs\no_avatar_f.jpg")
            mUnKnowAvatar_F_Img = fncMakeImage(My.Application.Info.DirectoryPath & "\docs\UnknownMember.jpg")
            mCardBg = fncMakeImage(My.Settings.strCard1Bg)

            If mblnDrawS1 Then
                objDrawS1 = New clsDrawTreeS1(1, 1)
                If Not objDrawS1.fncReadData() Then Exit Function
                dtUser = objDrawS1.MemberList
                dtSpouseRel = objDrawS1.SpouseRel
                dtChildRel = objDrawS1.ChildRel
            Else
                objDrawA1 = New clsDrawTreeA1(1, 1)
                If Not objDrawA1.fncReadData() Then Exit Function
                dtUser = objDrawA1.MemberList
                dtSpouseRel = objDrawA1.SpouseRel
                dtChildRel = objDrawA1.ChildRel
            End If

            If IsNothing(objDic) Then Exit Function

            objPdf = New clsPdf(False)
            If objDic.Count > 0 Then
                objPdf.FamilyAnniInfo = mstrFamAniInfo
                objPdf.FamilyInfo = mstrFamInfo
                objPdf.CreateDate = mstrCreateDate
                objPdf.RootInfo = mstrRootInfo
                objPdf.CreateMember = mstrCreateMem
                objPdf.ShowBorder = mblnShowBorder
                For Each objPair In objDic
                    If mblnDrawS1 Then
                        objDrawS1 = New clsDrawTreeS1(objPair.Key, objPair.Value)
                        objDrawS1.MemberList = dtUser
                        objDrawS1.SpouseRel = dtSpouseRel
                        objDrawS1.ChildRel = dtChildRel
                        objDrawS1.fncGetDataToPdf(objPair.Key, objPair.Value, clsDefine.TREE_S1_STARTX, clsDefine.TREE_S1_STARTY)
                        'GetCard Info
                        dtCardInfo = objDrawS1.DrawList
                        'Get Card Control
                        objCardControl = objDrawS1.DrawingCard
                        'Get Nomal Lines
                        lstNomalLine = objDrawS1.NormalLine
                        'Get SpecialLines
                        lstSpecialLine = objDrawS1.SpecialLine

                        intMaxW = objDrawS1.MaxWidth
                        intMaxH = objDrawS1.MaxHeight
                    Else
                        objDrawA1 = New clsDrawTreeA1(objPair.Key, objPair.Value)
                        objDrawA1.MemberList = dtUser
                        objDrawA1.SpouseRel = dtSpouseRel
                        objDrawA1.ChildRel = dtChildRel
                        objDrawA1.fncGetDataToPdf(objPair.Key, objPair.Value, clsDefine.TREE_S1_STARTX, clsDefine.TREE_S1_STARTY)
                        'GetCard Info
                        dtCardInfo = objDrawA1.DrawList
                        'Get Card Control
                        objCardControl = objDrawA1.DrawingCard
                        'Get Nomal Lines
                        lstNomalLine = objDrawA1.NormalLine
                        'Get SpecialLines
                        lstSpecialLine = objDrawA1.SpecialLine

                        intMaxW = objDrawA1.MaxWidth
                        intMaxH = objDrawA1.MaxHeight
                    End If

                    objPdf.fncAddpage(intMaxW, intMaxH)
                    ReDim mobjImage(objCardControl.Count - 1)
                    ReDim mobjCard(objCardControl.Count - 1)
                    i = -1
                    For Each element As DictionaryEntry In objCardControl
                        objCard = CType(element.Value, usrMemCardBase)
                        If objCard.Visible = True Then
                            i = i + 1
                            intId = CInt(element.Key)
                            mobjCard(i) = objCard
                            If mintMinX > objCard.Location.X Then
                                mintMinX = objCard.Location.X
                            End If

                            If mintMaxX < objCard.Location.X + objCard.Width Then
                                mintMaxX = objCard.Location.X + objCard.Width
                            End If
                            strPath = String.Format(basConst.gcstrUsrCardFileFormat, strFolder, objCard.CardID)
                            If System.IO.File.Exists(strPath) Then
                                strPath = mobjCard(i).fncGetImage(strFolder, False)
                            Else
                                strPath = mobjCard(i).fncGetImage(strFolder)
                            End If


                            If My.Settings.intCardStyle = clsEnum.emCardStyle.CARD2 Then
                                mobjImage(i) = XImage.FromFile(strPath)
                            Else
                                mobjImage(i) = xGetMemberAvatarImage(CType(mobjCard(i), usrMemberCard1))
                            End If
                        End If
                    Next
                    If mblnDrawS1 Then
                        objPdf.RootInfo = "GIA ĐÌNH " + objDrawS1.RootMemberInfo
                    Else
                        objPdf.RootInfo = "GIA ĐÌNH " + objDrawA1.RootMemberInfo
                    End If

                    If objPdf.fncExportTree(mobjImage, mobjCard, lstNomalLine, lstSpecialLine, False) Then
                    End If
                    intTmp += 1
                Next
                xProgressDone()
                Dim dlgSaveFile As SaveFileDialog = New SaveFileDialog()

                dlgSaveFile.CheckPathExists = True
                dlgSaveFile.InitialDirectory = Application.StartupPath + "\List"
                dlgSaveFile.Title = "Cay pha he " & Now.ToString("ddMMyyyy") & ".pdf"
                dlgSaveFile.Filter = "Tệp tin PDF(*.pdf)|*.pdf|Tất cả các file(*.*)|*.*"

                If dlgSaveFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    objPdf.Save(dlgSaveFile.FileName)
                End If
            End If
            Return True
        Catch ex As Exception
            xProgressDone()
        End Try
    End Function

    '   ******************************************************************
    '　　　FUNCTION   : xGetMemberAvatarImage
    '      MEMO       : 
    '      CREATE     :2013/03/20  PHV 
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xGetMemberAvatarImage(ByVal objCard As usrMemberCard1) As XImage

        xGetMemberAvatarImage = mNoAvatar_Img
        Try

            If objCard.CardImageLocation() <> "" Then Return XImage.FromFile(objCard.CardImageLocation)

            If objCard.CardGender = clsEnum.emGender.FEMALE Then

                Return mNoAvatar_F_Img

            ElseIf objCard.CardGender = clsEnum.emGender.UNKNOW Then

                Return mUnKnowAvatar_F_Img

            End If
        Catch ex As Exception

        End Try


    End Function
    '   ************************************************************************ 
    '      FUNCTION   : frmDrawTreeManual_FormClosed
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Sub frmDrawTreeManual_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

            Dim i As Integer = 0

            Do While i < mobjImage.Length
                mobjImage(i).Dispose()
                mobjImage(i) = Nothing
                mobjCard(i) = Nothing
                i = i + 1
            Loop

            mobjImage = Nothing
            mobjCard = Nothing

            Dim strFolder As String
            strFolder = My.Application.Info.DirectoryPath & basConst.gcstrTempFolder
            basCommon.fncDeleteFolder(strFolder)

        Catch ex As Exception

        End Try
    End Sub
    '   ************************************************************************ 
    '      FUNCTION   : dgvData_CellClick
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Try
            If e.ColumnIndex = dgvData.Columns("GenCount").Index Then
                If dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Selected = True Then
                    DirectCast(dgvData.EditingControl, DataGridViewComboBoxEditingControl).DroppedDown = True
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub
    '   ************************************************************************ 
    '      FUNCTION   : chkSelectAll_Click
    '      VALUE      : 
    '      PARAMS     : 
    '      MEMO       :  
    '      CREATE     : 2013/03/20  PHV 
    '      UPDATE     :  
    '   ************************************************************************
    Private Sub chkSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.Click
        Try
            If chkSelectAll.Checked = True Then
                chkSelectAll.Text = "Bỏ chọn tất cả"
                If dgvData.RowCount > 0 Then
                    For i As Integer = 0 To dgvData.RowCount - 1
                        dgvData.Item(enmGridCol.emChoose, i).Value = "True"
                    Next
                End If
            Else
                chkSelectAll.Text = "Chọn tất cả"
                If dgvData.RowCount > 0 Then
                    For i As Integer = 0 To dgvData.RowCount - 1
                        dgvData.Item(enmGridCol.emChoose, i).Value = "False"
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub


    '   ******************************************************************
    '　　　FUNCTION   : xProgressDone, close waiting dialog
    '      MEMO       : 
    '      CREATE     : 2011/12/21  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Sub xProgressDone()

        Try
            Dim objCloseWaitForm As MethodInvoker

            'close thread
            mobjLoadingThread = Nothing

            'close waiting form
            objCloseWaitForm = New MethodInvoker(AddressOf xCloseWaitForm)
            Me.Invoke(objCloseWaitForm)

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xProgressDone", ex, Nothing, False)
        End Try

    End Sub


    '   ******************************************************************
    '　　　FUNCTION   : xCloseWaitForm, close waiting form
    '      MEMO       : 
    '      CREATE     : 2011/12/21  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Sub xCloseWaitForm()

        Try
            If mfrmWaiting IsNot Nothing Then
                mfrmWaiting.Close()
                mfrmWaiting.Dispose()
            End If
            mfrmWaiting = Nothing

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xCloseWaitForm", ex, Nothing, False)
        End Try

    End Sub

End Class