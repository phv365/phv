﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamilyEvents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFamilyEvents))
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDateDie1 = New System.Windows.Forms.Label()
        Me.lblDateDie = New System.Windows.Forms.Label()
        Me.lblBirthday1 = New System.Windows.Forms.Label()
        Me.lblBirthday = New System.Windows.Forms.Label()
        Me.lblDateVN = New System.Windows.Forms.Label()
        Me.lblDayOfWeek = New System.Windows.Forms.Label()
        Me.lblDayOfMonth = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblTitle.Location = New System.Drawing.Point(27, 19)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(313, 31)
        Me.lblTitle.TabIndex = 14
        Me.lblTitle.Text = "2013 - Tháng 1"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Silver
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Millimeter, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Gray
        Me.Label3.Location = New System.Drawing.Point(170, 266)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(1, 100)
        Me.Label3.TabIndex = 23
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Gray
        Me.Label1.Location = New System.Drawing.Point(7, 246)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(330, 16)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "----------------------------------------------------------------------------"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDateDie1
        '
        Me.lblDateDie1.AutoSize = True
        Me.lblDateDie1.BackColor = System.Drawing.Color.Transparent
        Me.lblDateDie1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateDie1.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblDateDie1.Location = New System.Drawing.Point(195, 298)
        Me.lblDateDie1.Margin = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.lblDateDie1.Name = "lblDateDie1"
        Me.lblDateDie1.Padding = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.lblDateDie1.Size = New System.Drawing.Size(10, 16)
        Me.lblDateDie1.TabIndex = 21
        '
        'lblDateDie
        '
        Me.lblDateDie.AutoSize = True
        Me.lblDateDie.BackColor = System.Drawing.Color.Transparent
        Me.lblDateDie.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateDie.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblDateDie.Location = New System.Drawing.Point(219, 266)
        Me.lblDateDie.Name = "lblDateDie"
        Me.lblDateDie.Size = New System.Drawing.Size(72, 16)
        Me.lblDateDie.TabIndex = 20
        Me.lblDateDie.Text = "NGÀY GIỖ"
        '
        'lblBirthday1
        '
        Me.lblBirthday1.AutoSize = True
        Me.lblBirthday1.BackColor = System.Drawing.Color.Transparent
        Me.lblBirthday1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthday1.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblBirthday1.Location = New System.Drawing.Point(28, 298)
        Me.lblBirthday1.Margin = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.lblBirthday1.Name = "lblBirthday1"
        Me.lblBirthday1.Padding = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.lblBirthday1.Size = New System.Drawing.Size(10, 16)
        Me.lblBirthday1.TabIndex = 19
        '
        'lblBirthday
        '
        Me.lblBirthday.AutoSize = True
        Me.lblBirthday.BackColor = System.Drawing.Color.Transparent
        Me.lblBirthday.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthday.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblBirthday.Location = New System.Drawing.Point(56, 266)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(78, 16)
        Me.lblBirthday.TabIndex = 18
        Me.lblBirthday.Text = "SINH NHẬT"
        '
        'lblDateVN
        '
        Me.lblDateVN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDateVN.AutoSize = True
        Me.lblDateVN.BackColor = System.Drawing.Color.Transparent
        Me.lblDateVN.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateVN.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblDateVN.Location = New System.Drawing.Point(52, 227)
        Me.lblDateVN.Name = "lblDateVN"
        Me.lblDateVN.Size = New System.Drawing.Size(255, 17)
        Me.lblDateVN.TabIndex = 17
        Me.lblDateVN.Text = "(Ngày 22 tháng 12 năm 2012, âm lịch)"
        Me.lblDateVN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDayOfWeek
        '
        Me.lblDayOfWeek.AutoSize = True
        Me.lblDayOfWeek.BackColor = System.Drawing.Color.Transparent
        Me.lblDayOfWeek.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDayOfWeek.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblDayOfWeek.Location = New System.Drawing.Point(127, 199)
        Me.lblDayOfWeek.Name = "lblDayOfWeek"
        Me.lblDayOfWeek.Size = New System.Drawing.Size(90, 24)
        Me.lblDayOfWeek.TabIndex = 16
        Me.lblDayOfWeek.Text = "THỨ TƯ"
        '
        'lblDayOfMonth
        '
        Me.lblDayOfMonth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDayOfMonth.BackColor = System.Drawing.Color.Transparent
        Me.lblDayOfMonth.Font = New System.Drawing.Font("Arial", 120.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDayOfMonth.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblDayOfMonth.Location = New System.Drawing.Point(32, 38)
        Me.lblDayOfMonth.Name = "lblDayOfMonth"
        Me.lblDayOfMonth.Size = New System.Drawing.Size(273, 183)
        Me.lblDayOfMonth.TabIndex = 15
        Me.lblDayOfMonth.Text = "1"
        Me.lblDayOfMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Gray
        Me.Label2.Location = New System.Drawing.Point(7, 282)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(330, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "----------------------------------------------------------------------------"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmFamilyEvents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(342, 416)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDateDie1)
        Me.Controls.Add(Me.lblDateDie)
        Me.Controls.Add(Me.lblBirthday1)
        Me.Controls.Add(Me.lblBirthday)
        Me.Controls.Add(Me.lblDateVN)
        Me.Controls.Add(Me.lblDayOfWeek)
        Me.Controls.Add(Me.lblDayOfMonth)
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFamilyEvents"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lịch ngày"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblDateDie1 As System.Windows.Forms.Label
    Friend WithEvents lblDateDie As System.Windows.Forms.Label
    Friend WithEvents lblBirthday1 As System.Windows.Forms.Label
    Friend WithEvents lblBirthday As System.Windows.Forms.Label
    Friend WithEvents lblDateVN As System.Windows.Forms.Label
    Friend WithEvents lblDayOfWeek As System.Windows.Forms.Label
    Friend WithEvents lblDayOfMonth As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
