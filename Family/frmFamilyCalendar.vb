﻿'******************************************************************
'   TITLE    : frmFamilyCalendar
'   FUNCTION : 
'   MEMO     : 無し
'   CREATE   : 2012/12/17 PHV
'******************************************************************
Option Explicit On
Option Strict On

Imports System.Data
Imports System.IO

Public Class frmFamilyCalendar

#Region "Form variable - Constrains"

    Private Const mcstrClsName As String = "frmFamilyCalendar"
    Private Const mintDefaultHeigh As Integer = 1024                                            'Default heigh
    Private Const mintDefaultWidth As Integer = 845
    Private Const mintDefaultWidthPanel As Integer = 860
    Private Const mintDefaultHeighForm As Integer = 660
    Private Const mntDefaultHeigh As Integer = 17
    Private Const mintLineBuffer As Integer = 2
    Private Const mintSeven As Integer = 7

    Private Const mcstrANI_DAY As String = "ANI_DAY"
    Private Const mcstrANI_MON As String = "ANI_MON"
    Private Const mcstrANI_YEA As String = "ANI_YEA"
    Private Const mcstrFomart As String = "{0:00}"
    Private Const mcstrFirstName As String = "Thang_"
    Private Const mcstrLastName As String = ".jpeg"

    Private mclsCom As clsCommon = New clsCommon()
    Private mdtbMonth As DataTable = New DataTable                                      'datatable

    'Default width for frame
    'Grid heigh
    Private mintStartCell As Integer = 0                                                'No of start cell
    Private mintMonth As Integer = 0                                                    'Month
    Private mintYear As Integer = 0                                                     'Year
    Private mintDay As Integer = 0                                                      'Day
    Private mintEnd As Integer = 0                                                      'No of last cell
    Private mTextSize As Size                                                           'Text size
    Private mintCountCell As Integer = -1

    Private mColorNavy As System.Drawing.Color = Color.Navy                             'Color navy
    Private mColorRed As System.Drawing.Color = Color.Red                               'Color red
    Private mArialBold As Font = New Font(Me.Font, FontStyle.Bold)                      'Font bold
    Private mArialItalic As Font = New Font(Me.Font, FontStyle.Italic)                  'Font italic
    Private mSolidNavy As SolidBrush = New SolidBrush(mColorNavy)                       'Solid brush

    Private mstrBirthPerson As String
    Private mstrDeathPerson As String
    'String list
    Private memMode As frmPersonalAnniversary.emFormMode                                                       'form mode
    Private mtblData As DataTable                                                       'datatable

    Private mobjVnCal As clsLunarCalendar                                               'lunar calendar
    'list of anni birth
    'list of anni decease

    'Two dimentional array
    Private mArrayInfo(,) As String
    Private mblnGetData As Boolean = False
    Private mblnResetHeigh As Boolean = False
    Private mintRowIndex As Integer = -1
    Private mintMaxRowInCell As Integer = 0

#End Region

    'Form mode
    Public Enum emFormMode

        BIRTH_LIST
        DECEASE_LIST

    End Enum

    '******************************************************************
    '　　　FUNCTION     : Form1_Load
    '　　　MEMO         : 無し 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/17 PHV  
    '******************************************************************
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            mdtbMonth.Clear()
            'Add columns

            mdtbMonth.Columns.Add(basConst.gcstrSun)
            mdtbMonth.Columns.Add(basConst.gcstrMon)
            mdtbMonth.Columns.Add(basConst.gcstrTue)
            mdtbMonth.Columns.Add(basConst.gcstrWed)
            mdtbMonth.Columns.Add(basConst.gcstrThur)
            mdtbMonth.Columns.Add(basConst.gcstrFir)
            mdtbMonth.Columns.Add(basConst.gcstrSat)

            If mintMonth = 0 Then
                cmdMonth.SelectedItem = DateTime.Now.Month.ToString
            End If

            If mintYear = 0 Then
                ndnYear.Value = DateTime.Now.Year
            End If



        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "Form1_Load", ex, Nothing, False)
        End Try

    End Sub

    '******************************************************************
    '　　　FUNCTION     : xBindingByMonthYear
    '　　　MEMO         : Binding data for gridview by month and year
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/20 PHV  
    '******************************************************************
    Private Function xBindingByMonthYear(ByVal intMonth As Integer, ByVal intYear As Integer, ByRef dtbMonth As DataTable) As Boolean
        Try
            xBindingByMonthYear = False

            If intMonth = 0 Then Return False
            If intYear = 1000 Then Return False

            Dim intStart As Integer = 0
            Dim intTotalRow As Integer = 0
            Dim strSun, strMon, strTue, strWed, strThur, strFri, strSat As String
            'Total days of month
            Dim intDaysOfMonth As Integer = mclsCom.fncGetLastDateOfMonth(intMonth, intYear)

            intTotalRow = (intDaysOfMonth \ mintSeven) + 1

            'Get datetime by month and year
            Dim strDate As String = basCommon.fncCnvNullToString(intMonth) & "/01/" & basCommon.fncCnvNullToString(intYear)

            dtbMonth.Rows.Clear()
            dgvCalendar.Controls.Clear()
            dgvCalendar.ClearSelection()

            'Get start cell by day of week's the first day of month
            mintStartCell = mclsCom.fncGetCellNo(mclsCom.fncGetFirstDayOfMonth(CType(strDate, Date)))

            mArrayInfo = New String(42, 3) {}

            For i As Integer = 1 To intDaysOfMonth Step mintSeven


                'Set start cell in gridview by the first day of month
                If i <= mintStartCell Then

                    xCreateFirstRow(mintStartCell, i, mintMonth, mintYear, dtbMonth)

                Else

                    intStart = i - mintStartCell

                    If intStart + mintSeven > intDaysOfMonth Then

                        mintEnd = intDaysOfMonth - intStart + 1

                        'Add last row
                        xCreateLastRow(mintEnd, intStart, dtbMonth, mintMonth, intDaysOfMonth)


                    Else

                        strSun = basCommon.fncCnvNullToString(intStart)
                        strMon = basCommon.fncCnvNullToString(intStart + 1)
                        strTue = basCommon.fncCnvNullToString(intStart + 2)
                        strWed = basCommon.fncCnvNullToString(intStart + 3)
                        strThur = basCommon.fncCnvNullToString(intStart + 4)
                        strFri = basCommon.fncCnvNullToString(intStart + 5)
                        strSat = basCommon.fncCnvNullToString(intStart + 6)

                        'Add row
                        dtbMonth.Rows.Add(strSun, strMon, strTue, strWed, strThur, strFri, strSat)

                        xEventsForCell(basCommon.fncCnvToInt(strSun), mintMonth, mintYear, i - 1, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strMon), mintMonth, mintYear, i, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strTue), mintMonth, mintYear, i + 1, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strWed), mintMonth, mintYear, i + 2, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strThur), mintMonth, mintYear, i + 3, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strFri), mintMonth, mintYear, i + 4, mArrayInfo, mtblData)
                        xEventsForCell(basCommon.fncCnvToInt(strSat), mintMonth, mintYear, i + 5, mArrayInfo, mtblData)

                        mintEnd = intDaysOfMonth - (intStart + 6)

                        'Add last row
                        If intStart + 6 >= i And intStart + mintSeven = intDaysOfMonth Then
                            xCreateLastRow(mintEnd, (intStart + 6) + 1, dtbMonth, mintMonth, intDaysOfMonth)
                            '30 days
                        ElseIf intStart + 6 >= i And intStart + mintSeven = 30 Then
                            xCreateLastRow(mintEnd, (intStart + mintSeven), dtbMonth, mintMonth, intDaysOfMonth)
                            '28 days
                        ElseIf intDaysOfMonth = 28 Then
                            xCreateLastRow(mintEnd, (intStart + mintSeven), dtbMonth, mintMonth, intDaysOfMonth)
                        End If

                    End If

                End If

            Next

            'bin data for gridview
            dgvCalendar.DataSource = dtbMonth

            'Set width for cell of datagridview
            mclsCom.fncStyleForGridView(dgvCalendar, mintStartCell, mintEnd)

            'Focus day
            If Date.Now.Month = intMonth Then

                For i As Integer = 0 To dgvCalendar.Columns.Count - 1

                    For j As Integer = 0 To dgvCalendar.Rows.Count - 1

                        If basCommon.fncCnvToInt(dgvCalendar(i, j).Value) = Date.Now.Day Then
                            dgvCalendar(i, j).Selected = True
                        End If

                    Next

                Next

            End If

            xBindingByMonthYear = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xBindingByMonthYear", ex, Nothing, False)
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : xCreateFirstRow
    '　　　MEMO         : Consist day of pre month and day of current month 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/17 PHV  
    '******************************************************************
    Private Function xCreateFirstRow(ByVal vintNumber As Integer, ByVal vintNumberOfFor As Integer, _
                                ByVal vintMonth As Integer, ByVal vintYear As Integer, ByRef vdtb As DataTable) As Boolean
        Try
            xCreateFirstRow = False
            Dim intStep As Integer = -1
            Dim strArray(6) As String
            Dim intDaysOfLastMonth As Integer = 0
            Dim intDate As Integer = 0
            Dim intMonth As Integer = 0
            Dim intYear As Integer = 0

            'Get Days of last month
            If vintMonth <> 1 Then
                intMonth = vintMonth - 1
                intYear = vintYear
            Else
                intMonth = 12
                intYear = vintYear - 1
            End If

            intDaysOfLastMonth = mclsCom.fncGetLastDateOfMonth(intMonth, intYear)

            'Set days of last month
            For j As Integer = intDaysOfLastMonth - (vintNumber - 1) To intDaysOfLastMonth Step 1
                intStep += 1
                strArray(intStep) = fncCnvNullToString(j)
                xEventsForCell(j, intMonth, intYear, intStep, mArrayInfo, mtblData)
            Next

            'Set days of current month
            For i As Integer = vintNumber To 6 Step 1
                intStep += 1
                intDate = i - vintNumber + 1
                strArray(intStep) = fncCnvNullToString(intDate)
                xEventsForCell(intDate, mintMonth, mintYear, intStep, mArrayInfo, mtblData)
            Next

            'Add row
            vdtb.Rows.Add(strArray)

            xCreateFirstRow = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xCreateFirstRow", ex, Nothing, False)
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : xCreateLastRow
    '　　　MEMO         : Consist day of last month and current month
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/17 PHV  
    '******************************************************************
    Private Function xCreateLastRow(ByVal vintNumber As Integer, _
                                ByVal vintNumberOfFor As Integer, _
                                ByRef vdtb As DataTable, _
                                ByVal vintMonth As Integer,
                                ByVal vintTotalNumber As Integer) As Boolean
        Try
            xCreateLastRow = False
            Dim intCount As Integer = 0
            Dim strArray(6) As String
            Dim intCellIndex As Integer = (mintStartCell + vintNumberOfFor) - 2
            Dim intMonth As Integer = 0
            Dim intYear As Integer = 0

            For j As Integer = 0 To vintNumber - 1 Step 1
                If vintNumber - 1 = 0 Then
                    strArray(j) = fncCnvNullToString(vintNumberOfFor)
                    intCellIndex += 1
                    xEventsForCell(vintNumberOfFor, mintMonth, mintYear, intCellIndex, mArrayInfo, mtblData)
                Else

                    If (vintNumberOfFor + j) <= vintTotalNumber Then
                        strArray(j) = fncCnvNullToString(vintNumberOfFor + j)
                        intCellIndex += 1
                        xEventsForCell(vintNumberOfFor + j, mintMonth, mintYear, intCellIndex, mArrayInfo, mtblData)
                    Else
                        Exit For
                    End If

                End If

            Next

            If vintMonth = 12 Then
                intMonth = 1
                intYear = mintYear + 1
            Else
                intMonth = vintMonth + 1
                intYear = mintYear
            End If

            'Set days of next month
            For i As Integer = vintNumber To 6 Step 1
                intCount = intCount + 1
                strArray(i) = fncCnvNullToString(intCount)
                intCellIndex += 1
                xEventsForCell(intCount, intMonth, intYear, intCellIndex, mArrayInfo, mtblData)
            Next

            'Add row
            vdtb.Rows.Add(strArray)

            xCreateLastRow = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xCreateLastRow", ex, Nothing, False)
        End Try

    End Function


    '******************************************************************
    '　　　FUNCTION     : xEventsForCell
    '　　　MEMO         : 
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/17 PHV  
    '******************************************************************
    Private Function xEventsForCell(ByVal vintDay As Integer, _
                                   ByVal vintMonth As Integer, _
                                   ByVal vintYear As Integer, _
                                   ByVal vintIndex As Integer, _
                                               ByRef vArray(,) As String, _
                                               ByVal vdtbData As DataTable) As Boolean
        Try
            xEventsForCell = False

            Dim dtmNewDate As DateTime = DateTime.MinValue
            dtmNewDate = New DateTime(vintYear, vintMonth, vintDay)

            mArrayInfo(vintIndex, basCommon.CellInfo.DateVN) = mclsCom.fncToVietNamCalendar(dtmNewDate)
            mArrayInfo(vintIndex, basCommon.CellInfo.Birthday) = mclsCom.fncGetEvents(vintDay, vintMonth, vintYear, mtblData, True)
            mArrayInfo(vintIndex, basCommon.CellInfo.DeathDay) = mclsCom.fncGetEvents(vintDay, vintMonth, vintYear, mtblData, False)

            xEventsForCell = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xEventsForCell", ex, Nothing, False)
        End Try
    End Function

    '******************************************************************
    '　　　FUNCTION     : dgvCalendar_CellPainting
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/26 PHV  
    '******************************************************************
    Private Sub dgvCalendar_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvCalendar.CellPainting
        Try

            If e.RowIndex = dgvCalendar.NewRowIndex Then Return

            mintCountCell = e.RowIndex * mintSeven + e.ColumnIndex

            Dim intWidth, intHeigh As Integer
            Dim strDateTypeVN As String = String.Empty
            Dim intLastRow As Integer = 0

            'Draw event   
            Dim intTotalRows As Integer = 0
            Dim intStartY As Integer = 25 + e.CellBounds.Top

            'Get day
            mintDay = basCommon.fncCnvToInt(e.Value)

            'Remove focus
            Dim backParts As DataGridViewPaintParts = _
            e.PaintParts And Not DataGridViewPaintParts.Focus

            'Draw cell
            e.Paint(e.CellBounds, backParts)

            strDateTypeVN = mArrayInfo(mintCountCell, basCommon.CellInfo.DateVN)
            mstrBirthPerson = mArrayInfo(mintCountCell, basCommon.CellInfo.Birthday)
            mstrDeathPerson = mArrayInfo(mintCountCell, basCommon.CellInfo.DeathDay)

            'Resize row height
            intTotalRows = fncGetNoEvent(mstrBirthPerson) + fncGetNoEvent(mstrDeathPerson)

            If mintRowIndex < e.RowIndex Then
                mintRowIndex = e.RowIndex
                mintMaxRowInCell = 0
            End If

            If mintMaxRowInCell < intTotalRows Then
                mintMaxRowInCell = intTotalRows

                mblnResetHeigh = False
                If Not mblnResetHeigh Then
                    If mintMaxRowInCell >= 5 Then
                        dgvCalendar.Rows(e.RowIndex).Height = (mintMaxRowInCell * mntDefaultHeigh) + 40
                        mblnResetHeigh = True
                    End If
                End If

            End If


            'Get size of text
            mTextSize = TextRenderer.MeasureText(strDateTypeVN, mArialBold)

            intWidth = e.CellBounds.Left + e.CellBounds.Width - basConst.gcintCellWidth
            intHeigh = e.CellBounds.Bottom - mTextSize.Height

            'Draw day of vietnamese
            e.Graphics.DrawString(strDateTypeVN, mArialBold, New SolidBrush(Color.Red), intWidth + 80, e.CellBounds.Top + 4)


            If mstrBirthPerson <> "" Then

                e.Graphics.DrawString(basConst.gcstrBirthday, mArialBold, mSolidNavy, intWidth, intStartY - mintLineBuffer)
                intStartY += mntDefaultHeigh - mintLineBuffer
                'Draw events
                xDrawEvents(mstrBirthPerson, e, intWidth, intStartY - mintLineBuffer, mntDefaultHeigh, intTotalRows)

            End If

            If mstrDeathPerson <> "" Then

                If mstrBirthPerson <> "" Then
                    intStartY = intStartY + mntDefaultHeigh
                End If
                e.Graphics.DrawString(basConst.gcstrDateDie, mArialBold, mSolidNavy, intWidth, intStartY)
                ' intStartY += mntDefaultHeigh

                'Draw events
                xDrawEvents(mstrDeathPerson, e, intWidth, intStartY + mntDefaultHeigh, mntDefaultHeigh, intTotalRows)

            End If

            e.Handled = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "dgvCalendar_CellPainting", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : xBindingCalendarToGridview
    '　　　MEMO         : binding data for gridview
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/20 PHV  
    '******************************************************************
    Private Function xBindingCalendarToGridview() As Boolean
        xBindingCalendarToGridview = False
        Try

            mintMonth = basCommon.fncCnvToInt(cmdMonth.SelectedItem)
            mintYear = basCommon.fncCnvToInt(ndnYear.Value)

            'Allow draw cell

            dgvCalendar.Visible = False

            'Get data
            If Not mblnGetData Then
                If Not xGetData() Then Exit Function
            End If

            Me.Cursor = Cursors.WaitCursor

            mintRowIndex = -1
            xBindingByMonthYear(mintMonth, mintYear, mdtbMonth)
            dgvCalendar.Visible = True

            xBindingCalendarToGridview = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xBindingCalendarToGridview", ex, Nothing, False)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : ndnYear_Click
    '　　　MEMO         : binding data by year
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/15 PHV  
    '******************************************************************
    Private Sub ndnYear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If Not xBindingCalendarToGridview() Then Exit Sub

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "ndnYear_Click", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : ndnYear_ValueChanged
    '　　　MEMO         : binding data by year
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/15 PHV  
    '******************************************************************
    Private Sub ndnYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ndnYear.ValueChanged
        Try
            If Not xBindingCalendarToGridview() Then Exit Sub

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "ndnYear_ValueChanged", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : cmdMonth_SelectedIndexChanged
    '　　　MEMO         : binding data by month
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/15 PHV
    '******************************************************************
    Private Sub cmdMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMonth.SelectedIndexChanged
        Try
            If Not xBindingCalendarToGridview() Then Exit Sub

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "cmdMonth_SelectedIndexChanged", ex, Nothing, False)
        End Try

    End Sub

    '   ******************************************************************
    '　　　FUNCTION   : xGetData, get data from database
    '      VALUE      : Boolean, true - success, false - failure
    '      PARAMS     : 
    '      MEMO       : 
    '      CREATE     : 2011/12/29  PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xGetData() As Boolean

        xGetData = False
        mtblData = New DataTable
        Try
            'Dim dtTempBirth As Date
            'Dim dtTempDecease As Date
            Dim stBirth As frmCalendar.stCalendar
            Dim stDeath As frmCalendar.stCalendar
            Dim intTempYear As Integer

            mtblData = gobjDB.fncGetMemberMain()

            If mtblData Is Nothing Then Exit Function


            mtblData.Columns.Add(mcstrANI_DAY)
            mtblData.Columns.Add(mcstrANI_MON)
            mtblData.Columns.Add(mcstrANI_YEA)

            'loop from end of datatable
            For i As Integer = mtblData.Rows.Count - 1 To 0 Step -1

                stBirth = Nothing
                stDeath = Nothing

                With stBirth
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("BIR_DAY_SUN")), .intDay)
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("BIR_MON_SUN")), .intMon)
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("BIR_YEA_SUN")), .intYea)
                End With

                With stDeath
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("DEA_DAY_LUNAR")), .intDay)
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("DEA_MON_LUNAR")), .intMon)
                    Integer.TryParse(basCommon.fncCnvNullToString(mtblData.Rows(i)("DEA_YEA_LUNAR")), .intYea)
                End With


                If stBirth.intDay <= 0 And stBirth.intMon <= 0 And stBirth.intYea <= 0 And stDeath.intDay <= 0 And stDeath.intMon <= 0 And stDeath.intYea <= 0 Then

                    mtblData.Rows.RemoveAt(i)
                    Continue For

                End If

                If memMode = emFormMode.BIRTH_LIST Then
                    'there is a value
                    'set ANI BIRTH value
                    If stBirth.intDay > 0 Or stBirth.intMon > 0 Or stBirth.intYea > 0 Then

                        'birthday already passed
                        If basCommon.fncCompareDate(Date.Today.Year, stBirth.intMon, stBirth.intDay, Date.Today) < 0 Then
                            intTempYear = Date.Today.Year + 1
                        Else
                            intTempYear = Date.Today.Year
                        End If

                        mtblData.Rows(i)(mcstrANI_DAY) = String.Format(mcstrFomart, stBirth.intDay)
                        mtblData.Rows(i)(mcstrANI_MON) = String.Format(mcstrFomart, stBirth.intMon)
                        mtblData.Rows(i)(mcstrANI_YEA) = intTempYear

                        If Date.IsLeapYear(intTempYear) And stBirth.intMon = 2 And stBirth.intDay = 29 Then
                            mtblData.Rows(i)(mcstrANI_DAY) = stBirth.intDay - 1
                        End If

                    End If

                Else
                    'set TEMP DECEASE
                    If stDeath.intDay <= 0 And stDeath.intMon <= 0 And stDeath.intYea <= 0 Then Continue For
                    If stDeath.intDay > 0 Or stDeath.intMon > 0 Or stDeath.intYea > 0 Then

                        mtblData.Rows(i)(mcstrANI_DAY) = String.Format(mcstrFomart, stDeath.intDay)

                        Dim intTmpMonth As Integer = 0

                        'Check Leap Year

                        If mobjVnCal.IsLeapYear(Date.Today.Year) Then
                            'Check Leap Month of year
                            For j As Integer = 1 To 12
                                If mobjVnCal.IsLeapMonth(Date.Today.Year, j) Then
                                    intTmpMonth = j
                                    Exit For
                                End If
                            Next
                            If stDeath.intMon > intTmpMonth Then
                                stDeath.intMon += 1
                            End If

                        End If
                        mtblData.Rows(i)(mcstrANI_MON) = String.Format(mcstrFomart, stDeath.intMon)

                        'birthday already passed
                        If basCommon.fncCompareDate(Date.Today.Year, stDeath.intMon, stDeath.intDay, Date.Today) < 0 Then
                            mtblData.Rows(i)(mcstrANI_YEA) = Date.Today.Year + 1
                        Else
                            mtblData.Rows(i)(mcstrANI_YEA) = Date.Today.Year
                        End If

                    End If
                End If

            Next

            Return True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xGetData", ex, Nothing, False)
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : fncGetNoEvent
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/26 PHV  
    '******************************************************************
    Private Function fncGetNoEvent(ByVal vstrEvent As String) As Integer
        fncGetNoEvent = 0

        Try
            If vstrEvent <> "" Then
                Dim strArray() As String = vstrEvent.Split(CChar(","))
                fncGetNoEvent = strArray.Length
            End If

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "fncGetNoEvent", ex, Nothing, False)
        End Try
    End Function

    '******************************************************************
    '　　　FUNCTION     : xDrawEvents
    '　　　MEMO         : Draw events
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/26 PHV  
    '******************************************************************
    Private Sub xDrawEvents(ByVal vstrEvent As String, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs, _
                            ByVal vintWidth As Integer, ByRef intStartY As Integer, ByRef intDefaultHeigh As Integer, ByRef vintTotalRows As Integer)
        Try

            Dim strArray() As String = vstrEvent.Split(CChar(","))

            For Each strName As String In strArray
                If strName <> "" Then
                    vintTotalRows += 1
                    e.Graphics.DrawString(strName, mArialItalic, mSolidNavy, vintWidth, intStartY)
                    intStartY += intDefaultHeigh
                Else
                    Exit For
                End If
            Next

            intStartY += mntDefaultHeigh

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xDrawEvents", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : lblPre_Click
    '　　　MEMO         : View last month
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/26 PHV  
    '******************************************************************
    Private Sub lblPre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblPre.Click
        Try
            dgvCalendar.Visible = False

            If CInt(cmdMonth.SelectedItem) = 1 Then
                mintMonth = 12
                mintYear = mintYear - 1
                ndnYear.DownButton()
            Else
                mintMonth = mintMonth - 1
            End If

            cmdMonth.SelectedItem = basCommon.fncCnvNullToString(mintMonth)

            'binding data for gridview
            If Not xBindingByMonthYear(mintMonth, mintYear, mdtbMonth) Then Exit Sub

            dgvCalendar.Visible = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "lblPre_Click", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : lblNext_Click
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/26 PHV  
    '******************************************************************
    Private Sub lblNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblNext.Click
        Try

            dgvCalendar.Visible = False

            If CInt(cmdMonth.SelectedItem) = 12 Then
                mintMonth = 1
                mintYear = mintYear + 1
                ndnYear.UpButton()
            Else
                mintMonth = mintMonth + 1
            End If

            cmdMonth.SelectedItem = basCommon.fncCnvNullToString(mintMonth)

            'binding data for gridview
            If Not xBindingByMonthYear(mintMonth, mintYear, mdtbMonth) Then Exit Sub

            dgvCalendar.Visible = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "lblNext_Click", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : dgvCalendar_CellDoubleClick
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/27 PHV  
    '****************************************************************** 

    Private Sub dgvCalendar_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCalendar.CellDoubleClick
        Try
            Dim intCellIndex As Integer = 0
            If e.RowIndex <> 0 Then
                intCellIndex = e.RowIndex * mintSeven + e.ColumnIndex
            Else
                intCellIndex = e.ColumnIndex
            End If

            'Popup form detail
            If Not xBindingFormDetail(intCellIndex) Then Exit Sub


        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "dgvCalendar_CellDoubleClick", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : xBindingFormDetail
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/27 PHV  
    '****************************************************************** 
    Private Function xBindingFormDetail(ByVal vintCell As Integer) As Boolean
        Try
            xBindingFormDetail = False

            frmFamilyEvents.Refresh()
            Dim dtmDate As DateTime = New DateTime(mintYear, mintMonth, mintDay)

            frmFamilyEvents.lblDayOfMonth.Text = String.Empty
            frmFamilyEvents.lblDayOfWeek.Text = String.Empty
            frmFamilyEvents.lblDateVN.Text = String.Empty

            'Set value for controls of frmFamilyEvents
            frmFamilyEvents.lblTitle.Text = basCommon.fncCnvNullToString(mintYear) & " - " & _
                                        basConst.gcstrMonth & basConst.gcstrDistant & _
                                         basCommon.fncCnvNullToString(mintMonth)

            frmFamilyEvents.lblDayOfMonth.Text = basCommon.fncCnvNullToString(mintDay)
            frmFamilyEvents.lblDayOfWeek.Text = mclsCom.fncToDayOfWeekVN(dtmDate).ToUpper()

            'Datetime by vietnamese calendar
            frmFamilyEvents.lblDateVN.Text = "(" & basConst.gcstrDay & basConst.gcstrDistant & _
                                              mclsCom.fncToDayVN(dtmDate) & basConst.gcstrDistant & _
                                              basConst.gcstrMonth & basConst.gcstrDistant & _
                                              mclsCom.fncToMonthVN(dtmDate) & basConst.gcstrDistant & _
                                              basConst.gcstrYear & basConst.gcstrDistant & _
                                              mclsCom.fncToYearVN(dtmDate) & _
                                          ")"

            mstrBirthPerson = mArrayInfo(vintCell, basCommon.CellInfo.Birthday)
            mstrDeathPerson = mArrayInfo(vintCell, basCommon.CellInfo.DeathDay)

            'Check and Draw event birthday
            If mstrBirthPerson IsNot Nothing Then
                frmFamilyEvents.lblBirthday1.Text = mstrBirthPerson.Replace(",", vbCrLf)
            End If

            'Draw event died
            If mstrDeathPerson IsNot Nothing Then
                frmFamilyEvents.lblDateDie1.Text = mstrDeathPerson.Replace(",", vbCrLf)
            End If

            xSetStyleByDayOfWeek(dtmDate)

            'Show form
            frmFamilyEvents.ShowDialog()

            xBindingFormDetail = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xBindingFormDetail", ex, Nothing, False)
        End Try
    End Function

    '******************************************************************
    '　　　FUNCTION     : xSetStyleByDayOfWeek
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/28 PHV  
    '******************************************************************
    Private Function xSetStyleByDayOfWeek(ByVal vday As Date) As Boolean
        Try
            xSetStyleByDayOfWeek = False

            'Check sunday and saturday, change forecolor
            If mclsCom.fncToDayOfWeekVN(vday) = basConst.gcstrSun Or _
            mclsCom.fncToDayOfWeekVN(vday) = basConst.gcstrSat Then

                frmFamilyEvents.lblTitle.ForeColor = mColorRed
                frmFamilyEvents.lblDayOfMonth.ForeColor = mColorRed
                frmFamilyEvents.lblDayOfWeek.ForeColor = mColorRed
                frmFamilyEvents.lblDateVN.ForeColor = mColorRed

            End If

            Select Case mclsCom.fncToDayOfWeekVN(vday)
                Case basConst.gcstrSun
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.cn
                Case basConst.gcstrMon
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t2
                Case basConst.gcstrTue
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t3
                Case basConst.gcstrWed
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t4
                Case basConst.gcstrThur
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t5
                Case basConst.gcstrFir
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t6
                Case basConst.gcstrSat
                    frmFamilyEvents.BackgroundImage = phv.My.Resources.t7
            End Select

            xSetStyleByDayOfWeek = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xSetStyleByDayOfWeek", ex, Nothing, False)
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : xDisplayTitle
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/28 PHV  
    '******************************************************************
    Private Function xDisplayTitle(ByVal vblnCheck As Boolean) As Boolean
        Try
            xDisplayTitle = False

            If vblnCheck = True Then

                lblTitleForExport.Text = basConst.gcstrMonth & basConst.gcstrDistant & _
                                         basCommon.fncCnvNullToString(mintMonth) & basConst.gcstrDistant & _
                                         basConst.gcstrYear & basConst.gcstrDistant & _
                                         basCommon.fncCnvNullToString(mintYear)
                lblTitleForExport.Visible = True
                lblNext.Visible = False
                lblPre.Visible = False
                lblMonth.Visible = False
                cmdMonth.Visible = False
                lblYear.Visible = False
                ndnYear.Visible = False

                'Width - heigh
                dgvCalendar.ScrollBars = ScrollBars.None
                dgvCalendar.AutoSize = True

                dgvCalendar.Height = mintDefaultHeigh

                Panel1.BorderStyle = BorderStyle.None
                Panel1.Height = dgvCalendar.Height
                Panel1.Width = mintDefaultWidth
                Panel2.Width = mintDefaultWidth


            Else

                lblTitleForExport.Visible = False
                lblNext.Visible = True
                lblPre.Visible = True
                lblMonth.Visible = True
                cmdMonth.Visible = True
                lblYear.Visible = True
                ndnYear.Visible = True
                dgvCalendar.AutoSize = False
                dgvCalendar.ScrollBars = ScrollBars.Vertical
                Panel1.BorderStyle = BorderStyle.FixedSingle
                Panel1.Height = mintDefaultHeighForm
                Panel1.Width = mintDefaultWidthPanel
                Panel2.Width = mintDefaultWidthPanel
                dgvCalendar.Height = mintDefaultHeighForm
                dgvCalendar.Width = mintDefaultWidthPanel

            End If

            xDisplayTitle = True

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xDisplayTitle", ex, Nothing, False)
        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : btnPdf_Click
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/28 PHV  
    '******************************************************************  
    Private Sub btnPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPdf.Click
        Try
            Dim strFilePath As String = My.Application.Info.DirectoryPath & basConst.gcstrDocsFolder

            xDisplayTitle(True)

            'Check and create temp folder
            If Not Directory.Exists(strFilePath) Then
                Directory.CreateDirectory(strFilePath)
            End If

            'Get path
            Dim strPath As String = strFilePath & mcstrFirstName & mintMonth.ToString & mcstrLastName
            'Export
            If Not mclsCom.fncCnvImageToPdf(strPath, Panel1) Then
                MessageBox.Show(basConst.gcstrMsError_Pdf)
            End If

            xDisplayTitle(False)

            'Delete file images
            File.Delete(strPath)

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnPdf_Click", ex, Nothing, False)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : btnCancel_Click
    '　　　MEMO         : 
    '　　　VALUE        :  
    '　　　PARAMS       : 
    '　　　CREATE       : 2012/12/28 PHV  
    '******************************************************************  
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "btnCancel_Click", ex, Nothing, False)
        End Try
    End Sub

End Class