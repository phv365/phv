﻿Public Class frmBranchMngt
    Private mcstrClsName As String = "frmBranchMngt"
    Private vstrDatafields As String() = {"BRANCH_ID", "BRANCH_NAME", "BRANCH_MANAGER", "BRANCH_MANAGER_NAME"}
    Private mstrHeader As String() = {"Mã chi", "Tên chi", "RootID", "Trưởng chi"}
    Private mintWidth As Integer() = {60, 330, 0, 280}
    Private mintAlign As DataGridViewContentAlignment() = {DataGridViewContentAlignment.MiddleLeft, _
                                                           DataGridViewContentAlignment.MiddleLeft, _
                                                           DataGridViewContentAlignment.MiddleLeft, _
                                                           DataGridViewContentAlignment.MiddleLeft}
    Private Const mcstrDelConfirm As String = "Bạn có chắc chắn muốn xóa chi này?"
    Private Const mcstrSaveConfirm As String = "Bạn có chắc chắn muốn lưu dữ liệu hiện tại?"
    Private Const mcstrMissingBranchName As String = "Bạn phải nhập dữ liệu tên của chi họ."
    Private Const mcstrExistBranchName As String = "Tên của chi đã tồn tại, bạn hãy vui lòng nhập lại."
    Private Const mcstrMissingRootBranch As String = "Bạn phải nhập dữ liệu của trưởng chi."

    Private mintBranchId As Integer = 0
    Private mintRoodID As Integer = 0
    Private mstrBranchMngName As String = ""
    Private mstrBranchNameSelected As String = ""
    Private mintRowSelectedIndex As Integer
    Private mblnUpdate As Boolean
    
    Private Enum emFormMode

        AddNew = 1
        Edit = 2

    End Enum

    Private Enum enmGridCol
        em_BranchID = 0
        em_BranchName = 1
        em_BranchMngId = 2
        em_BranchMngName = 3

    End Enum

    Private mintFormMade As Integer

    ''' <summary>
    ''' The change is made
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Updated() As Boolean
        Get
            Return mblnUpdate
        End Get
    End Property


    '   ******************************************************************
    '　　　FUNCTION   : xMakeGrid
    '　　　VALUE      : 
    '      PARAMS     : 
    '      MEMO       : 
    '      CREATE     : 2010/12/13 PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xMakeGrid() As Boolean

        xMakeGrid = False

        Try

            fncMakeGrid(dgvData, vstrDatafields, mstrHeader, mintWidth, mintAlign)
            xMakeGrid = True

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "xMakeGrid", ex)

        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : make data grid
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Public Function fncMakeGrid(ByRef rgrdData As DataGridView, _
                                ByVal vstrDataFields() As String, _
                                ByVal vstrHeadername() As String, _
                                ByVal vlngColWidth() As Integer, _
                                ByVal vlngColAlign() As DataGridViewContentAlignment, _
                                Optional ByVal vlngMaxInputLength As Object = Nothing) As Boolean

        fncMakeGrid = False

        Try

            Dim strTemp As String
            Dim i As Integer
            Dim lngMaxinputlength As Integer()
            ReDim lngMaxinputlength(vstrHeadername.Length)
            strTemp = ""
            If Not IsNothing(vlngMaxInputLength) Then

                lngMaxinputlength = vlngMaxInputLength
            End If

            rgrdData.Columns.Clear()

            With rgrdData

                .AllowUserToAddRows = False
                .AllowUserToResizeRows = False
                .AllowUserToResizeColumns = False
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .EditMode = DataGridViewEditMode.EditProgrammatically
                .MultiSelect = False
                .RowHeadersVisible = False

                For i = 0 To vstrHeadername.Length - 1
                    If Not IsNothing(vlngMaxInputLength) Then
                        .Columns.Add(fncMakeGridTextColum(vstrDataFields(i), vstrHeadername(i), vlngColWidth(i), vlngColAlign(i), lngMaxinputlength(i)))

                    Else
                        .Columns.Add(fncMakeGridTextColum(vstrDataFields(i), vstrHeadername(i), vlngColWidth(i), vlngColAlign(i)))
                    End If
                Next i

                .DataSource = Nothing


            End With

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "fncMakeGrid", ex)

        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     : make data grid column
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Public Function fncMakeGridTextColum(ByVal vstrDataField As String, _
                                         ByVal vstrHeader As String, _
                                         ByVal vintWidth As Integer, _
                                         ByVal vintAlign As DataGridViewContentAlignment, _
                                        Optional ByVal vintMaxInput As Integer = 0) As DataGridViewTextBoxColumn

        fncMakeGridTextColum = Nothing

        Try

            Dim objGridCol As New DataGridViewTextBoxColumn

            With objGridCol

                .DataPropertyName = vstrDataField
                .Width = vintWidth

                .DefaultCellStyle.Alignment = vintAlign
                .DefaultCellStyle.Font = New Font("Arial", 9, FontStyle.Regular)

                .HeaderCell.Style.Font = New Font("Arial", 9, FontStyle.Bold)
                .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .HeaderText = vstrHeader
                If vintMaxInput <> 0 Then
                    .MaxInputLength = vintMaxInput
                End If

                .SortMode = DataGridViewColumnSortMode.NotSortable

                If vintWidth = 0 Then
                    .Visible = False
                End If



            End With

            Return objGridCol

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "fncMakeGridTextColum", ex)

        End Try

    End Function


    '   ******************************************************************
    '　　　FUNCTION   : Set data to Gridview
    '　　　VALUE      : 
    '      PARAMS     : 
    '      MEMO       : 
    '      CREATE     : 2010/12/13 PHV
    '      UPDATE     : 
    '   ******************************************************************
    Private Function xSetGridData() As Boolean

        xSetGridData = False

        Try

            Dim objData As DataTable


            objData = gobjDB.fncGetBranchList("")

            If Not fncSetGridData(dgvData, objData, vstrDatafields) Then Return False
            If IsNothing(objData) Then

                mintFormMade = emFormMode.AddNew

            End If

            xSetGridData = True

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "xSetGridData", ex)

        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     :Set data to Grid
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Public Function fncSetGridData(ByRef rgrdData As DataGridView, _
                                   ByVal vobjData As DataTable, _
                                   ByVal vstrDataFields() As String) As Boolean
        fncSetGridData = False

        Try
            Dim i, j As Integer

            Dim strData() As String



            If rgrdData.Rows.Count > 0 Then

                For i = 0 To rgrdData.Rows.Count - 1

                    rgrdData.Rows.RemoveAt(0)

                Next

            End If

            If IsNothing(vobjData) Then

                Return True

            End If

            For i = 0 To vobjData.Rows.Count - 1

                ReDim strData(vstrDataFields.Length - 1)

                For j = 0 To vstrDataFields.Length - 2
                    strData(j) = fncCnvNullToString(vobjData.Rows(i).Item(j))
                    'Lay ve ten cua truong chi tu ID 
                    If j = 2 Then
                        strData(enmGridCol.em_BranchMngName) = fncGetMemberName(fncCnvToInt(vobjData.Rows(i).Item(enmGridCol.em_BranchMngId)))
                    End If
                Next j

                rgrdData.Rows.Add(strData)

            Next i

            rgrdData.ClearSelection()

            fncSetGridData = True

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "fncSetGridData", ex)

        End Try

    End Function

    '******************************************************************
    '　　　FUNCTION     :ShowForm
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Public Sub ShowForm()

        xClear()
        mblnUpdate = False
        mintFormMade = emFormMode.Edit
        If Not xMakeGrid() Then Exit Sub

        If Not xSetGridData() Then Exit Sub

        xBindingFromGridToTexBox(0)
        txtBranchName.Focus()

        Me.ShowDialog()


    End Sub
    '******************************************************************
    '　　　FUNCTION     :btnSave_Click
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click

        Try

            If Not fncMessageConfirm(mcstrSaveConfirm) Then Return

            If dgvData.SelectedRows.Count > 0 Then
                mintFormMade = emFormMode.Edit
            Else
                mintFormMade = emFormMode.AddNew
            End If

            If dgvData.SelectedRows.Count > 0 And mintRoodID = 0 Then
                mintRoodID = basCommon.fncCnvToInt(dgvData.SelectedRows.Item(0).Cells(enmGridCol.em_BranchMngId).Value.ToString)
            End If

            If Not xCheckData() Then Exit Sub

            If mintBranchId <> 0 Then
                gobjDB.fncUpdateBranch(txtBranchName.Text, mintRoodID, mintBranchId)
                mintRowSelectedIndex = dgvData.CurrentRow.Index
            Else
                gobjDB.fncSetBranch(txtBranchName.Text, mintRoodID)
            End If

            xSetGridData()
            xSelectRow()
            mintFormMade = emFormMode.Edit

            fncMessageInfo("Dữ liệu được lưu thành công!")
            mblnUpdate = True

        Catch ex As Exception

            fncSaveErr(mcstrClsName, "btnSave_Click", ex)

        End Try
    End Sub
    '******************************************************************
    '　　　FUNCTION     :btnCancel_Click
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV 
    '      UPDATE       : 
    '******************************************************************
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub
    '******************************************************************
    '　　　FUNCTION     :dgvData_CellClick
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub dgvData_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Try

            'exit if it is header
            If e.RowIndex < 0 Then Exit Sub
            xBindingFromGridToTexBox(e.RowIndex)
            mintFormMade = emFormMode.Edit
        Catch ex As Exception
            fncSaveErr(mcstrClsName, "dgvData_CellClick", ex)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     :btnAdd_Click
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2010/12/13 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            xClear()
            mintRoodID = 0
            If dgvData.SelectedRows.Count > 0 Then
                dgvData.Rows(dgvData.CurrentRow.Index).Selected = False
            End If
            mintFormMade = emFormMode.AddNew
        Catch ex As Exception
            fncSaveErr(mcstrClsName, "btnAdd_Click", ex)
        End Try
    End Sub
    '******************************************************************
    '　　　FUNCTION     :btnDelete_Click
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/02/23 AKB
    '      UPDATE       : 
    '******************************************************************
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dgvData.SelectedRows.Count < 0 Then Return

            If Not basCommon.fncMessageConfirm(mcstrDelConfirm) Then Exit Sub

            gobjDB.fncDeleteBranch(mintBranchId)
            xClear()
            xSetGridData()
            mintFormMade = emFormMode.AddNew

            mblnUpdate = True

        Catch ex As Exception
            fncSaveErr(mcstrClsName, "btnDelete_Click", ex)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     : Clear data
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/02/23 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub xClear()
        Try
            txtBranchName.Text = ""
            txtBrandRootPerson.Text = ""
            mintBranchId = 0
        Catch ex As Exception
            fncSaveErr(mcstrClsName, "xClear", ex)
        End Try
    End Sub

    '******************************************************************
    '　　　FUNCTION     :  xBindingFromGridToTexBox
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/02/23 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub xBindingFromGridToTexBox(ByVal intIndex As Integer)
        Try
            mintRoodID = 0
            If dgvData.Rows.Count <= 0 Then Exit Sub
            'fill data from grid to text box
            mintBranchId = basCommon.fncCnvToInt(dgvData.Item(enmGridCol.em_BranchID, intIndex).Value)
            txtBranchName.Text = basCommon.fncCnvNullToString(dgvData.Item(enmGridCol.em_BranchName, intIndex).Value)
            mstrBranchNameSelected = basCommon.fncCnvNullToString(dgvData.Item(enmGridCol.em_BranchName, intIndex).Value)
            txtBrandRootPerson.Text = dgvData.Item(enmGridCol.em_BranchMngName, intIndex).Value

        Catch ex As Exception
            fncSaveErr(mcstrClsName, " xBindingFromGridToTexBox", ex)
        End Try
    End Sub
    '******************************************************************
    '　　　FUNCTION     :btnChooseRoot_Click
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/03/07 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Sub btnChooseRoot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChooseRoot.Click
        Try
            mintRoodID = xGetRootID()
            mstrBranchMngName = basCommon.fncGetMemberName(mintRoodID)
            If mintRoodID > gcintNO_MEMBER Then txtBrandRootPerson.Text = basCommon.fncGetMemberName(mintRoodID)
        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "txtBrandRootPerson_GotFocus", ex, Nothing)
        End Try
    End Sub
    '******************************************************************
    '　　　FUNCTION     :xGetRootID
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/03/07 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Function xGetRootID() As Integer

        xGetRootID = mintRoodID 'basConst.gcintNO_MEMBER

        Try
            Dim intTotalMember As Integer
            Dim frmPerInfo As frmPersonInfo
            Dim frmPerList As frmPersonList

            intTotalMember = gobjDB.fncGetTotalMember()

            'there is no member in database -> addnew
            If intTotalMember = 0 Then

                frmPerInfo = New frmPersonInfo(clsEnum.emMode.ADD)
                If frmPerInfo.fncShowForm() Then

                    If frmPerInfo.FormModified Then xGetRootID = frmPerInfo.MemberID

                End If

            Else
                'there is at least 1 member in database -> select from list
                frmPerList = New frmPersonList()

                If frmPerList.fncShowForm(frmPersonList.enFormMode.SELECT_MEMBER, 0, clsEnum.emGender.UNKNOW) Then

                    If frmPerList.MemberSelected Then xGetRootID = frmPerList.MemberID

                End If

            End If

        Catch ex As Exception
            basCommon.fncSaveErr(mcstrClsName, "xGetRootID", ex, Nothing)
        End Try

    End Function
    '******************************************************************
    '　　　FUNCTION     :xCheckData
    '　　　MEMO         : 
    '　　　VALUE        : 
    '      PARAMS       : 
    '      CREATE       : 2013/03/07 PHV
    '      UPDATE       : 
    '******************************************************************
    Private Function xCheckData() As Boolean
        xCheckData = False
        Dim objDtTab As New DataTable
        Dim strWhere As String = ""
        Try
            'Kiểm tra tên của Chi
            If Trim(txtBranchName.Text) = "" Then
                fncMessageWarning(mcstrMissingBranchName)
                txtBranchName.Focus()
                Exit Function
            End If
            'Kiểm tra tên của chi đã tồn tại?
            If dgvData.SelectedRows.Count > 0 Then
                mstrBranchNameSelected = basCommon.fncCnvNullToString(dgvData.Item(enmGridCol.em_BranchName, dgvData.CurrentRow.Index).Value)
            End If

            If mstrBranchNameSelected <> Trim(txtBranchName.Text) Then
                strWhere = " WHERE "
                strWhere += " BRANCH_NAME = " + gobjDB.xStrSQLFormat(Trim(txtBranchName.Text))
                objDtTab = gobjDB.fncGetBranchList(strWhere)
                If Not IsNothing(objDtTab) Then
                    If objDtTab.Rows.Count > 0 Then
                        fncMessageWarning(mcstrExistBranchName)
                        txtBranchName.Focus()
                        Exit Function
                    End If
                End If
            End If
            'Kiem tra ten cua truong chi
            If Trim(txtBrandRootPerson.Text) = "" Then
                fncMessageWarning(mcstrMissingRootBranch)
                txtBrandRootPerson.Focus()
                Exit Function
            End If
            xCheckData = True
        Catch ex As Exception

        End Try
    End Function
    Private Function xSelectRow() As Boolean
        Try
            Dim intRowIndex As Integer
            'Lay ve index cua dong can duoc select
            If mintFormMade = emFormMode.AddNew Then
                intRowIndex = dgvData.RowCount - 1
            Else
                intRowIndex = mintRowSelectedIndex
            End If
            'select dong duoc chon
            dgvData.Rows(intRowIndex).Selected = True
        Catch ex As Exception

        End Try
    End Function

End Class
