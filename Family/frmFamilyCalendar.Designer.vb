﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamilyCalendar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFamilyCalendar))
        Me.lblNext = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvCalendar = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblPre = New System.Windows.Forms.Label()
        Me.cmdMonth = New System.Windows.Forms.ComboBox()
        Me.ndnYear = New System.Windows.Forms.NumericUpDown()
        Me.lblYear = New System.Windows.Forms.Label()
        Me.lblMonth = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPdf = New System.Windows.Forms.Button()
        Me.lblTitleForExport = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.ndnYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblNext
        '
        Me.lblNext.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNext.AutoSize = True
        Me.lblNext.BackColor = System.Drawing.Color.Transparent
        Me.lblNext.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblNext.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNext.ForeColor = System.Drawing.Color.White
        Me.lblNext.Location = New System.Drawing.Point(620, 6)
        Me.lblNext.Name = "lblNext"
        Me.lblNext.Size = New System.Drawing.Size(32, 22)
        Me.lblNext.TabIndex = 6
        Me.lblNext.Text = ">>"
        Me.lblNext.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.dgvCalendar)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(3, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(861, 660)
        Me.Panel1.TabIndex = 12
        '
        'dgvCalendar
        '
        Me.dgvCalendar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.dgvCalendar.AllowUserToAddRows = False
        Me.dgvCalendar.AllowUserToDeleteRows = False
        Me.dgvCalendar.AllowUserToResizeColumns = False
        Me.dgvCalendar.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalendar.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCalendar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCalendar.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvCalendar.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCalendar.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.dgvCalendar.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Navy
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Navy
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalendar.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCalendar.ColumnHeadersHeight = 25
        Me.dgvCalendar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvCalendar.Cursor = System.Windows.Forms.Cursors.Hand
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Tomato
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCalendar.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvCalendar.EnableHeadersVisualStyles = False
        Me.dgvCalendar.GridColor = System.Drawing.Color.MidnightBlue
        Me.dgvCalendar.Location = New System.Drawing.Point(0, 34)
        Me.dgvCalendar.Margin = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.dgvCalendar.MultiSelect = False
        Me.dgvCalendar.Name = "dgvCalendar"
        Me.dgvCalendar.ReadOnly = True
        Me.dgvCalendar.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Navy
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Navy
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalendar.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvCalendar.RowHeadersVisible = False
        Me.dgvCalendar.RowHeadersWidth = 10
        Me.dgvCalendar.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Navy
        Me.dgvCalendar.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Navy
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.Padding = New System.Windows.Forms.Padding(0, 0, 0, 5)
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.dgvCalendar.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Navy
        Me.dgvCalendar.RowTemplate.Height = 110
        Me.dgvCalendar.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCalendar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvCalendar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCalendar.ShowCellErrors = False
        Me.dgvCalendar.ShowCellToolTips = False
        Me.dgvCalendar.ShowEditingIcon = False
        Me.dgvCalendar.ShowRowErrors = False
        Me.dgvCalendar.Size = New System.Drawing.Size(861, 660)
        Me.dgvCalendar.StandardTab = True
        Me.dgvCalendar.TabIndex = 10
        Me.dgvCalendar.VirtualMode = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Navy
        Me.Panel2.Controls.Add(Me.lblTitleForExport)
        Me.Panel2.Controls.Add(Me.lblNext)
        Me.Panel2.Controls.Add(Me.lblPre)
        Me.Panel2.Controls.Add(Me.cmdMonth)
        Me.Panel2.Controls.Add(Me.ndnYear)
        Me.Panel2.Controls.Add(Me.lblYear)
        Me.Panel2.Controls.Add(Me.lblMonth)
        Me.Panel2.Location = New System.Drawing.Point(-1, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(861, 35)
        Me.Panel2.TabIndex = 9
        '
        'lblPre
        '
        Me.lblPre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPre.AutoSize = True
        Me.lblPre.BackColor = System.Drawing.Color.Transparent
        Me.lblPre.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblPre.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPre.ForeColor = System.Drawing.Color.White
        Me.lblPre.Location = New System.Drawing.Point(199, 6)
        Me.lblPre.Name = "lblPre"
        Me.lblPre.Size = New System.Drawing.Size(32, 22)
        Me.lblPre.TabIndex = 5
        Me.lblPre.Text = "<<"
        Me.lblPre.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdMonth
        '
        Me.cmdMonth.BackColor = System.Drawing.Color.Navy
        Me.cmdMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmdMonth.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdMonth.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdMonth.ForeColor = System.Drawing.Color.White
        Me.cmdMonth.FormatString = "N0"
        Me.cmdMonth.FormattingEnabled = True
        Me.cmdMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cmdMonth.Location = New System.Drawing.Point(346, 4)
        Me.cmdMonth.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cmdMonth.MaxDropDownItems = 12
        Me.cmdMonth.MaxLength = 2
        Me.cmdMonth.Name = "cmdMonth"
        Me.cmdMonth.Size = New System.Drawing.Size(60, 26)
        Me.cmdMonth.TabIndex = 2
        '
        'ndnYear
        '
        Me.ndnYear.BackColor = System.Drawing.Color.Navy
        Me.ndnYear.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ndnYear.ForeColor = System.Drawing.Color.White
        Me.ndnYear.Location = New System.Drawing.Point(493, 4)
        Me.ndnYear.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.ndnYear.Maximum = New Decimal(New Integer() {2500, 0, 0, 0})
        Me.ndnYear.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.ndnYear.Name = "ndnYear"
        Me.ndnYear.Size = New System.Drawing.Size(80, 26)
        Me.ndnYear.TabIndex = 4
        Me.ndnYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ndnYear.Value = New Decimal(New Integer() {2013, 0, 0, 0})
        '
        'lblYear
        '
        Me.lblYear.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblYear.AutoSize = True
        Me.lblYear.BackColor = System.Drawing.Color.Transparent
        Me.lblYear.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.ForeColor = System.Drawing.Color.White
        Me.lblYear.Location = New System.Drawing.Point(423, 6)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(49, 22)
        Me.lblYear.TabIndex = 3
        Me.lblYear.Text = "Năm"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMonth
        '
        Me.lblMonth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonth.AutoSize = True
        Me.lblMonth.BackColor = System.Drawing.Color.Transparent
        Me.lblMonth.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonth.ForeColor = System.Drawing.Color.White
        Me.lblMonth.Location = New System.Drawing.Point(268, 6)
        Me.lblMonth.Name = "lblMonth"
        Me.lblMonth.Size = New System.Drawing.Size(63, 22)
        Me.lblMonth.TabIndex = 1
        Me.lblMonth.Text = "Tháng"
        Me.lblMonth.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.phv.My.Resources.Resources.Cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(433, 671)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(103, 38)
        Me.btnCancel.TabIndex = 63
        Me.btnCancel.Text = "         Đóng"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPdf
        '
        Me.btnPdf.AutoSize = True
        Me.btnPdf.Image = Global.phv.My.Resources.Resources.exit32
        Me.btnPdf.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnPdf.Location = New System.Drawing.Point(290, 671)
        Me.btnPdf.Name = "btnPdf"
        Me.btnPdf.Size = New System.Drawing.Size(140, 38)
        Me.btnPdf.TabIndex = 62
        Me.btnPdf.Text = "     Xuất ra file Pdf"
        Me.btnPdf.UseVisualStyleBackColor = True
        '
        'lblTitleForExport
        '
        Me.lblTitleForExport.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitleForExport.AutoSize = True
        Me.lblTitleForExport.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleForExport.ForeColor = System.Drawing.Color.White
        Me.lblTitleForExport.Location = New System.Drawing.Point(345, 6)
        Me.lblTitleForExport.Name = "lblTitleForExport"
        Me.lblTitleForExport.Size = New System.Drawing.Size(180, 22)
        Me.lblTitleForExport.TabIndex = 11
        Me.lblTitleForExport.Text = "Tháng 12 năm 2013"
        Me.lblTitleForExport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTitleForExport.Visible = False
        '
        'frmFamilyCalendar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(869, 712)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPdf)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFamilyCalendar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lịch sự kiện"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.ndnYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblNext As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvCalendar As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblPre As System.Windows.Forms.Label
    Friend WithEvents cmdMonth As System.Windows.Forms.ComboBox
    Friend WithEvents ndnYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents lblMonth As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPdf As System.Windows.Forms.Button
    Friend WithEvents lblTitleForExport As System.Windows.Forms.Label
End Class
